#pragma warning disable 618
#pragma warning disable 612
#pragma warning disable 414
#pragma warning disable 168
namespace ZeroFormatter
{
    using global::System;
    using global::System.Collections.Generic;
    using global::System.Linq;
    using global::ZeroFormatter.Formatters;
    using global::ZeroFormatter.Internal;
    using global::ZeroFormatter.Segments;
    using global::ZeroFormatter.Comparers;

    public static partial class ZeroFormatterInitializer
    {
        static bool registered = false;

        [UnityEngine.RuntimeInitializeOnLoadMethod(UnityEngine.RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void Register()
        {
            if(registered) return;
            registered = true;
            // Enums
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.Inventory.CharacterStats.StatModType>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.CharacterStats.StatModTypeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.Inventory.CharacterStats.StatModType>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.CharacterStats.StatModTypeEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.Inventory.CharacterStats.StatModType?>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.CharacterStats.NullableStatModTypeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.Inventory.CharacterStats.StatModType?>.Register(new NullableEqualityComparer<global::_Balance._02_Scripts.Inventory.CharacterStats.StatModType>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.Inventory.Items.EquipmentType>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.Items.EquipmentTypeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.EquipmentType>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.Items.EquipmentTypeEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.Inventory.Items.EquipmentType?>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.Items.NullableEquipmentTypeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.EquipmentType?>.Register(new NullableEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.EquipmentType>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.Inventory.Items.EItem>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.Items.EItemFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.EItem>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.Items.EItemEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.Inventory.Items.EItem?>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.Items.NullableEItemFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.EItem?>.Register(new NullableEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.EItem>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.Inventory.Items.EPlaceable>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.Items.EPlaceableFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.EPlaceable>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.Items.EPlaceableEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.Inventory.Items.EPlaceable?>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.Items.NullableEPlaceableFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.EPlaceable?>.Register(new NullableEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.EPlaceable>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.Inventory.Items.EProp>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.Items.EPropFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.EProp>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.Items.EPropEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.Inventory.Items.EProp?>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.Items.NullableEPropFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.EProp?>.Register(new NullableEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.EProp>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.Inventory.Items.EPlantCategory>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.Items.EPlantCategoryFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.EPlantCategory>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.Items.EPlantCategoryEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.Inventory.Items.EPlantCategory?>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.Items.NullableEPlantCategoryFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.EPlantCategory?>.Register(new NullableEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.EPlantCategory>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.Inventory.Items.EPlantRequirement>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.Items.EPlantRequirementFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.EPlantRequirement>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.Items.EPlantRequirementEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.Inventory.Items.EPlantRequirement?>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.Items.NullableEPlantRequirementFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.EPlantRequirement?>.Register(new NullableEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.EPlantRequirement>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.Inventory.Items.ESoil>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.Items.ESoilFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.ESoil>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.Items.ESoilEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.Inventory.Items.ESoil?>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.Items.NullableESoilFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.ESoil?>.Register(new NullableEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.ESoil>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.ProceduralWorld.Chunks.SpawnType>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.ProceduralWorld.Chunks.SpawnTypeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.ProceduralWorld.Chunks.SpawnType>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.ProceduralWorld.Chunks.SpawnTypeEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.ProceduralWorld.Chunks.SpawnType?>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.ProceduralWorld.Chunks.NullableSpawnTypeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.ProceduralWorld.Chunks.SpawnType?>.Register(new NullableEqualityComparer<global::_Balance._02_Scripts.ProceduralWorld.Chunks.SpawnType>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.ProceduralWorld.Chunks.TileStateFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.ProceduralWorld.Chunks.TileStateEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState?>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.ProceduralWorld.Chunks.NullableTileStateFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState?>.Register(new NullableEqualityComparer<global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.ProceduralWorld.Chunks.Elevation>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.ProceduralWorld.Chunks.ElevationFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.ProceduralWorld.Chunks.Elevation>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.ProceduralWorld.Chunks.ElevationEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.ProceduralWorld.Chunks.Elevation?>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.ProceduralWorld.Chunks.NullableElevationFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.ProceduralWorld.Chunks.Elevation?>.Register(new NullableEqualityComparer<global::_Balance._02_Scripts.ProceduralWorld.Chunks.Elevation>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileRatioChange>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.ProceduralWorld.Chunks.TileRatioChangeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileRatioChange>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.ProceduralWorld.Chunks.TileRatioChangeEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileRatioChange?>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.ProceduralWorld.Chunks.NullableTileRatioChangeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileRatioChange?>.Register(new NullableEqualityComparer<global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileRatioChange>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.NoiseGenerator.NormalizeMode>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.ProceduralWorld.MapGenerator.NoiseGenerator_NormalizeModeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.NoiseGenerator.NormalizeMode>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.ProceduralWorld.MapGenerator.NoiseGenerator_NormalizeModeEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.NoiseGenerator.NormalizeMode?>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.ProceduralWorld.MapGenerator.NullableNoiseGenerator_NormalizeModeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.NoiseGenerator.NormalizeMode?>.Register(new NullableEqualityComparer<global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.NoiseGenerator.NormalizeMode>());
            
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.TestMapGenerator.DrawMode>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.ProceduralWorld.MapGenerator.TestMapGenerator_DrawModeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.TestMapGenerator.DrawMode>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.ProceduralWorld.MapGenerator.TestMapGenerator_DrawModeEqualityComparer());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.TestMapGenerator.DrawMode?>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.ProceduralWorld.MapGenerator.NullableTestMapGenerator_DrawModeFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Comparers.ZeroFormatterEqualityComparer<global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.TestMapGenerator.DrawMode?>.Register(new NullableEqualityComparer<global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.TestMapGenerator.DrawMode>());
            
            // Objects
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.Inventory.FileIO.ItemSlotSaveData>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.FileIO.ItemSlotSaveDataFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.Inventory.FileIO.ItemContainerSaveData>.Register(new ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.FileIO.ItemContainerSaveDataFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Savegame>.Register(new ZeroFormatter.DynamicObjectSegments.SavegameFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::TypeHint>.Register(new ZeroFormatter.DynamicObjectSegments.TypeHintFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::ChunkData>.Register(new ZeroFormatter.DynamicObjectSegments.ChunkDataFormatter<ZeroFormatter.Formatters.DefaultResolver>());
            // Structs
            {
                var structFormatter = new ZeroFormatter.DynamicObjectSegments.UnityEngine.Vector3Formatter<ZeroFormatter.Formatters.DefaultResolver>();
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::UnityEngine.Vector3>.Register(structFormatter);
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::UnityEngine.Vector3?>.Register(new global::ZeroFormatter.Formatters.NullableStructFormatter<ZeroFormatter.Formatters.DefaultResolver, global::UnityEngine.Vector3>(structFormatter));
            }
            {
                var structFormatter = new ZeroFormatter.DynamicObjectSegments.UnityEngine.Vector2Formatter<ZeroFormatter.Formatters.DefaultResolver>();
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::UnityEngine.Vector2>.Register(structFormatter);
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::UnityEngine.Vector2?>.Register(new global::ZeroFormatter.Formatters.NullableStructFormatter<ZeroFormatter.Formatters.DefaultResolver, global::UnityEngine.Vector2>(structFormatter));
            }
            {
                var structFormatter = new ZeroFormatter.DynamicObjectSegments.RotationFormatter<ZeroFormatter.Formatters.DefaultResolver>();
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Rotation>.Register(structFormatter);
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::Rotation?>.Register(new global::ZeroFormatter.Formatters.NullableStructFormatter<ZeroFormatter.Formatters.DefaultResolver, global::Rotation>(structFormatter));
            }
            {
                var structFormatter = new ZeroFormatter.DynamicObjectSegments.ItemPickupDataFormatter<ZeroFormatter.Formatters.DefaultResolver>();
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::ItemPickupData>.Register(structFormatter);
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::ItemPickupData?>.Register(new global::ZeroFormatter.Formatters.NullableStructFormatter<ZeroFormatter.Formatters.DefaultResolver, global::ItemPickupData>(structFormatter));
            }
            {
                var structFormatter = new ZeroFormatter.DynamicObjectSegments.PlantDataFormatter<ZeroFormatter.Formatters.DefaultResolver>();
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::PlantData>.Register(structFormatter);
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::PlantData?>.Register(new global::ZeroFormatter.Formatters.NullableStructFormatter<ZeroFormatter.Formatters.DefaultResolver, global::PlantData>(structFormatter));
            }
            {
                var structFormatter = new ZeroFormatter.DynamicObjectSegments.ItemStashDataFormatter<ZeroFormatter.Formatters.DefaultResolver>();
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::ItemStashData>.Register(structFormatter);
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::ItemStashData?>.Register(new global::ZeroFormatter.Formatters.NullableStructFormatter<ZeroFormatter.Formatters.DefaultResolver, global::ItemStashData>(structFormatter));
            }
            {
                var structFormatter = new ZeroFormatter.DynamicObjectSegments.PropDataFormatter<ZeroFormatter.Formatters.DefaultResolver>();
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::PropData>.Register(structFormatter);
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::PropData?>.Register(new global::ZeroFormatter.Formatters.NullableStructFormatter<ZeroFormatter.Formatters.DefaultResolver, global::PropData>(structFormatter));
            }
            {
                var structFormatter = new ZeroFormatter.DynamicObjectSegments.TreeDataFormatter<ZeroFormatter.Formatters.DefaultResolver>();
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::TreeData>.Register(structFormatter);
                ZeroFormatter.Formatters.Formatter<ZeroFormatter.Formatters.DefaultResolver, global::TreeData?>.Register(new global::ZeroFormatter.Formatters.NullableStructFormatter<ZeroFormatter.Formatters.DefaultResolver, global::TreeData>(structFormatter));
            }
            // Unions
            // Generics
            ZeroFormatter.Formatters.Formatter.RegisterArray<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.Inventory.FileIO.ItemSlotSaveData>();
            ZeroFormatter.Formatters.Formatter.RegisterArray<ZeroFormatter.Formatters.DefaultResolver, global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState>();
            ZeroFormatter.Formatters.Formatter.RegisterArray<ZeroFormatter.Formatters.DefaultResolver, global::ItemPickupData>();
            ZeroFormatter.Formatters.Formatter.RegisterArray<ZeroFormatter.Formatters.DefaultResolver, global::ItemStashData>();
            ZeroFormatter.Formatters.Formatter.RegisterArray<ZeroFormatter.Formatters.DefaultResolver, global::PlantData>();
            ZeroFormatter.Formatters.Formatter.RegisterArray<ZeroFormatter.Formatters.DefaultResolver, global::PropData>();
            ZeroFormatter.Formatters.Formatter.RegisterArray<ZeroFormatter.Formatters.DefaultResolver, global::TreeData>();
        }
    }
}
#pragma warning restore 168
#pragma warning restore 414
#pragma warning restore 618
#pragma warning restore 612
#pragma warning disable 618
#pragma warning disable 612
#pragma warning disable 414
#pragma warning disable 168
namespace ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.FileIO
{
    using global::System;
    using global::ZeroFormatter.Formatters;
    using global::ZeroFormatter.Internal;
    using global::ZeroFormatter.Segments;

    public class ItemSlotSaveDataFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.Inventory.FileIO.ItemSlotSaveData>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return null;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.Inventory.FileIO.ItemSlotSaveData value)
        {
            var segment = value as IZeroFormatterSegment;
            if (segment != null)
            {
                return segment.Serialize(ref bytes, offset);
            }
            else if (value == null)
            {
                BinaryUtil.WriteInt32(ref bytes, offset, -1);
                return 4;
            }
            else
            {
                var startOffset = offset;

                offset += (8 + 4 * (1 + 1));
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, string>(ref bytes, startOffset, offset, 0, value.ItemID);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, int>(ref bytes, startOffset, offset, 1, value.Amount);

                return ObjectSegmentHelper.WriteSize(ref bytes, startOffset, offset, 1);
            }
        }

        public override global::_Balance._02_Scripts.Inventory.FileIO.ItemSlotSaveData Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = BinaryUtil.ReadInt32(ref bytes, offset);
            if (byteSize == -1)
            {
                byteSize = 4;
                return null;
            }
            return new ItemSlotSaveDataObjectSegment<TTypeResolver>(tracker, new ArraySegment<byte>(bytes, offset, byteSize));
        }
    }

    public class ItemSlotSaveDataObjectSegment<TTypeResolver> : global::_Balance._02_Scripts.Inventory.FileIO.ItemSlotSaveData, IZeroFormatterSegment
        where TTypeResolver : ITypeResolver, new()
    {
        static readonly int[] __elementSizes = new int[]{ 0, 4 };

        readonly ArraySegment<byte> __originalBytes;
        readonly global::ZeroFormatter.DirtyTracker __tracker;
        readonly int __binaryLastIndex;
        readonly byte[] __extraFixedBytes;

        CacheSegment<TTypeResolver, string> _ItemID;

        // 0
        public override string ItemID
        {
            get
            {
                return _ItemID.Value;
            }
            set
            {
                _ItemID.Value = value;
            }
        }

        // 1
        public override int Amount
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, int>(__originalBytes, 1, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, int>(__originalBytes, 1, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }


        public ItemSlotSaveDataObjectSegment(global::ZeroFormatter.DirtyTracker dirtyTracker, ArraySegment<byte> originalBytes)
        {
            var __array = originalBytes.Array;

            this.__originalBytes = originalBytes;
            this.__tracker = dirtyTracker = dirtyTracker.CreateChild();
            this.__binaryLastIndex = BinaryUtil.ReadInt32(ref __array, originalBytes.Offset + 4);

            this.__extraFixedBytes = ObjectSegmentHelper.CreateExtraFixedBytes(this.__binaryLastIndex, 1, __elementSizes);

            _ItemID = new CacheSegment<TTypeResolver, string>(__tracker, ObjectSegmentHelper.GetSegment(originalBytes, 0, __binaryLastIndex, __tracker));
        }

        public bool CanDirectCopy()
        {
            return !__tracker.IsDirty;
        }

        public ArraySegment<byte> GetBufferReference()
        {
            return __originalBytes;
        }

        public int Serialize(ref byte[] targetBytes, int offset)
        {
            if (__extraFixedBytes != null || __tracker.IsDirty)
            {
                var startOffset = offset;
                offset += (8 + 4 * (1 + 1));

                offset += ObjectSegmentHelper.SerializeCacheSegment<TTypeResolver, string>(ref targetBytes, startOffset, offset, 0, ref _ItemID);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, int>(ref targetBytes, startOffset, offset, 1, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);

                return ObjectSegmentHelper.WriteSize(ref targetBytes, startOffset, offset, 1);
            }
            else
            {
                return ObjectSegmentHelper.DirectCopyAll(__originalBytes, ref targetBytes, offset);
            }
        }
    }

    public class ItemContainerSaveDataFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.Inventory.FileIO.ItemContainerSaveData>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return null;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.Inventory.FileIO.ItemContainerSaveData value)
        {
            var segment = value as IZeroFormatterSegment;
            if (segment != null)
            {
                return segment.Serialize(ref bytes, offset);
            }
            else if (value == null)
            {
                BinaryUtil.WriteInt32(ref bytes, offset, -1);
                return 4;
            }
            else
            {
                var startOffset = offset;

                offset += (8 + 4 * (0 + 1));
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::_Balance._02_Scripts.Inventory.FileIO.ItemSlotSaveData[]>(ref bytes, startOffset, offset, 0, value.SavedSlots);

                return ObjectSegmentHelper.WriteSize(ref bytes, startOffset, offset, 0);
            }
        }

        public override global::_Balance._02_Scripts.Inventory.FileIO.ItemContainerSaveData Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = BinaryUtil.ReadInt32(ref bytes, offset);
            if (byteSize == -1)
            {
                byteSize = 4;
                return null;
            }
            return new ItemContainerSaveDataObjectSegment<TTypeResolver>(tracker, new ArraySegment<byte>(bytes, offset, byteSize));
        }
    }

    public class ItemContainerSaveDataObjectSegment<TTypeResolver> : global::_Balance._02_Scripts.Inventory.FileIO.ItemContainerSaveData, IZeroFormatterSegment
        where TTypeResolver : ITypeResolver, new()
    {
        static readonly int[] __elementSizes = new int[]{ 0 };

        readonly ArraySegment<byte> __originalBytes;
        readonly global::ZeroFormatter.DirtyTracker __tracker;
        readonly int __binaryLastIndex;
        readonly byte[] __extraFixedBytes;

        CacheSegment<TTypeResolver, global::_Balance._02_Scripts.Inventory.FileIO.ItemSlotSaveData[]> _SavedSlots;

        // 0
        public override global::_Balance._02_Scripts.Inventory.FileIO.ItemSlotSaveData[] SavedSlots
        {
            get
            {
                return _SavedSlots.Value;
            }
            set
            {
                _SavedSlots.Value = value;
            }
        }


        public ItemContainerSaveDataObjectSegment(global::ZeroFormatter.DirtyTracker dirtyTracker, ArraySegment<byte> originalBytes)
        {
            var __array = originalBytes.Array;

            this.__originalBytes = originalBytes;
            this.__tracker = dirtyTracker = dirtyTracker.CreateChild();
            this.__binaryLastIndex = BinaryUtil.ReadInt32(ref __array, originalBytes.Offset + 4);

            this.__extraFixedBytes = ObjectSegmentHelper.CreateExtraFixedBytes(this.__binaryLastIndex, 0, __elementSizes);

            _SavedSlots = new CacheSegment<TTypeResolver, global::_Balance._02_Scripts.Inventory.FileIO.ItemSlotSaveData[]>(__tracker, ObjectSegmentHelper.GetSegment(originalBytes, 0, __binaryLastIndex, __tracker));
        }

        public bool CanDirectCopy()
        {
            return !__tracker.IsDirty;
        }

        public ArraySegment<byte> GetBufferReference()
        {
            return __originalBytes;
        }

        public int Serialize(ref byte[] targetBytes, int offset)
        {
            if (__extraFixedBytes != null || __tracker.IsDirty)
            {
                var startOffset = offset;
                offset += (8 + 4 * (0 + 1));

                offset += ObjectSegmentHelper.SerializeCacheSegment<TTypeResolver, global::_Balance._02_Scripts.Inventory.FileIO.ItemSlotSaveData[]>(ref targetBytes, startOffset, offset, 0, ref _SavedSlots);

                return ObjectSegmentHelper.WriteSize(ref targetBytes, startOffset, offset, 0);
            }
            else
            {
                return ObjectSegmentHelper.DirectCopyAll(__originalBytes, ref targetBytes, offset);
            }
        }
    }


}

#pragma warning restore 168
#pragma warning restore 414
#pragma warning restore 618
#pragma warning restore 612
#pragma warning disable 618
#pragma warning disable 612
#pragma warning disable 414
#pragma warning disable 168
namespace ZeroFormatter.DynamicObjectSegments
{
    using global::System;
    using global::ZeroFormatter.Formatters;
    using global::ZeroFormatter.Internal;
    using global::ZeroFormatter.Segments;

    public class SavegameFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Savegame>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return null;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Savegame value)
        {
            var segment = value as IZeroFormatterSegment;
            if (segment != null)
            {
                return segment.Serialize(ref bytes, offset);
            }
            else if (value == null)
            {
                BinaryUtil.WriteInt32(ref bytes, offset, -1);
                return 4;
            }
            else
            {
                var startOffset = offset;

                offset += (8 + 4 * (11 + 1));
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, string>(ref bytes, startOffset, offset, 0, value.WorldName);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, int>(ref bytes, startOffset, offset, 1, value.Seed);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, float>(ref bytes, startOffset, offset, 2, value.DayTime);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, float>(ref bytes, startOffset, offset, 3, value.Balance);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::UnityEngine.Vector3>(ref bytes, startOffset, offset, 4, value.PlayerPos);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::_Balance._02_Scripts.Inventory.FileIO.ItemContainerSaveData>(ref bytes, startOffset, offset, 5, value.InventoryItems);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::System.Collections.Generic.Dictionary<global::UnityEngine.Vector2, global::ChunkData>>(ref bytes, startOffset, offset, 6, value.ChunkData);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::UnityEngine.Vector2>(ref bytes, startOffset, offset, 7, value.PlayerChunkCoords);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, int>(ref bytes, startOffset, offset, 8, value.AmmoCount);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, float>(ref bytes, startOffset, offset, 9, value.Health);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::Rotation>(ref bytes, startOffset, offset, 10, value.PlayerRotation);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::Rotation>(ref bytes, startOffset, offset, 11, value.CameraRotation);

                return ObjectSegmentHelper.WriteSize(ref bytes, startOffset, offset, 11);
            }
        }

        public override global::Savegame Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = BinaryUtil.ReadInt32(ref bytes, offset);
            if (byteSize == -1)
            {
                byteSize = 4;
                return null;
            }
            return new SavegameObjectSegment<TTypeResolver>(tracker, new ArraySegment<byte>(bytes, offset, byteSize));
        }
    }

    public class SavegameObjectSegment<TTypeResolver> : global::Savegame, IZeroFormatterSegment
        where TTypeResolver : ITypeResolver, new()
    {
        static readonly int[] __elementSizes = new int[]{ 0, 4, 4, 4, 0, 0, 0, 0, 4, 4, 0, 0 };

        readonly ArraySegment<byte> __originalBytes;
        readonly global::ZeroFormatter.DirtyTracker __tracker;
        readonly int __binaryLastIndex;
        readonly byte[] __extraFixedBytes;

        CacheSegment<TTypeResolver, string> _WorldName;
        CacheSegment<TTypeResolver, global::UnityEngine.Vector3> _PlayerPos;
        global::_Balance._02_Scripts.Inventory.FileIO.ItemContainerSaveData _InventoryItems;
        CacheSegment<TTypeResolver, global::System.Collections.Generic.Dictionary<global::UnityEngine.Vector2, global::ChunkData>> _ChunkData;
        CacheSegment<TTypeResolver, global::UnityEngine.Vector2> _PlayerChunkCoords;
        CacheSegment<TTypeResolver, global::Rotation> _PlayerRotation;
        CacheSegment<TTypeResolver, global::Rotation> _CameraRotation;

        // 0
        public override string WorldName
        {
            get
            {
                return _WorldName.Value;
            }
            set
            {
                _WorldName.Value = value;
            }
        }

        // 1
        public override int Seed
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, int>(__originalBytes, 1, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, int>(__originalBytes, 1, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 2
        public override float DayTime
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, float>(__originalBytes, 2, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, float>(__originalBytes, 2, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 3
        public override float Balance
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, float>(__originalBytes, 3, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, float>(__originalBytes, 3, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 4
        public override global::UnityEngine.Vector3 PlayerPos
        {
            get
            {
                return _PlayerPos.Value;
            }
            set
            {
                _PlayerPos.Value = value;
            }
        }

        // 5
        public override global::_Balance._02_Scripts.Inventory.FileIO.ItemContainerSaveData InventoryItems
        {
            get
            {
                return _InventoryItems;
            }
            set
            {
                __tracker.Dirty();
                _InventoryItems = value;
            }
        }

        // 6
        public override global::System.Collections.Generic.Dictionary<global::UnityEngine.Vector2, global::ChunkData> ChunkData
        {
            get
            {
                return _ChunkData.Value;
            }
            set
            {
                _ChunkData.Value = value;
            }
        }

        // 7
        public override global::UnityEngine.Vector2 PlayerChunkCoords
        {
            get
            {
                return _PlayerChunkCoords.Value;
            }
            set
            {
                _PlayerChunkCoords.Value = value;
            }
        }

        // 8
        public override int AmmoCount
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, int>(__originalBytes, 8, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, int>(__originalBytes, 8, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 9
        public override float Health
        {
            get
            {
                return ObjectSegmentHelper.GetFixedProperty<TTypeResolver, float>(__originalBytes, 9, __binaryLastIndex, __extraFixedBytes, __tracker);
            }
            set
            {
                ObjectSegmentHelper.SetFixedProperty<TTypeResolver, float>(__originalBytes, 9, __binaryLastIndex, __extraFixedBytes, value, __tracker);
            }
        }

        // 10
        public override global::Rotation PlayerRotation
        {
            get
            {
                return _PlayerRotation.Value;
            }
            set
            {
                _PlayerRotation.Value = value;
            }
        }

        // 11
        public override global::Rotation CameraRotation
        {
            get
            {
                return _CameraRotation.Value;
            }
            set
            {
                _CameraRotation.Value = value;
            }
        }


        public SavegameObjectSegment(global::ZeroFormatter.DirtyTracker dirtyTracker, ArraySegment<byte> originalBytes)
        {
            var __array = originalBytes.Array;

            this.__originalBytes = originalBytes;
            this.__tracker = dirtyTracker = dirtyTracker.CreateChild();
            this.__binaryLastIndex = BinaryUtil.ReadInt32(ref __array, originalBytes.Offset + 4);

            this.__extraFixedBytes = ObjectSegmentHelper.CreateExtraFixedBytes(this.__binaryLastIndex, 11, __elementSizes);

            _WorldName = new CacheSegment<TTypeResolver, string>(__tracker, ObjectSegmentHelper.GetSegment(originalBytes, 0, __binaryLastIndex, __tracker));
            _PlayerPos = new CacheSegment<TTypeResolver, global::UnityEngine.Vector3>(__tracker, ObjectSegmentHelper.GetSegment(originalBytes, 4, __binaryLastIndex, __tracker));
            _InventoryItems = ObjectSegmentHelper.DeserializeSegment<TTypeResolver, global::_Balance._02_Scripts.Inventory.FileIO.ItemContainerSaveData>(originalBytes, 5, __binaryLastIndex, __tracker);
            _ChunkData = new CacheSegment<TTypeResolver, global::System.Collections.Generic.Dictionary<global::UnityEngine.Vector2, global::ChunkData>>(__tracker, ObjectSegmentHelper.GetSegment(originalBytes, 6, __binaryLastIndex, __tracker));
            _PlayerChunkCoords = new CacheSegment<TTypeResolver, global::UnityEngine.Vector2>(__tracker, ObjectSegmentHelper.GetSegment(originalBytes, 7, __binaryLastIndex, __tracker));
            _PlayerRotation = new CacheSegment<TTypeResolver, global::Rotation>(__tracker, ObjectSegmentHelper.GetSegment(originalBytes, 10, __binaryLastIndex, __tracker));
            _CameraRotation = new CacheSegment<TTypeResolver, global::Rotation>(__tracker, ObjectSegmentHelper.GetSegment(originalBytes, 11, __binaryLastIndex, __tracker));
        }

        public bool CanDirectCopy()
        {
            return !__tracker.IsDirty;
        }

        public ArraySegment<byte> GetBufferReference()
        {
            return __originalBytes;
        }

        public int Serialize(ref byte[] targetBytes, int offset)
        {
            if (__extraFixedBytes != null || __tracker.IsDirty)
            {
                var startOffset = offset;
                offset += (8 + 4 * (11 + 1));

                offset += ObjectSegmentHelper.SerializeCacheSegment<TTypeResolver, string>(ref targetBytes, startOffset, offset, 0, ref _WorldName);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, int>(ref targetBytes, startOffset, offset, 1, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, float>(ref targetBytes, startOffset, offset, 2, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, float>(ref targetBytes, startOffset, offset, 3, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeCacheSegment<TTypeResolver, global::UnityEngine.Vector3>(ref targetBytes, startOffset, offset, 4, ref _PlayerPos);
                offset += ObjectSegmentHelper.SerializeSegment<TTypeResolver, global::_Balance._02_Scripts.Inventory.FileIO.ItemContainerSaveData>(ref targetBytes, startOffset, offset, 5, _InventoryItems);
                offset += ObjectSegmentHelper.SerializeCacheSegment<TTypeResolver, global::System.Collections.Generic.Dictionary<global::UnityEngine.Vector2, global::ChunkData>>(ref targetBytes, startOffset, offset, 6, ref _ChunkData);
                offset += ObjectSegmentHelper.SerializeCacheSegment<TTypeResolver, global::UnityEngine.Vector2>(ref targetBytes, startOffset, offset, 7, ref _PlayerChunkCoords);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, int>(ref targetBytes, startOffset, offset, 8, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeFixedLength<TTypeResolver, float>(ref targetBytes, startOffset, offset, 9, __binaryLastIndex, __originalBytes, __extraFixedBytes, __tracker);
                offset += ObjectSegmentHelper.SerializeCacheSegment<TTypeResolver, global::Rotation>(ref targetBytes, startOffset, offset, 10, ref _PlayerRotation);
                offset += ObjectSegmentHelper.SerializeCacheSegment<TTypeResolver, global::Rotation>(ref targetBytes, startOffset, offset, 11, ref _CameraRotation);

                return ObjectSegmentHelper.WriteSize(ref targetBytes, startOffset, offset, 11);
            }
            else
            {
                return ObjectSegmentHelper.DirectCopyAll(__originalBytes, ref targetBytes, offset);
            }
        }
    }

    public class TypeHintFormatter<TTypeResolver> : Formatter<TTypeResolver, global::TypeHint>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return null;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::TypeHint value)
        {
            var segment = value as IZeroFormatterSegment;
            if (segment != null)
            {
                return segment.Serialize(ref bytes, offset);
            }
            else if (value == null)
            {
                BinaryUtil.WriteInt32(ref bytes, offset, -1);
                return 4;
            }
            else
            {
                var startOffset = offset;

                offset += (8 + 4 * (1 + 1));
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState[]>(ref bytes, startOffset, offset, 0, value.Hint1);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::System.Collections.Generic.Dictionary<global::UnityEngine.Vector2, global::ChunkData>>(ref bytes, startOffset, offset, 1, value.Hint2);

                return ObjectSegmentHelper.WriteSize(ref bytes, startOffset, offset, 1);
            }
        }

        public override global::TypeHint Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = BinaryUtil.ReadInt32(ref bytes, offset);
            if (byteSize == -1)
            {
                byteSize = 4;
                return null;
            }
            return new TypeHintObjectSegment<TTypeResolver>(tracker, new ArraySegment<byte>(bytes, offset, byteSize));
        }
    }

    public class TypeHintObjectSegment<TTypeResolver> : global::TypeHint, IZeroFormatterSegment
        where TTypeResolver : ITypeResolver, new()
    {
        static readonly int[] __elementSizes = new int[]{ 0, 0 };

        readonly ArraySegment<byte> __originalBytes;
        readonly global::ZeroFormatter.DirtyTracker __tracker;
        readonly int __binaryLastIndex;
        readonly byte[] __extraFixedBytes;

        CacheSegment<TTypeResolver, global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState[]> _Hint1;
        CacheSegment<TTypeResolver, global::System.Collections.Generic.Dictionary<global::UnityEngine.Vector2, global::ChunkData>> _Hint2;

        // 0
        public override global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState[] Hint1
        {
            get
            {
                return _Hint1.Value;
            }
            set
            {
                _Hint1.Value = value;
            }
        }

        // 1
        public override global::System.Collections.Generic.Dictionary<global::UnityEngine.Vector2, global::ChunkData> Hint2
        {
            get
            {
                return _Hint2.Value;
            }
            set
            {
                _Hint2.Value = value;
            }
        }


        public TypeHintObjectSegment(global::ZeroFormatter.DirtyTracker dirtyTracker, ArraySegment<byte> originalBytes)
        {
            var __array = originalBytes.Array;

            this.__originalBytes = originalBytes;
            this.__tracker = dirtyTracker = dirtyTracker.CreateChild();
            this.__binaryLastIndex = BinaryUtil.ReadInt32(ref __array, originalBytes.Offset + 4);

            this.__extraFixedBytes = ObjectSegmentHelper.CreateExtraFixedBytes(this.__binaryLastIndex, 1, __elementSizes);

            _Hint1 = new CacheSegment<TTypeResolver, global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState[]>(__tracker, ObjectSegmentHelper.GetSegment(originalBytes, 0, __binaryLastIndex, __tracker));
            _Hint2 = new CacheSegment<TTypeResolver, global::System.Collections.Generic.Dictionary<global::UnityEngine.Vector2, global::ChunkData>>(__tracker, ObjectSegmentHelper.GetSegment(originalBytes, 1, __binaryLastIndex, __tracker));
        }

        public bool CanDirectCopy()
        {
            return !__tracker.IsDirty;
        }

        public ArraySegment<byte> GetBufferReference()
        {
            return __originalBytes;
        }

        public int Serialize(ref byte[] targetBytes, int offset)
        {
            if (__extraFixedBytes != null || __tracker.IsDirty)
            {
                var startOffset = offset;
                offset += (8 + 4 * (1 + 1));

                offset += ObjectSegmentHelper.SerializeCacheSegment<TTypeResolver, global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState[]>(ref targetBytes, startOffset, offset, 0, ref _Hint1);
                offset += ObjectSegmentHelper.SerializeCacheSegment<TTypeResolver, global::System.Collections.Generic.Dictionary<global::UnityEngine.Vector2, global::ChunkData>>(ref targetBytes, startOffset, offset, 1, ref _Hint2);

                return ObjectSegmentHelper.WriteSize(ref targetBytes, startOffset, offset, 1);
            }
            else
            {
                return ObjectSegmentHelper.DirectCopyAll(__originalBytes, ref targetBytes, offset);
            }
        }
    }

    public class ChunkDataFormatter<TTypeResolver> : Formatter<TTypeResolver, global::ChunkData>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return null;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::ChunkData value)
        {
            var segment = value as IZeroFormatterSegment;
            if (segment != null)
            {
                return segment.Serialize(ref bytes, offset);
            }
            else if (value == null)
            {
                BinaryUtil.WriteInt32(ref bytes, offset, -1);
                return 4;
            }
            else
            {
                var startOffset = offset;

                offset += (8 + 4 * (5 + 1));
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState[]>(ref bytes, startOffset, offset, 0, value.TileStates);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::ItemPickupData[]>(ref bytes, startOffset, offset, 1, value.Items);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::PlantData[]>(ref bytes, startOffset, offset, 2, value.Plants);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::ItemStashData[]>(ref bytes, startOffset, offset, 3, value.ItemStashes);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::PropData[]>(ref bytes, startOffset, offset, 4, value.Props);
                offset += ObjectSegmentHelper.SerializeFromFormatter<TTypeResolver, global::TreeData[]>(ref bytes, startOffset, offset, 5, value.Trees);

                return ObjectSegmentHelper.WriteSize(ref bytes, startOffset, offset, 5);
            }
        }

        public override global::ChunkData Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = BinaryUtil.ReadInt32(ref bytes, offset);
            if (byteSize == -1)
            {
                byteSize = 4;
                return null;
            }
            return new ChunkDataObjectSegment<TTypeResolver>(tracker, new ArraySegment<byte>(bytes, offset, byteSize));
        }
    }

    public class ChunkDataObjectSegment<TTypeResolver> : global::ChunkData, IZeroFormatterSegment
        where TTypeResolver : ITypeResolver, new()
    {
        static readonly int[] __elementSizes = new int[]{ 0, 0, 0, 0, 0, 0 };

        readonly ArraySegment<byte> __originalBytes;
        readonly global::ZeroFormatter.DirtyTracker __tracker;
        readonly int __binaryLastIndex;
        readonly byte[] __extraFixedBytes;

        CacheSegment<TTypeResolver, global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState[]> _TileStates;
        CacheSegment<TTypeResolver, global::ItemPickupData[]> _Items;
        CacheSegment<TTypeResolver, global::PlantData[]> _Plants;
        CacheSegment<TTypeResolver, global::ItemStashData[]> _ItemStashes;
        CacheSegment<TTypeResolver, global::PropData[]> _Props;
        CacheSegment<TTypeResolver, global::TreeData[]> _Trees;

        // 0
        public override global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState[] TileStates
        {
            get
            {
                return _TileStates.Value;
            }
            set
            {
                _TileStates.Value = value;
            }
        }

        // 1
        public override global::ItemPickupData[] Items
        {
            get
            {
                return _Items.Value;
            }
            set
            {
                _Items.Value = value;
            }
        }

        // 2
        public override global::PlantData[] Plants
        {
            get
            {
                return _Plants.Value;
            }
            set
            {
                _Plants.Value = value;
            }
        }

        // 3
        public override global::ItemStashData[] ItemStashes
        {
            get
            {
                return _ItemStashes.Value;
            }
            set
            {
                _ItemStashes.Value = value;
            }
        }

        // 4
        public override global::PropData[] Props
        {
            get
            {
                return _Props.Value;
            }
            set
            {
                _Props.Value = value;
            }
        }

        // 5
        public override global::TreeData[] Trees
        {
            get
            {
                return _Trees.Value;
            }
            set
            {
                _Trees.Value = value;
            }
        }


        public ChunkDataObjectSegment(global::ZeroFormatter.DirtyTracker dirtyTracker, ArraySegment<byte> originalBytes)
        {
            var __array = originalBytes.Array;

            this.__originalBytes = originalBytes;
            this.__tracker = dirtyTracker = dirtyTracker.CreateChild();
            this.__binaryLastIndex = BinaryUtil.ReadInt32(ref __array, originalBytes.Offset + 4);

            this.__extraFixedBytes = ObjectSegmentHelper.CreateExtraFixedBytes(this.__binaryLastIndex, 5, __elementSizes);

            _TileStates = new CacheSegment<TTypeResolver, global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState[]>(__tracker, ObjectSegmentHelper.GetSegment(originalBytes, 0, __binaryLastIndex, __tracker));
            _Items = new CacheSegment<TTypeResolver, global::ItemPickupData[]>(__tracker, ObjectSegmentHelper.GetSegment(originalBytes, 1, __binaryLastIndex, __tracker));
            _Plants = new CacheSegment<TTypeResolver, global::PlantData[]>(__tracker, ObjectSegmentHelper.GetSegment(originalBytes, 2, __binaryLastIndex, __tracker));
            _ItemStashes = new CacheSegment<TTypeResolver, global::ItemStashData[]>(__tracker, ObjectSegmentHelper.GetSegment(originalBytes, 3, __binaryLastIndex, __tracker));
            _Props = new CacheSegment<TTypeResolver, global::PropData[]>(__tracker, ObjectSegmentHelper.GetSegment(originalBytes, 4, __binaryLastIndex, __tracker));
            _Trees = new CacheSegment<TTypeResolver, global::TreeData[]>(__tracker, ObjectSegmentHelper.GetSegment(originalBytes, 5, __binaryLastIndex, __tracker));
        }

        public bool CanDirectCopy()
        {
            return !__tracker.IsDirty;
        }

        public ArraySegment<byte> GetBufferReference()
        {
            return __originalBytes;
        }

        public int Serialize(ref byte[] targetBytes, int offset)
        {
            if (__extraFixedBytes != null || __tracker.IsDirty)
            {
                var startOffset = offset;
                offset += (8 + 4 * (5 + 1));

                offset += ObjectSegmentHelper.SerializeCacheSegment<TTypeResolver, global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState[]>(ref targetBytes, startOffset, offset, 0, ref _TileStates);
                offset += ObjectSegmentHelper.SerializeCacheSegment<TTypeResolver, global::ItemPickupData[]>(ref targetBytes, startOffset, offset, 1, ref _Items);
                offset += ObjectSegmentHelper.SerializeCacheSegment<TTypeResolver, global::PlantData[]>(ref targetBytes, startOffset, offset, 2, ref _Plants);
                offset += ObjectSegmentHelper.SerializeCacheSegment<TTypeResolver, global::ItemStashData[]>(ref targetBytes, startOffset, offset, 3, ref _ItemStashes);
                offset += ObjectSegmentHelper.SerializeCacheSegment<TTypeResolver, global::PropData[]>(ref targetBytes, startOffset, offset, 4, ref _Props);
                offset += ObjectSegmentHelper.SerializeCacheSegment<TTypeResolver, global::TreeData[]>(ref targetBytes, startOffset, offset, 5, ref _Trees);

                return ObjectSegmentHelper.WriteSize(ref targetBytes, startOffset, offset, 5);
            }
            else
            {
                return ObjectSegmentHelper.DirectCopyAll(__originalBytes, ref targetBytes, offset);
            }
        }
    }


}

#pragma warning restore 168
#pragma warning restore 414
#pragma warning restore 618
#pragma warning restore 612
#pragma warning disable 618
#pragma warning disable 612
#pragma warning disable 414
#pragma warning disable 168
namespace ZeroFormatter.DynamicObjectSegments.UnityEngine
{
    using global::System;
    using global::ZeroFormatter.Formatters;
    using global::ZeroFormatter.Internal;
    using global::ZeroFormatter.Segments;

    public class Vector3Formatter<TTypeResolver> : Formatter<TTypeResolver, global::UnityEngine.Vector3>
        where TTypeResolver : ITypeResolver, new()
    {
        readonly Formatter<TTypeResolver, float> formatter0;
        readonly Formatter<TTypeResolver, float> formatter1;
        readonly Formatter<TTypeResolver, float> formatter2;
        
        public override bool NoUseDirtyTracker
        {
            get
            {
                return formatter0.NoUseDirtyTracker
                    && formatter1.NoUseDirtyTracker
                    && formatter2.NoUseDirtyTracker
                ;
            }
        }

        public Vector3Formatter()
        {
            formatter0 = Formatter<TTypeResolver, float>.Default;
            formatter1 = Formatter<TTypeResolver, float>.Default;
            formatter2 = Formatter<TTypeResolver, float>.Default;
            
        }

        public override int? GetLength()
        {
            return 12;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::UnityEngine.Vector3 value)
        {
            BinaryUtil.EnsureCapacity(ref bytes, offset, 12);
            var startOffset = offset;
            offset += formatter0.Serialize(ref bytes, offset, value.x);
            offset += formatter1.Serialize(ref bytes, offset, value.y);
            offset += formatter2.Serialize(ref bytes, offset, value.z);
            return offset - startOffset;
        }

        public override global::UnityEngine.Vector3 Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 0;
            int size;
            var item0 = formatter0.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            var item1 = formatter1.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            var item2 = formatter2.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            
            return new global::UnityEngine.Vector3(item0, item1, item2);
        }
    }

    public class Vector2Formatter<TTypeResolver> : Formatter<TTypeResolver, global::UnityEngine.Vector2>
        where TTypeResolver : ITypeResolver, new()
    {
        readonly Formatter<TTypeResolver, float> formatter0;
        readonly Formatter<TTypeResolver, float> formatter1;
        
        public override bool NoUseDirtyTracker
        {
            get
            {
                return formatter0.NoUseDirtyTracker
                    && formatter1.NoUseDirtyTracker
                ;
            }
        }

        public Vector2Formatter()
        {
            formatter0 = Formatter<TTypeResolver, float>.Default;
            formatter1 = Formatter<TTypeResolver, float>.Default;
            
        }

        public override int? GetLength()
        {
            return 8;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::UnityEngine.Vector2 value)
        {
            BinaryUtil.EnsureCapacity(ref bytes, offset, 8);
            var startOffset = offset;
            offset += formatter0.Serialize(ref bytes, offset, value.x);
            offset += formatter1.Serialize(ref bytes, offset, value.y);
            return offset - startOffset;
        }

        public override global::UnityEngine.Vector2 Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 0;
            int size;
            var item0 = formatter0.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            var item1 = formatter1.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            
            return new global::UnityEngine.Vector2(item0, item1);
        }
    }


}

#pragma warning restore 168
#pragma warning restore 414
#pragma warning restore 618
#pragma warning restore 612
#pragma warning disable 618
#pragma warning disable 612
#pragma warning disable 414
#pragma warning disable 168
namespace ZeroFormatter.DynamicObjectSegments
{
    using global::System;
    using global::ZeroFormatter.Formatters;
    using global::ZeroFormatter.Internal;
    using global::ZeroFormatter.Segments;

    public class RotationFormatter<TTypeResolver> : Formatter<TTypeResolver, global::Rotation>
        where TTypeResolver : ITypeResolver, new()
    {
        readonly Formatter<TTypeResolver, float> formatter0;
        readonly Formatter<TTypeResolver, float> formatter1;
        readonly Formatter<TTypeResolver, float> formatter2;
        readonly Formatter<TTypeResolver, float> formatter3;
        
        public override bool NoUseDirtyTracker
        {
            get
            {
                return formatter0.NoUseDirtyTracker
                    && formatter1.NoUseDirtyTracker
                    && formatter2.NoUseDirtyTracker
                    && formatter3.NoUseDirtyTracker
                ;
            }
        }

        public RotationFormatter()
        {
            formatter0 = Formatter<TTypeResolver, float>.Default;
            formatter1 = Formatter<TTypeResolver, float>.Default;
            formatter2 = Formatter<TTypeResolver, float>.Default;
            formatter3 = Formatter<TTypeResolver, float>.Default;
            
        }

        public override int? GetLength()
        {
            return 16;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::Rotation value)
        {
            BinaryUtil.EnsureCapacity(ref bytes, offset, 16);
            var startOffset = offset;
            offset += formatter0.Serialize(ref bytes, offset, value.X);
            offset += formatter1.Serialize(ref bytes, offset, value.Y);
            offset += formatter2.Serialize(ref bytes, offset, value.Z);
            offset += formatter3.Serialize(ref bytes, offset, value.W);
            return offset - startOffset;
        }

        public override global::Rotation Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 0;
            int size;
            var item0 = formatter0.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            var item1 = formatter1.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            var item2 = formatter2.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            var item3 = formatter3.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            
            return new global::Rotation(item0, item1, item2, item3);
        }
    }

    public class ItemPickupDataFormatter<TTypeResolver> : Formatter<TTypeResolver, global::ItemPickupData>
        where TTypeResolver : ITypeResolver, new()
    {
        readonly Formatter<TTypeResolver, global::UnityEngine.Vector3> formatter0;
        readonly Formatter<TTypeResolver, int> formatter1;
        
        public override bool NoUseDirtyTracker
        {
            get
            {
                return formatter0.NoUseDirtyTracker
                    && formatter1.NoUseDirtyTracker
                ;
            }
        }

        public ItemPickupDataFormatter()
        {
            formatter0 = Formatter<TTypeResolver, global::UnityEngine.Vector3>.Default;
            formatter1 = Formatter<TTypeResolver, int>.Default;
            
        }

        public override int? GetLength()
        {
            return null;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::ItemPickupData value)
        {
            var startOffset = offset;
            offset += formatter0.Serialize(ref bytes, offset, value.Position);
            offset += formatter1.Serialize(ref bytes, offset, value.VariantIndex);
            return offset - startOffset;
        }

        public override global::ItemPickupData Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 0;
            int size;
            var item0 = formatter0.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            var item1 = formatter1.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            
            return new global::ItemPickupData(item0, item1);
        }
    }

    public class PlantDataFormatter<TTypeResolver> : Formatter<TTypeResolver, global::PlantData>
        where TTypeResolver : ITypeResolver, new()
    {
        readonly Formatter<TTypeResolver, global::UnityEngine.Vector3> formatter0;
        readonly Formatter<TTypeResolver, global::_Balance._02_Scripts.Inventory.Items.EPlaceable> formatter1;
        readonly Formatter<TTypeResolver, float> formatter2;
        readonly Formatter<TTypeResolver, global::Rotation> formatter3;
        readonly Formatter<TTypeResolver, float> formatter4;
        readonly Formatter<TTypeResolver, float> formatter5;
        
        public override bool NoUseDirtyTracker
        {
            get
            {
                return formatter0.NoUseDirtyTracker
                    && formatter1.NoUseDirtyTracker
                    && formatter2.NoUseDirtyTracker
                    && formatter3.NoUseDirtyTracker
                    && formatter4.NoUseDirtyTracker
                    && formatter5.NoUseDirtyTracker
                ;
            }
        }

        public PlantDataFormatter()
        {
            formatter0 = Formatter<TTypeResolver, global::UnityEngine.Vector3>.Default;
            formatter1 = Formatter<TTypeResolver, global::_Balance._02_Scripts.Inventory.Items.EPlaceable>.Default;
            formatter2 = Formatter<TTypeResolver, float>.Default;
            formatter3 = Formatter<TTypeResolver, global::Rotation>.Default;
            formatter4 = Formatter<TTypeResolver, float>.Default;
            formatter5 = Formatter<TTypeResolver, float>.Default;
            
        }

        public override int? GetLength()
        {
            return null;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::PlantData value)
        {
            var startOffset = offset;
            offset += formatter0.Serialize(ref bytes, offset, value.Position);
            offset += formatter1.Serialize(ref bytes, offset, value.Type);
            offset += formatter2.Serialize(ref bytes, offset, value.LifeTime);
            offset += formatter3.Serialize(ref bytes, offset, value.Rotation);
            offset += formatter4.Serialize(ref bytes, offset, value.FullGrowthTime);
            offset += formatter5.Serialize(ref bytes, offset, value.BalanceChange);
            return offset - startOffset;
        }

        public override global::PlantData Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 0;
            int size;
            var item0 = formatter0.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            var item1 = formatter1.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            var item2 = formatter2.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            var item3 = formatter3.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            var item4 = formatter4.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            var item5 = formatter5.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            
            return new global::PlantData(item0, item1, item2, item3, item4, item5);
        }
    }

    public class ItemStashDataFormatter<TTypeResolver> : Formatter<TTypeResolver, global::ItemStashData>
        where TTypeResolver : ITypeResolver, new()
    {
        readonly Formatter<TTypeResolver, global::UnityEngine.Vector3> formatter0;
        readonly Formatter<TTypeResolver, global::_Balance._02_Scripts.Inventory.FileIO.ItemContainerSaveData> formatter1;
        
        public override bool NoUseDirtyTracker
        {
            get
            {
                return formatter0.NoUseDirtyTracker
                    && formatter1.NoUseDirtyTracker
                ;
            }
        }

        public ItemStashDataFormatter()
        {
            formatter0 = Formatter<TTypeResolver, global::UnityEngine.Vector3>.Default;
            formatter1 = Formatter<TTypeResolver, global::_Balance._02_Scripts.Inventory.FileIO.ItemContainerSaveData>.Default;
            
        }

        public override int? GetLength()
        {
            return null;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::ItemStashData value)
        {
            var startOffset = offset;
            offset += formatter0.Serialize(ref bytes, offset, value.Position);
            offset += formatter1.Serialize(ref bytes, offset, value.Contents);
            return offset - startOffset;
        }

        public override global::ItemStashData Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 0;
            int size;
            var item0 = formatter0.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            var item1 = formatter1.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            
            return new global::ItemStashData(item0, item1);
        }
    }

    public class PropDataFormatter<TTypeResolver> : Formatter<TTypeResolver, global::PropData>
        where TTypeResolver : ITypeResolver, new()
    {
        readonly Formatter<TTypeResolver, global::UnityEngine.Vector3> formatter0;
        readonly Formatter<TTypeResolver, global::Rotation> formatter1;
        readonly Formatter<TTypeResolver, global::UnityEngine.Vector3> formatter2;
        readonly Formatter<TTypeResolver, global::_Balance._02_Scripts.Inventory.Items.EProp> formatter3;
        
        public override bool NoUseDirtyTracker
        {
            get
            {
                return formatter0.NoUseDirtyTracker
                    && formatter1.NoUseDirtyTracker
                    && formatter2.NoUseDirtyTracker
                    && formatter3.NoUseDirtyTracker
                ;
            }
        }

        public PropDataFormatter()
        {
            formatter0 = Formatter<TTypeResolver, global::UnityEngine.Vector3>.Default;
            formatter1 = Formatter<TTypeResolver, global::Rotation>.Default;
            formatter2 = Formatter<TTypeResolver, global::UnityEngine.Vector3>.Default;
            formatter3 = Formatter<TTypeResolver, global::_Balance._02_Scripts.Inventory.Items.EProp>.Default;
            
        }

        public override int? GetLength()
        {
            return null;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::PropData value)
        {
            var startOffset = offset;
            offset += formatter0.Serialize(ref bytes, offset, value.Position);
            offset += formatter1.Serialize(ref bytes, offset, value.Rotation);
            offset += formatter2.Serialize(ref bytes, offset, value.Scale);
            offset += formatter3.Serialize(ref bytes, offset, value.Type);
            return offset - startOffset;
        }

        public override global::PropData Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 0;
            int size;
            var item0 = formatter0.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            var item1 = formatter1.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            var item2 = formatter2.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            var item3 = formatter3.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            
            return new global::PropData(item0, item1, item2, item3);
        }
    }

    public class TreeDataFormatter<TTypeResolver> : Formatter<TTypeResolver, global::TreeData>
        where TTypeResolver : ITypeResolver, new()
    {
        readonly Formatter<TTypeResolver, global::UnityEngine.Vector3> formatter0;
        readonly Formatter<TTypeResolver, global::Rotation> formatter1;
        readonly Formatter<TTypeResolver, int> formatter2;
        
        public override bool NoUseDirtyTracker
        {
            get
            {
                return formatter0.NoUseDirtyTracker
                    && formatter1.NoUseDirtyTracker
                    && formatter2.NoUseDirtyTracker
                ;
            }
        }

        public TreeDataFormatter()
        {
            formatter0 = Formatter<TTypeResolver, global::UnityEngine.Vector3>.Default;
            formatter1 = Formatter<TTypeResolver, global::Rotation>.Default;
            formatter2 = Formatter<TTypeResolver, int>.Default;
            
        }

        public override int? GetLength()
        {
            return null;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::TreeData value)
        {
            var startOffset = offset;
            offset += formatter0.Serialize(ref bytes, offset, value.Position);
            offset += formatter1.Serialize(ref bytes, offset, value.Rotation);
            offset += formatter2.Serialize(ref bytes, offset, value.VariantIndex);
            return offset - startOffset;
        }

        public override global::TreeData Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 0;
            int size;
            var item0 = formatter0.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            var item1 = formatter1.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            var item2 = formatter2.Deserialize(ref bytes, offset, tracker, out size);
            offset += size;
            byteSize += size;
            
            return new global::TreeData(item0, item1, item2);
        }
    }


}

#pragma warning restore 168
#pragma warning restore 414
#pragma warning restore 618
#pragma warning restore 612
#pragma warning disable 618
#pragma warning disable 612
#pragma warning disable 414
#pragma warning disable 168
namespace ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.CharacterStats
{
    using global::System;
    using global::System.Collections.Generic;
    using global::ZeroFormatter.Formatters;
    using global::ZeroFormatter.Internal;
    using global::ZeroFormatter.Segments;


    public class StatModTypeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.Inventory.CharacterStats.StatModType>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.Inventory.CharacterStats.StatModType value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::_Balance._02_Scripts.Inventory.CharacterStats.StatModType Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::_Balance._02_Scripts.Inventory.CharacterStats.StatModType)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableStatModTypeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.Inventory.CharacterStats.StatModType?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.Inventory.CharacterStats.StatModType? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::_Balance._02_Scripts.Inventory.CharacterStats.StatModType? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::_Balance._02_Scripts.Inventory.CharacterStats.StatModType)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class StatModTypeEqualityComparer : IEqualityComparer<global::_Balance._02_Scripts.Inventory.CharacterStats.StatModType>
    {
        public bool Equals(global::_Balance._02_Scripts.Inventory.CharacterStats.StatModType x, global::_Balance._02_Scripts.Inventory.CharacterStats.StatModType y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::_Balance._02_Scripts.Inventory.CharacterStats.StatModType x)
        {
            return (int)x;
        }
    }



}
#pragma warning restore 168
#pragma warning restore 414
#pragma warning restore 618
#pragma warning restore 612
#pragma warning disable 618
#pragma warning disable 612
#pragma warning disable 414
#pragma warning disable 168
namespace ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.Inventory.Items
{
    using global::System;
    using global::System.Collections.Generic;
    using global::ZeroFormatter.Formatters;
    using global::ZeroFormatter.Internal;
    using global::ZeroFormatter.Segments;


    public class EquipmentTypeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.Inventory.Items.EquipmentType>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.Inventory.Items.EquipmentType value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::_Balance._02_Scripts.Inventory.Items.EquipmentType Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::_Balance._02_Scripts.Inventory.Items.EquipmentType)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableEquipmentTypeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.Inventory.Items.EquipmentType?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.Inventory.Items.EquipmentType? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::_Balance._02_Scripts.Inventory.Items.EquipmentType? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::_Balance._02_Scripts.Inventory.Items.EquipmentType)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class EquipmentTypeEqualityComparer : IEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.EquipmentType>
    {
        public bool Equals(global::_Balance._02_Scripts.Inventory.Items.EquipmentType x, global::_Balance._02_Scripts.Inventory.Items.EquipmentType y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::_Balance._02_Scripts.Inventory.Items.EquipmentType x)
        {
            return (int)x;
        }
    }



    public class EItemFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.Inventory.Items.EItem>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.Inventory.Items.EItem value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::_Balance._02_Scripts.Inventory.Items.EItem Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::_Balance._02_Scripts.Inventory.Items.EItem)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableEItemFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.Inventory.Items.EItem?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.Inventory.Items.EItem? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::_Balance._02_Scripts.Inventory.Items.EItem? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::_Balance._02_Scripts.Inventory.Items.EItem)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class EItemEqualityComparer : IEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.EItem>
    {
        public bool Equals(global::_Balance._02_Scripts.Inventory.Items.EItem x, global::_Balance._02_Scripts.Inventory.Items.EItem y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::_Balance._02_Scripts.Inventory.Items.EItem x)
        {
            return (int)x;
        }
    }



    public class EPlaceableFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.Inventory.Items.EPlaceable>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.Inventory.Items.EPlaceable value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::_Balance._02_Scripts.Inventory.Items.EPlaceable Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::_Balance._02_Scripts.Inventory.Items.EPlaceable)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableEPlaceableFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.Inventory.Items.EPlaceable?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.Inventory.Items.EPlaceable? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::_Balance._02_Scripts.Inventory.Items.EPlaceable? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::_Balance._02_Scripts.Inventory.Items.EPlaceable)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class EPlaceableEqualityComparer : IEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.EPlaceable>
    {
        public bool Equals(global::_Balance._02_Scripts.Inventory.Items.EPlaceable x, global::_Balance._02_Scripts.Inventory.Items.EPlaceable y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::_Balance._02_Scripts.Inventory.Items.EPlaceable x)
        {
            return (int)x;
        }
    }



    public class EPropFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.Inventory.Items.EProp>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.Inventory.Items.EProp value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::_Balance._02_Scripts.Inventory.Items.EProp Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::_Balance._02_Scripts.Inventory.Items.EProp)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableEPropFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.Inventory.Items.EProp?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.Inventory.Items.EProp? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::_Balance._02_Scripts.Inventory.Items.EProp? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::_Balance._02_Scripts.Inventory.Items.EProp)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class EPropEqualityComparer : IEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.EProp>
    {
        public bool Equals(global::_Balance._02_Scripts.Inventory.Items.EProp x, global::_Balance._02_Scripts.Inventory.Items.EProp y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::_Balance._02_Scripts.Inventory.Items.EProp x)
        {
            return (int)x;
        }
    }



    public class EPlantCategoryFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.Inventory.Items.EPlantCategory>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.Inventory.Items.EPlantCategory value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::_Balance._02_Scripts.Inventory.Items.EPlantCategory Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::_Balance._02_Scripts.Inventory.Items.EPlantCategory)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableEPlantCategoryFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.Inventory.Items.EPlantCategory?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.Inventory.Items.EPlantCategory? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::_Balance._02_Scripts.Inventory.Items.EPlantCategory? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::_Balance._02_Scripts.Inventory.Items.EPlantCategory)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class EPlantCategoryEqualityComparer : IEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.EPlantCategory>
    {
        public bool Equals(global::_Balance._02_Scripts.Inventory.Items.EPlantCategory x, global::_Balance._02_Scripts.Inventory.Items.EPlantCategory y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::_Balance._02_Scripts.Inventory.Items.EPlantCategory x)
        {
            return (int)x;
        }
    }



    public class EPlantRequirementFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.Inventory.Items.EPlantRequirement>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.Inventory.Items.EPlantRequirement value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::_Balance._02_Scripts.Inventory.Items.EPlantRequirement Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::_Balance._02_Scripts.Inventory.Items.EPlantRequirement)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableEPlantRequirementFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.Inventory.Items.EPlantRequirement?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.Inventory.Items.EPlantRequirement? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::_Balance._02_Scripts.Inventory.Items.EPlantRequirement? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::_Balance._02_Scripts.Inventory.Items.EPlantRequirement)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class EPlantRequirementEqualityComparer : IEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.EPlantRequirement>
    {
        public bool Equals(global::_Balance._02_Scripts.Inventory.Items.EPlantRequirement x, global::_Balance._02_Scripts.Inventory.Items.EPlantRequirement y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::_Balance._02_Scripts.Inventory.Items.EPlantRequirement x)
        {
            return (int)x;
        }
    }



    public class ESoilFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.Inventory.Items.ESoil>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.Inventory.Items.ESoil value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::_Balance._02_Scripts.Inventory.Items.ESoil Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::_Balance._02_Scripts.Inventory.Items.ESoil)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableESoilFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.Inventory.Items.ESoil?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.Inventory.Items.ESoil? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::_Balance._02_Scripts.Inventory.Items.ESoil? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::_Balance._02_Scripts.Inventory.Items.ESoil)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class ESoilEqualityComparer : IEqualityComparer<global::_Balance._02_Scripts.Inventory.Items.ESoil>
    {
        public bool Equals(global::_Balance._02_Scripts.Inventory.Items.ESoil x, global::_Balance._02_Scripts.Inventory.Items.ESoil y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::_Balance._02_Scripts.Inventory.Items.ESoil x)
        {
            return (int)x;
        }
    }



}
#pragma warning restore 168
#pragma warning restore 414
#pragma warning restore 618
#pragma warning restore 612
#pragma warning disable 618
#pragma warning disable 612
#pragma warning disable 414
#pragma warning disable 168
namespace ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.ProceduralWorld.Chunks
{
    using global::System;
    using global::System.Collections.Generic;
    using global::ZeroFormatter.Formatters;
    using global::ZeroFormatter.Internal;
    using global::ZeroFormatter.Segments;


    public class SpawnTypeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.ProceduralWorld.Chunks.SpawnType>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.ProceduralWorld.Chunks.SpawnType value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::_Balance._02_Scripts.ProceduralWorld.Chunks.SpawnType Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::_Balance._02_Scripts.ProceduralWorld.Chunks.SpawnType)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableSpawnTypeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.ProceduralWorld.Chunks.SpawnType?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.ProceduralWorld.Chunks.SpawnType? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::_Balance._02_Scripts.ProceduralWorld.Chunks.SpawnType? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::_Balance._02_Scripts.ProceduralWorld.Chunks.SpawnType)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class SpawnTypeEqualityComparer : IEqualityComparer<global::_Balance._02_Scripts.ProceduralWorld.Chunks.SpawnType>
    {
        public bool Equals(global::_Balance._02_Scripts.ProceduralWorld.Chunks.SpawnType x, global::_Balance._02_Scripts.ProceduralWorld.Chunks.SpawnType y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::_Balance._02_Scripts.ProceduralWorld.Chunks.SpawnType x)
        {
            return (int)x;
        }
    }



    public class TileStateFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableTileStateFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class TileStateEqualityComparer : IEqualityComparer<global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState>
    {
        public bool Equals(global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState x, global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileState x)
        {
            return (int)x;
        }
    }



    public class ElevationFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.ProceduralWorld.Chunks.Elevation>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.ProceduralWorld.Chunks.Elevation value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::_Balance._02_Scripts.ProceduralWorld.Chunks.Elevation Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::_Balance._02_Scripts.ProceduralWorld.Chunks.Elevation)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableElevationFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.ProceduralWorld.Chunks.Elevation?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.ProceduralWorld.Chunks.Elevation? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::_Balance._02_Scripts.ProceduralWorld.Chunks.Elevation? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::_Balance._02_Scripts.ProceduralWorld.Chunks.Elevation)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class ElevationEqualityComparer : IEqualityComparer<global::_Balance._02_Scripts.ProceduralWorld.Chunks.Elevation>
    {
        public bool Equals(global::_Balance._02_Scripts.ProceduralWorld.Chunks.Elevation x, global::_Balance._02_Scripts.ProceduralWorld.Chunks.Elevation y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::_Balance._02_Scripts.ProceduralWorld.Chunks.Elevation x)
        {
            return (int)x;
        }
    }



    public class TileRatioChangeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileRatioChange>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileRatioChange value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileRatioChange Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileRatioChange)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableTileRatioChangeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileRatioChange?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileRatioChange? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileRatioChange? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileRatioChange)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class TileRatioChangeEqualityComparer : IEqualityComparer<global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileRatioChange>
    {
        public bool Equals(global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileRatioChange x, global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileRatioChange y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::_Balance._02_Scripts.ProceduralWorld.Chunks.TileRatioChange x)
        {
            return (int)x;
        }
    }



}
#pragma warning restore 168
#pragma warning restore 414
#pragma warning restore 618
#pragma warning restore 612
#pragma warning disable 618
#pragma warning disable 612
#pragma warning disable 414
#pragma warning disable 168
namespace ZeroFormatter.DynamicObjectSegments._Balance._02_Scripts.ProceduralWorld.MapGenerator
{
    using global::System;
    using global::System.Collections.Generic;
    using global::ZeroFormatter.Formatters;
    using global::ZeroFormatter.Internal;
    using global::ZeroFormatter.Segments;


    public class NoiseGenerator_NormalizeModeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.NoiseGenerator.NormalizeMode>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.NoiseGenerator.NormalizeMode value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.NoiseGenerator.NormalizeMode Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.NoiseGenerator.NormalizeMode)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableNoiseGenerator_NormalizeModeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.NoiseGenerator.NormalizeMode?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.NoiseGenerator.NormalizeMode? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.NoiseGenerator.NormalizeMode? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.NoiseGenerator.NormalizeMode)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class NoiseGenerator_NormalizeModeEqualityComparer : IEqualityComparer<global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.NoiseGenerator.NormalizeMode>
    {
        public bool Equals(global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.NoiseGenerator.NormalizeMode x, global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.NoiseGenerator.NormalizeMode y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.NoiseGenerator.NormalizeMode x)
        {
            return (int)x;
        }
    }



    public class TestMapGenerator_DrawModeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.TestMapGenerator.DrawMode>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 4;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.TestMapGenerator.DrawMode value)
        {
            return BinaryUtil.WriteInt32(ref bytes, offset, (Int32)value);
        }

        public override global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.TestMapGenerator.DrawMode Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 4;
            return (global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.TestMapGenerator.DrawMode)BinaryUtil.ReadInt32(ref bytes, offset);
        }
    }


    public class NullableTestMapGenerator_DrawModeFormatter<TTypeResolver> : Formatter<TTypeResolver, global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.TestMapGenerator.DrawMode?>
        where TTypeResolver : ITypeResolver, new()
    {
        public override int? GetLength()
        {
            return 5;
        }

        public override int Serialize(ref byte[] bytes, int offset, global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.TestMapGenerator.DrawMode? value)
        {
            BinaryUtil.WriteBoolean(ref bytes, offset, value.HasValue);
            if (value.HasValue)
            {
                BinaryUtil.WriteInt32(ref bytes, offset + 1, (Int32)value.Value);
            }
            else
            {
                BinaryUtil.EnsureCapacity(ref bytes, offset, offset + 5);
            }

            return 5;
        }

        public override global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.TestMapGenerator.DrawMode? Deserialize(ref byte[] bytes, int offset, global::ZeroFormatter.DirtyTracker tracker, out int byteSize)
        {
            byteSize = 5;
            var hasValue = BinaryUtil.ReadBoolean(ref bytes, offset);
            if (!hasValue) return null;

            return (global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.TestMapGenerator.DrawMode)BinaryUtil.ReadInt32(ref bytes, offset + 1);
        }
    }



    public class TestMapGenerator_DrawModeEqualityComparer : IEqualityComparer<global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.TestMapGenerator.DrawMode>
    {
        public bool Equals(global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.TestMapGenerator.DrawMode x, global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.TestMapGenerator.DrawMode y)
        {
            return (Int32)x == (Int32)y;
        }

        public int GetHashCode(global::_Balance._02_Scripts.ProceduralWorld.MapGenerator.TestMapGenerator.DrawMode x)
        {
            return (int)x;
        }
    }



}
#pragma warning restore 168
#pragma warning restore 414
#pragma warning restore 618
#pragma warning restore 612
