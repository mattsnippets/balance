﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Texture Splatting" {

	Properties {
		_MainTex ("Splat Map", 2D) = "white" {}
		[NoScaleOffset] _Texture1 ("Clean", 2D) = "white" {}
		[NoScaleOffset] _Texture2 ("Planted", 2D) = "white" {}
		[NoScaleOffset] _Texture3 ("Protected", 2D) = "white" {}
		[NoScaleOffset] _Texture4 ("Corrupted", 2D) = "white" {}
	}

	SubShader {

		Pass {

			Tags { "LightMode" = "ForwardBase" }

			CGPROGRAM

			#pragma vertex MyVertexProgram
			#pragma fragment MyFragmentProgram

			#include "UnityCG.cginc"
			#include "AutoLight.cginc"
			#pragma multi_compile_fwdbase

			float4 _MainTex_ST;
			sampler2D _MainTex;

			sampler2D _Texture1, _Texture2, _Texture3, _Texture4;

			struct VertexData {
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f {
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float2 uvSplat : TEXCOORD1;
				LIGHTING_COORDS(2, 3)
			};

			v2f MyVertexProgram (VertexData v) {
				v2f i;
				i.pos = UnityObjectToClipPos(v.vertex);
				TRANSFER_VERTEX_TO_FRAGMENT(i);
				i.uvSplat = v.uv;
				return i;
			}

			float4 MyFragmentProgram (v2f i) : SV_TARGET {
				float4 splat = tex2D(_MainTex, i.uvSplat);
				float4 color =
					tex2D(_Texture1, i.uv) * splat.r +
					tex2D(_Texture2, i.uv) * splat.g +
					tex2D(_Texture3, i.uv) * splat.b +
					tex2D(_Texture4, i.uv) * (1 - splat.r - splat.g - splat.b);
				return color * LIGHT_ATTENUATION(i);
			}

			ENDCG
		}
	}
	
	Fallback "VertexLit"
}