﻿using System;
using _Balance._02_Scripts.BusinessLogic;
using _Balance._02_Scripts.Entities;
using _Balance._02_Scripts.Player;
using _Balance._02_Scripts.Utilities;
using TMPro;
using UniRx;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Balance._02_Scripts.Enemies
{
    public class SpiderBrain : MonoBehaviour
    {
        [SerializeField] private Transform _attackSource;
        [SerializeField] private ParticleSystem _attackParticle;
        [SerializeField] private TextMeshPro _debugText;

        private SpiderSettings _settings;
        private bool _isInitialized;
        private Vector3[] _patrolPoints;
        private int _patrolIndex;
        private LayerMask _obstacleMask;

        private SpiderMovement _movement;
        private Vector3 _prevPosition;
        private Health _health;
        private Transform _movementTarget;
        private ProjectileTarget _projectileTarget;

        private State _state;
        private IDisposable _stuckCheckInterval;
        private IDisposable _aiInterval;
        private LayerMask _playerMask;
        private PlayerController _playerController;
        private Damageable _playerDamageable;

        private bool _isAttackCoolingDown;

        private void OnEnable()
        {
            if (!_isInitialized)
            {
                Init();
            }

            _state = State.Patrolling;
            _movement.SetRagdollState(false);
            SetupPatrol();

            ResetIntervals();

            _health.Reset();
        }

        private void ResetIntervals()
        {
            _stuckCheckInterval?.Dispose();
            _stuckCheckInterval = Observable.Interval(TimeSpan.FromSeconds(_settings.StuckCheckInterval)).Subscribe(_ =>
            {
                if ((transform.position - _prevPosition).sqrMagnitude < Mathf.Pow(_settings.StuckThreshold, 2))
                {
                    HandleMovementTargetReached();
                }

                _prevPosition = transform.position;
            }).AddTo(this);

            _aiInterval?.Dispose();
            _aiInterval = Observable.Interval(TimeSpan.FromSeconds(1.0)).Subscribe(_ => Think()).AddTo(this);
        }

        private void Think()
        {
            switch (_state)
            {
                case State.Patrolling:
                {
                    if (Physics.CheckSphere(transform.position, _settings.DetectionRadius, _playerMask))
                    {
                        _state = State.Chasing;
                        _movementTarget.parent = _playerController.transform;
                        _movementTarget.localPosition = Vector3.zero;
                        _movement.TargetReachedDistance = _settings.ChaseTargetReachedDistance;
                        _debugText.text = "Chasing";
                    }

                    break;
                }
                case State.Chasing:
                {
                    var targetSqrDistance = (_movementTarget.position - transform.position).sqrMagnitude;

                    if (targetSqrDistance >= _settings.ChaseMaxDistance * _settings.ChaseMaxDistance)
                    {
                        _state = State.Patrolling;
                        _movementTarget.parent = null;
                        _movementTarget.position = _patrolPoints[_patrolIndex];
                        _movement.TargetReachedDistance = _settings.PatrolTargetReachedDistance;
                        _debugText.text = "Patrolling";
                    }
                    else if (targetSqrDistance <= _settings.ChaseTargetReachedDistance * _settings.ChaseTargetReachedDistance)
                    {
                        TryAttacking();
                    }

                    break;
                }
            }
        }

        public void Kill()
        {
            _aiInterval?.Dispose();
            _movement.SetRagdollState(true);
        }

        private void TryAttacking()
        {
            var towardsTarget = _movementTarget.position - _attackSource.position;

            if (!(Vector3.ProjectOnPlane(towardsTarget, Vector3.up).sqrMagnitude <= _settings.AttackRange * _settings.AttackRange))
            {
                return;
            }

            if (_isAttackCoolingDown)
            {
                return;
            }

            _isAttackCoolingDown = true;
            _movement.IsStopped = true;

            Observable.Timer(TimeSpan.FromSeconds(_settings.AttackCooldown)).Subscribe(_ =>
                _isAttackCoolingDown = false).AddTo(this);

            Observable.Timer(TimeSpan.FromSeconds(1.0)).Subscribe(_ =>
            {
                if (!_movement.IsRagdolling)
                {
                    _movement.IsStopped = false;
                }
            }).AddTo(this);

            _attackParticle.Play();

            if (Vector3.Angle(_attackSource.forward, towardsTarget) <= _settings.AttackAngle)
            {
                _playerDamageable.Hit(_settings.AttackDamage);
            }
        }

        private void SetupPatrol()
        {
            GroundRaycaster.GetGroundPoint(transform.position, out var centerPoint);

            for (int i = 0; i < _settings.PatrolPointCount; i++)
            {
                _patrolPoints[i] = GeneratePatrolPoint(centerPoint);
            }

            _patrolIndex = 0;
            _movementTarget.position = _patrolPoints[_patrolIndex];
            _movement.TargetReachedDistance = _settings.PatrolTargetReachedDistance;

            _debugText.text = "Patrolling";
        }

        private void OnDisable() => _stuckCheckInterval?.Dispose();

        private void Init()
        {
            _isInitialized = true;

            var target = new GameObject("SpiderTarget");
            _movement = GetComponent<SpiderMovement>();
            _movement.MovementTarget = target.transform;
            _movementTarget = target.transform;

            _settings = ServiceLocator.Resolve<Settings>().SpiderSettings;
            _playerController = ServiceLocator.Resolve<PlayerController>();
            _playerDamageable = _playerController.GetComponent<Damageable>();

            _obstacleMask = LayerMask.GetMask("Trees");
            _playerMask = LayerMask.GetMask("Player");

            _patrolPoints = new Vector3[_settings.PatrolPointCount];

            _movement.OnMovementTargetReached.Subscribe(_ => HandleMovementTargetReached()).AddTo(this);

            _health = GetComponentInChildren<Health>();
            _health.OnDied.Subscribe(_ => Kill()).AddTo(this);

            _projectileTarget = GetComponentInChildren<ProjectileTarget>();
            _projectileTarget.OnHit.Subscribe(hitDir => _movement.Flinch(hitDir)).AddTo(this);
        }

        private void HandleMovementTargetReached()
        {
            _debugText.text = "Target Reached";

            if (_state == State.Patrolling)
            {
                _patrolIndex = ++_patrolIndex % _settings.PatrolPointCount;
                _movementTarget.position = _patrolPoints[_patrolIndex];
                _debugText.text = "Patrolling";
            }
        }

        private Vector3 GeneratePatrolPoint(Vector3 centerPoint)
        {
            int tries = 0;
            bool foundPosition = false;
            Vector3 patrolPosition = centerPoint;

            do
            {
                var pos = Random.insideUnitCircle * _settings.PatrolRadius;
                var posOnPlane = centerPoint + new Vector3(pos.x, 0f, pos.y);
                GroundRaycaster.GetGroundPoint(posOnPlane, out var possiblePosition);

                if (!Physics.CheckSphere(possiblePosition, _settings.PatrolObstacleFreeRadius, _obstacleMask))
                {
                    foundPosition = true;
                    patrolPosition = possiblePosition;
                }

                tries++;
            } while (tries < _settings.PatrolGenerationTries && !foundPosition);

            if (!foundPosition)
            {
                Debug.LogWarning("Could not find suitable patrol position");
            }

            return patrolPosition;
        }

        // private void OnDrawGizmos()
        // {
        //     Gizmos.color = Color.red;
        //
        //     // foreach (var point in _patrolPoints)
        //     // {
        //     //     Gizmos.DrawWireSphere(point, 1f);
        //     // }
        //
        //     Gizmos.DrawWireSphere(transform.position, _settings.DetectionRadius);
        // }

        private enum State
        {
            Patrolling = 1,
            Chasing = 2
        }
    }
}