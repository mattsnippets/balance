﻿using _Balance._02_Scripts.BusinessLogic;
using DitzelGames.FastIK;
using UniRx;
using UnityEngine;

namespace _Balance._02_Scripts.Enemies
{
    public class SpiderMovement : MonoBehaviour
    {
        [SerializeField] private Transform _body;
        [SerializeField] private Rigidbody _rigRigidbody;
        [SerializeField] private Rigidbody[] _legRigidbodies;
        [SerializeField] private SpiderBody _spiderBodyLogic;
        [SerializeField] private Transform _spiderTrigger;

        public Transform MovementTarget { get; set; }
        public float TargetReachedDistance { get; set; } = 0.3f;
        public readonly Subject<Unit> OnMovementTargetReached = new Subject<Unit>();
        public bool IsStopped { get; set; }
        public bool IsRagdolling => _isRagdolling;

        private SpiderSettings _settings;
        private Rigidbody _mainRigidbody;
        private Vector3 _movement;
        private bool _isRagdolling;

        private readonly RaycastHit[] _raycastHit = new RaycastHit[1];
        private readonly Collider[] _neighborEnemies = new Collider[5];

        private SpiderLeg[] _legs;
        private Transform[] _transforms;
        private FastIKFabric[] _ikScripts;
        private readonly Quaternion[] _legOriginalRots = new Quaternion[8];
        private Quaternion _rigRigidbodyOriginalRot;
        private Vector3[] _originalLocalPositions;

        private LayerMask _spiderMask;
        private LayerMask _treeMask;
        private int _ragdollsLayer;
        private int _enemiesLayer;
        private int _spidersLayer;

        private void Awake()
        {
            _treeMask = LayerMask.GetMask("Trees");
            _spiderMask = LayerMask.GetMask("Spiders");
            _ragdollsLayer = LayerMask.NameToLayer("Ragdolls");
            _enemiesLayer = LayerMask.NameToLayer("Enemies");
            _spidersLayer = LayerMask.NameToLayer("Spiders");

            _mainRigidbody = GetComponent<Rigidbody>();
            _transforms = GetComponentsInChildren<Transform>();
            _ikScripts = GetComponentsInChildren<FastIKFabric>();
            _legs = GetComponentsInChildren<SpiderLeg>();

            StoreOriginalTransformStates();
        }

        private void Start()
        {
            _settings = ServiceLocator.Resolve<Settings>().SpiderSettings;
        }

        private void Update()
        {
            if (IsStopped)
            {
                return;
            }

            _movement = CalculateMovement();
            RotateTowardsTarget();
        }

        private void FixedUpdate()
        {
            if (IsStopped)
            {
                return;
            }

            _mainRigidbody.AddForce(_movement, ForceMode.Acceleration);
            _mainRigidbody.velocity = Vector3.ClampMagnitude(_mainRigidbody.velocity, _settings.Speed);
        }

        private void StoreOriginalTransformStates()
        {
            _originalLocalPositions = new Vector3[_transforms.Length];

            for (int i = 0; i < _originalLocalPositions.Length; i++)
            {
                _originalLocalPositions[i] = _transforms[i].localPosition;
            }

            for (int i = 0; i < _legOriginalRots.Length; i++)
            {
                _legOriginalRots[i] = _legRigidbodies[i].transform.localRotation;
            }

            _rigRigidbodyOriginalRot = _rigRigidbody.transform.localRotation;
        }

        public void SetRagdollState(bool isEnabled)
        {
            IsStopped = isEnabled;
            _isRagdolling = isEnabled;
            
            _rigRigidbody.SetDensity(2000f);
            _rigRigidbody.mass = _rigRigidbody.mass;

            for (int i = 0; i < _legRigidbodies.Length; i++)
            {
                _legRigidbodies[i].SetDensity(50f);
                _legRigidbodies[i].mass = _legRigidbodies[i].mass;
                Destroy(_legRigidbodies[i].GetComponent<CharacterJoint>());
                var joint = _legRigidbodies[i].gameObject.AddComponent<CharacterJoint>();
                joint.connectedBody = _rigRigidbody;
            }

            if (isEnabled)
            {
                for (int i = 0; i < _transforms.Length; i++)
                {
                    _transforms[i].gameObject.layer = _ragdollsLayer;
                }

                _mainRigidbody.drag = 0.001f;
                _mainRigidbody.angularDrag = 0.001f;
            }
            else
            {
                for (int i = 0; i < _transforms.Length; i++)
                {
                    _transforms[i].gameObject.layer = _enemiesLayer;

                    if (i == 0)
                    {
                        continue;
                    }

                    _transforms[i].localPosition = _originalLocalPositions[i];
                }

                _spiderTrigger.gameObject.layer = _spidersLayer;

                _mainRigidbody.velocity = Vector3.zero;
                _mainRigidbody.angularVelocity = Vector3.zero;
                _mainRigidbody.rotation = Quaternion.identity;
                _mainRigidbody.isKinematic = false;
                _mainRigidbody.drag = 5f;
                _mainRigidbody.angularDrag = 1f;

                _rigRigidbody.transform.localRotation = _rigRigidbodyOriginalRot;
                _rigRigidbody.rotation = _rigRigidbodyOriginalRot;
                _rigRigidbody.velocity = Vector3.zero;
                _rigRigidbody.angularVelocity = Vector3.zero;
            }

            _rigRigidbody.useGravity = isEnabled;
            _rigRigidbody.isKinematic = !isEnabled;
            _spiderBodyLogic.enabled = !isEnabled;

            for (int i = 0; i < _legRigidbodies.Length; i++)
            {
                _legRigidbodies[i].useGravity = isEnabled;
                _legRigidbodies[i].isKinematic = !isEnabled;
                _legs[i].enabled = !isEnabled;
                _ikScripts[i].enabled = !isEnabled;

                if (!isEnabled)
                {
                    _legRigidbodies[i].transform.localRotation = _legOriginalRots[i];
                    _legRigidbodies[i].velocity = Vector3.zero;
                    _legRigidbodies[i].angularVelocity = Vector3.zero;
                }
            }
        }

        public void Flinch(Vector3 dir)
        {
            _mainRigidbody.AddForce(dir * _settings.FlinchAmount, ForceMode.VelocityChange);
        }

        private void RotateTowardsTarget()
        {
            var heading = Vector3.ProjectOnPlane(_movement, Vector3.up).normalized;

            if (heading.sqrMagnitude < 0.1f)
            {
                heading = Vector3.ProjectOnPlane(MovementTarget.position - transform.position, Vector3.up).normalized;
            }

            var rot = Quaternion.Slerp(
                transform.rotation,
                Quaternion.LookRotation(heading),
                _settings.TurnSpeed * Time.deltaTime);

            transform.rotation = rot;

            for (int i = 0; i < _legs.Length; i++)
            {
                var leg = _legs[i];

                if (leg.NextTarget)
                {
                    _legs[i].Target.rotation = rot;
                    _legs[i].NextTarget.rotation = rot;
                }
            }
        }

        private Vector3 CalculateMovement()
        {
            Vector3 movementVelocity = Vector3.zero;

            var seek = CalculateSeek();

            if ((MovementTarget.position - transform.position).sqrMagnitude
                <= TargetReachedDistance * TargetReachedDistance)
            {
                OnMovementTargetReached.OnNext(Unit.Default);
                seek = Vector3.zero;
            }

            movementVelocity += seek;
            movementVelocity += CalculateAvoidance();
            //movementVelocity += CalculateSeparation();

            if (movementVelocity.sqrMagnitude < _settings.MinMovementVectorSqMagnitude)
            {
                movementVelocity = Vector3.zero;
            }

            return movementVelocity;
        }

        private Vector3 CalculateSeek()
        {
            var desiredVelocity = Vector3.ProjectOnPlane(MovementTarget.position - transform.position, Vector3.up);
            var distance = desiredVelocity.magnitude;

            if (distance < _settings.SlowingDistance)
            {
                desiredVelocity = desiredVelocity.normalized
                    * (_settings.Speed * (distance / _settings.SlowingDistance));
            }
            else
            {
                desiredVelocity = desiredVelocity.normalized * _settings.Speed;
            }

            Vector3 seek = desiredVelocity - _mainRigidbody.velocity;
            return seek;
        }

        private Vector3 CalculateAvoidance()
        {
            var length = _mainRigidbody.velocity.magnitude / _settings.Speed;

            if (Physics.SphereCastNonAlloc(
                _body.position,
                _settings.AvoidanceRadius,
                transform.forward,
                _raycastHit,
                length,
                _treeMask) > 0)
            {
                var ahead = _body.position + transform.forward * length;

                var avoidance = Vector3.ProjectOnPlane(
                                    (ahead - _raycastHit[0].transform.position),
                                    Vector3.up).normalized
                                * _settings.MaxAvoidanceForce;

                return avoidance;
            }

            return Vector3.zero;
        }

        private Vector3 CalculateSeparation()
        {
            var separation = Vector3.zero;

            int neighborOverlaps = Physics.OverlapSphereNonAlloc(
                transform.position,
                _settings.SeparationCheckRadius,
                _neighborEnemies,
                _spiderMask,
                QueryTriggerInteraction.Collide);

            for (int i = 0; i < neighborOverlaps; i++)
            {
                var neighbor = _neighborEnemies[i].transform.parent.gameObject;

                if (neighbor != gameObject)
                {
                    separation +=
                        Vector3.ProjectOnPlane(transform.position - neighbor.transform.position, Vector3.up).normalized
                        * (1f / _settings.SeparationCheckRadius * _settings.SeparationStrength);
                }
            }

            return separation;
        }

        // private void OnDrawGizmos()
        // {
        //     if (_settings == null)
        //     {
        //         return;
        //     }
        //
        //     Gizmos.color = Color.yellow;
        //
        //     var length = _rigidbody.velocity.magnitude / _settings.Speed;
        //     
        //     for (int i = 0; i < 10; i++)
        //     {
        //         Gizmos.DrawWireSphere(_body.position + (transform.forward * length/10f * i), _settings.AvoidanceRadius);
        //     }
        // }
    }
}
