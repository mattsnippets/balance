﻿using System.Collections.Generic;
using _Balance._02_Scripts.BusinessLogic;
using _Balance._02_Scripts.Utilities;
using Lean.Pool;
using UnityEngine;

namespace _Balance._02_Scripts.Enemies
{
    public class SpiderNest : MonoBehaviour
    {
        [SerializeField] private GameObject _spiderPrefab;

        private SpiderSettings _settings;
        private readonly List<GameObject> _spiders = new List<GameObject>();

        private void OnEnable()
        {
            _settings = ServiceLocator.Resolve<Settings>().SpiderSettings;
            Spawn();
        }

        private void OnDisable()
        {
            DespawnSpiders();
        }

        private void Spawn()
        {
            GroundRaycaster.GetGroundPoint(transform.position, out var groundPoint);

            for (int i = 0; i < _settings.NestSpawnCount; i++)
            {
                _spiders.Add(LeanPool.Spawn(_spiderPrefab, groundPoint + Vector3.up * 6f, Quaternion.identity));
            }
        }
        
        private void DespawnSpiders()
        {
            foreach (var spider in _spiders)
            {
                LeanPool.Despawn(spider);
            }

            _spiders.Clear();
        }
    }
}
