﻿using System;
using _Balance._02_Scripts.BusinessLogic;
using _Balance._02_Scripts.Utilities;
using UnityEngine;
using DG.Tweening;

namespace _Balance._02_Scripts.Enemies
{
    public class SpiderLeg : MonoBehaviour
    {
        [SerializeField] private Transform _target;
        [SerializeField] private SpiderLeg[] _oppositeLegs;
        [SerializeField] private bool _showNextTargetDebug;

        private bool IsStepping { get; set; }
        public Transform Target => _target;
        public Transform NextTarget => _nextTarget;

        private SpiderSettings _settings;
        private LayerMask _groundMask;
        private readonly RaycastHit[] _raycastHit = new RaycastHit[1];
        private Transform _nextTarget;
        
        private Vector3 _targetOriginalPos;
        private Transform _targetOriginalParent;
        private Quaternion _targetOriginalRot;
        private Quaternion _originalRot;

        private void Awake()
        {
            ResetOriginalTransformStates();
        }

        private void OnEnable()
        {
            if (_settings == null)
            {
                _settings = ServiceLocator.Resolve<Settings>().SpiderSettings;
            }

            transform.rotation = _originalRot;
            _target.parent = _targetOriginalParent;
            _target.localPosition = _targetOriginalPos;
            _target.rotation = _targetOriginalRot;

            SetupTargets();
        }

        private void Update() => Step();

        private void ResetOriginalTransformStates()
        {
            _targetOriginalPos = _target.localPosition;
            _targetOriginalParent = _target.parent;
            _targetOriginalRot = _target.rotation;
            _originalRot = transform.localRotation;
        }

        private void Step()
        {
            if ((_target.position - _nextTarget.position).sqrMagnitude < Mathf.Pow(_settings.LegMaxDistance, 2))
            {
                return;
            }

            for (int i = 0; i < _oppositeLegs.Length; i++)
            {
                if (_oppositeLegs[i].IsStepping)
                {
                    return;
                }
            }

            IsStepping = true;

            var nextPos = _nextTarget.position;

            if (GroundRaycaster.GetGroundPoint(nextPos, out var groundPoint))
            {
                _nextTarget.position = new Vector3(
                    nextPos.x,
                    groundPoint.y + _settings.LegTargetVerticalOffset,
                    nextPos.z);
            }

            _target.DOJump(_nextTarget.position, _settings.LegJumpyness, 1, _settings.StepDuration)
                .OnComplete(() => IsStepping = false);
        }

        private void SetupTargets()
        {
            _groundMask = LayerMask.GetMask("Ground");

            Physics.RaycastNonAlloc(transform.position + Vector3.up * 100f, Vector3.down, _raycastHit, 200f, _groundMask);
            _target.position = _raycastHit[0].point + Vector3.up * _settings.LegTargetVerticalOffset;

            if (_nextTarget == null)
            {
                if (_showNextTargetDebug)
                {
                    var debugSphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                    debugSphere.GetComponent<Collider>().enabled = false;
                    _nextTarget = debugSphere.transform;
                }
                else
                {
                    _nextTarget = new GameObject("NextLegTarget").transform;
                }
            }

            _nextTarget.position =
                _target.position
                + _target.forward * _settings.NextLegTargetOffset
                + Vector3.up * _settings.LegTargetVerticalOffset;

            _nextTarget.parent = transform.parent;

            _target.parent = null;
        }
    }
}