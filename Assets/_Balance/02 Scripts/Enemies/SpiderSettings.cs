﻿using UnityEngine;

namespace _Balance._02_Scripts.Enemies
{
    [CreateAssetMenu(fileName = "SpiderSettings", menuName = "ScriptableObjects/SpiderSettings")]
    public class SpiderSettings : ScriptableObject
    {
        [Header("Movement")] public float Speed;
        public float TurnSpeed;
        public float SlowingDistance;
        public float MinMovementVectorSqMagnitude;
        public float LegMaxDistance;
        public float StepDuration;
        public float LegJumpyness;
        public float NextLegTargetOffset;
        public float LegTargetVerticalOffset;
        public float BodyYOffset;
        public float FlinchAmount;

        [Header("Avoidance")] public float AvoidanceRadius;
        public float MaxAvoidanceForce;
        public float SeparationCheckRadius;
        public float SeparationStrength;

        [Header("Patrolling")] public int PatrolPointCount;
        public float PatrolRadius;
        public float PatrolObstacleFreeRadius;
        public int PatrolGenerationTries;
        public double StuckCheckInterval;
        public float StuckThreshold;
        public float DetectionRadius;
        public float PatrolTargetReachedDistance;
       
        [Header("Chasing")] public float ChaseTargetReachedDistance;
        public float ChaseMaxDistance;
        public double AttackCooldown;
        public float AttackRange;
        public float AttackAngle;
        public float AttackDamage;

        [Header("Nests")] public float MinDistanceBetweenNests;
        public float NestClearRadius;
        public int NestSpawnCount;
    }
}