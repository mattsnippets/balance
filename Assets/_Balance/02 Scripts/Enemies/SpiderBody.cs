﻿using _Balance._02_Scripts.BusinessLogic;
using UnityEngine;

namespace _Balance._02_Scripts.Enemies
{
    public class SpiderBody : MonoBehaviour
    {
        [SerializeField] private Transform[] _legTargets;

        private LayerMask _groundMask;
        private SpiderSettings _settings;
        private Vector3 _originalPos;

        private void Awake()
        {
            _originalPos = new Vector3(0f, 0.7f, 0f);
        }

        private void Start()
        {
            _settings = ServiceLocator.Resolve<Settings>().SpiderSettings;
        }

        private void OnEnable()
        {
            transform.localPosition = _originalPos;
            transform.localRotation = Quaternion.identity;
        }

        private void Update()
        {
            SetBodyHeightAndTilt();
        }

        private void SetBodyHeightAndTilt()
        {
            var ownTransform = transform;
            
            Vector3 avgPos = Vector3.zero;
            Vector3 legsNormal = ownTransform.up;

            for (int i = 0; i < _legTargets.Length; i++)
            {
                avgPos += _legTargets[i].position;

                var legDir = _legTargets[i].position - transform.position;
                var normal = ownTransform.up;
                
                var tangent = legDir;
                Vector3.OrthoNormalize(ref normal, ref tangent);
                
                var fromTo = Quaternion.FromToRotation(tangent, legDir);
                fromTo = Quaternion.Lerp(Quaternion.identity, fromTo, 0.3f);

                legsNormal = fromTo * legsNormal;
            }

            avgPos /= _legTargets.Length;
            
            transform.position = new Vector3(
                transform.position.x,
                avgPos.y + _settings.BodyYOffset,
                ownTransform.position.z);

            legsNormal = Vector3.Dot(Vector3.up, legsNormal) <= 0f ? -legsNormal : legsNormal;

            transform.rotation = Quaternion.FromToRotation(Vector3.up, legsNormal)
                                 * Quaternion.LookRotation(transform.parent.forward);
        }
    }
}