﻿using _Balance._02_Scripts.Inventory.Items;
using _Balance._02_Scripts.Inventory.Placeables;
using _Balance._02_Scripts.ProceduralWorld.Props;
using UnityEngine;

namespace _Balance._02_Scripts.BusinessLogic
{
    public class ImplementationProvider : MonoBehaviour
    {
        [SerializeField] private ItemImplementationDictionary _implementations;
        [SerializeField] private PlaceableImplementationDictionary _placeableImplementations;
        [SerializeField] private PropImplementationDictionary _propImplementations;
        [SerializeField] private Trees _trees;

        public Trees Trees => _trees;

        public ItemImplementation GetItemImplementation(EItem item) => _implementations.Items[item];
        public PlaceableImplementation GetPlaceableImplementation(EPlaceable placeable) => _placeableImplementations.Items[placeable];
        public PropImplementation GetPropImplementation(EProp prop) => _propImplementations.Items[prop];
    }
}