﻿using UnityEngine;

namespace _Balance._02_Scripts.BusinessLogic
{
    public static class TimeScaleManager
    {
        public static void SetPauseState(bool paused)
        {
            Time.timeScale = paused ? 0f : 1f;
            Cursor.visible = paused;
            Cursor.lockState = paused ? CursorLockMode.None : CursorLockMode.Locked;
        }

        public static bool IsPaused => Time.timeScale == 0f;
    }
}