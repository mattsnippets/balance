﻿using UnityEngine;

namespace _Balance._02_Scripts.BusinessLogic
{
    public class GameSettings
    {
        public const float MinCameraSensitivity = 15f;
        public const float MaxCameraSensitivity = 105f;

        private float _cameraSensitivity;
        private bool _isMouseYInverted;
        
        public float CameraSensitivity
        {
            get => _cameraSensitivity;

            set
            {
                PlayerPrefs.SetFloat("CameraSensitivity", value);
                _cameraSensitivity = value;
            }
        }

        public bool IsMouseYInverted
        {
            get => _isMouseYInverted;
            
            set
            {
                PlayerPrefs.SetInt("IsMouseYInverted", value ? 1 : 0);
                _isMouseYInverted = value;
            }
        }

        public GameSettings()
        {
            _cameraSensitivity = PlayerPrefs.GetFloat("CameraSensitivity", 60f);
            _isMouseYInverted = PlayerPrefs.GetInt("IsMouseYInverted", 0) == 1;
        }
    }
}
