using System;
using System.IO;
using _Balance._02_Scripts.Entities;
using _Balance._02_Scripts.Inventory.CharacterPanel;
using _Balance._02_Scripts.Inventory.FileIO;
using _Balance._02_Scripts.Player;
using _Balance._02_Scripts.ProceduralWorld.Chunks;
using _Balance._02_Scripts.Ui;
using UniRx;
using UnityEngine;
using ZeroFormatter;

namespace _Balance._02_Scripts.BusinessLogic.GameSaves
{
    public class GameSaveManager : MonoBehaviour
    {
        [SerializeField] private string _filename;

        private GameState _gameState;
        private ChunkManager _chunkManager;
        private PlayerController _playerController;
        private ItemSaveManager _itemSaveManager;
        private InventoryController _inventoryController;
        private Gui _gui;

        private IDisposable _createdChunkRemovedListener;
        
        private void Start()
        {
            _gameState = ServiceLocator.Resolve<GameState>();
            _chunkManager = ServiceLocator.Resolve<ChunkManager>();
            _playerController = ServiceLocator.Resolve<PlayerController>();
            _itemSaveManager = ServiceLocator.Resolve<ItemSaveManager>();
            _inventoryController = ServiceLocator.Resolve<InventoryController>();
            _gui = ServiceLocator.Resolve<Gui>();
        }

        public void Save()
        {
            _gui.ToggleSavingOverlay(true);

            if (_chunkManager.ChunksBeingCreated.Count == 0)
            {
                DoSave();
            }
            else
            {
                _createdChunkRemovedListener?.Dispose();
                _createdChunkRemovedListener = _chunkManager.ChunksBeingCreated.ObserveRemove().Subscribe(_ =>
                {
                    if (_chunkManager.ChunksBeingCreated.Count == 0)
                    {
                        _createdChunkRemovedListener?.Dispose();
                        DoSave();
                    }
                }).AddTo(this);
            }
        }
        
        private void DoSave()
        {
            var playerRot = _playerController.transform.rotation;
            var cameraRot = Camera.main.transform.rotation;

            var savegame = new Savegame()
            {
                Balance = _gameState.Balance.Value,
                DayTime = _gameState.DayTime,
                PlayerPos = _playerController.transform.position,
                Seed = _gameState.RandomSeed,
                WorldName = "",
                InventoryItems = _itemSaveManager.SaveInventory(_inventoryController),
                ChunkData = _gameState.GetLatestChunkData(),
                PlayerChunkCoords = new Vector2(_chunkManager.PlayerXChunkCoord, _chunkManager.PlayerZChunkCoord),
                AmmoCount = FindObjectOfType<ProjectileHolder>().AmmoCount,
                Health = _playerController.GetComponent<Health>().Current,
                PlayerRotation = new Rotation(playerRot.x, playerRot.y, playerRot.z, playerRot.w),
                CameraRotation = new Rotation(cameraRot.x, cameraRot.y, cameraRot.z, cameraRot.w)
            };

            var bytes = ZeroFormatterSerializer.Serialize(savegame);
            var filePath = Application.persistentDataPath + "/" + _filename;
            File.WriteAllBytes(filePath, bytes);
            Debug.Log($"Game saved to: {filePath}");

            _gui.ToggleSavingOverlay(false);
            _gui.ShowGameSaved();
        }

        public Savegame Load() => Load(_filename);

        public Savegame Load(string filename)
        {
            var bytes = File.ReadAllBytes(Application.persistentDataPath + "/" + filename);
            Savegame save = ZeroFormatterSerializer.Deserialize<Savegame>(bytes);
            return save;
        }

        public void Delete() => Delete(_filename);

        private void Delete(string filename)
        {
            var filePath = Application.persistentDataPath + "/" + _filename;
            File.Delete(Application.persistentDataPath + "/" + filename);
            Debug.Log($"Save deleted: {filePath}");
        }
    }
}