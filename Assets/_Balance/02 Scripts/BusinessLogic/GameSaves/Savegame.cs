﻿using System.Collections.Generic;
using _Balance._02_Scripts.Inventory.FileIO;
using _Balance._02_Scripts.ProceduralWorld.Chunks;
using UnityEngine;
using ZeroFormatter;


[ZeroFormattable]
public class Savegame
{
    [Index(0)] public virtual string WorldName { get; set; } = "";
    [Index(1)] public virtual int Seed { get; set; }
    [Index(2)] public virtual float DayTime { get; set; }
    [Index(3)] public virtual float Balance { get; set; }
    [Index(4)] public virtual Vector3 PlayerPos { get; set; }
    [Index(5)] public virtual ItemContainerSaveData InventoryItems { get; set; }
    [Index(6)] public virtual Dictionary<Vector2, ChunkData> ChunkData { get; set; }
    [Index(7)] public virtual Vector2 PlayerChunkCoords { get; set; }
    [Index(8)] public virtual int AmmoCount { get; set; }
    [Index(9)] public virtual float Health { get; set; }
    [Index(10)] public virtual Rotation PlayerRotation { get; set; }
    [Index(11)] public virtual Rotation CameraRotation { get; set; }
}

[ZeroFormattable]
public class TypeHint
{
    [Index(0)] public virtual TileState[] Hint1 { get; set; }
    [Index(1)] public virtual Dictionary<Vector2, ChunkData> Hint2 { get; set; }
}


#if INCLUDE_ONLY_CODE_GENERATION
namespace UnityEngine
{
    [ZeroFormattable]
    public struct Vector2
    {
        [Index(0)]
        public float x;
        [Index(1)]
        public float y;

        public Vector2(float x, float y)
        {
            this.x = x;
            this.y = y;
        }
    }

    [ZeroFormattable]
    public struct Vector3
    {
        [Index(0)]
        public float x;
        [Index(1)]
        public float y;
        [Index(2)]
        public float z;

        public Vector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }
}

#endif