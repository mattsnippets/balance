﻿using System;
using System.Collections.Generic;

namespace _Balance._02_Scripts.BusinessLogic
{
    public static class ServiceLocator
    {
        private static readonly Dictionary<Type, object>
        Services = new Dictionary<Type, object>();

        public static void Register<T>(object serviceInstance) => Services[typeof(T)] = serviceInstance;

        public static T Resolve<T>() => (T)Services[typeof(T)];

        public static void Reset() => Services.Clear();
    }
}