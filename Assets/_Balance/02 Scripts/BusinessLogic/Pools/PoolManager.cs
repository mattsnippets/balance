﻿using System.Collections.Generic;
using Lean.Pool;
using UnityEngine;

namespace _Balance._02_Scripts.BusinessLogic.Pools
{
    public class PoolManager : MonoBehaviour
    {
        [SerializeField] private PoolConfig[] _poolConfigs;

        private List<LeanGameObjectPool> _pools = new List<LeanGameObjectPool>();

        private void CreatePools()
        {
            foreach (var config in _poolConfigs)
            {
                var pool = gameObject.AddComponent<LeanGameObjectPool>();
                pool.Prefab = config.Prefab;
                pool.Notification = config.NotificationType;
                pool.Strategy = config.StrategyType;
                pool.Preload = config.Preload;
                pool.Capacity = config.Capacity;
                pool.Recycle = config.Recycle;
                pool.Persist = config.Persist;
                pool.Stamp = config.Stamp;
                pool.Warnings = config.Warnings;
            }
        }

        private void Start()
        {
            CreatePools();
            
            foreach (var pool in GetComponentsInChildren<LeanGameObjectPool>())
            {
                pool.PreloadAll();
                _pools.Add(pool);
            }
        }
        
        private void OnDisable()
        {
            foreach (var pool in _pools)
            {
                pool.Clean();
            }
        }
    }
}