﻿using UnityEngine;
using static Lean.Pool.LeanGameObjectPool;

namespace _Balance._02_Scripts.BusinessLogic.Pools
{
    [CreateAssetMenu(fileName = "PoolConfig", menuName = "ScriptableObjects/PoolConfig")]
    public class PoolConfig : ScriptableObject
    {
        public GameObject Prefab;
        public NotificationType NotificationType;
        public StrategyType StrategyType;
        public int Preload;
        public int Capacity;
        public bool Recycle;
        public bool Persist;
        public bool Stamp;
        public bool Warnings;
    }
}
