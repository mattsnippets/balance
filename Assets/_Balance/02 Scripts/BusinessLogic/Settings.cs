﻿using System;
using _Balance._02_Scripts.Enemies;
using _Balance._02_Scripts.Inventory.ItemSpawning;
using _Balance._02_Scripts.ProceduralWorld;
using UnityEngine;

namespace _Balance._02_Scripts.BusinessLogic
{
	public class Settings : MonoBehaviour
	{
		public MapSettings MapSettings;
		public SpawnParameters SpawnParameters;
		public SpiderSettings SpiderSettings;
		public GameSettings GameSettings;

		private void Awake()
		{
			GameSettings = new GameSettings();
		}
	}
}
