﻿using System;
using System.Collections.Generic;
using System.IO;
using _Balance._02_Scripts.BusinessLogic.Events;
using _Balance._02_Scripts.BusinessLogic.GameSaves;
using _Balance._02_Scripts.Entities;
using _Balance._02_Scripts.Inventory.CharacterPanel;
using _Balance._02_Scripts.Inventory.FileIO;
using _Balance._02_Scripts.Player;
using _Balance._02_Scripts.ProceduralWorld.Chunks;
using _Balance._02_Scripts.ProceduralWorld.MapGenerator;
using _Balance._02_Scripts.ProceduralWorld.MiniMap;
using _Balance._02_Scripts.Ui;
using _Balance._02_Scripts.Utilities;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using ZeroFormatter.Formatters;
using Random = UnityEngine.Random;

namespace _Balance._02_Scripts.BusinessLogic
{
    public class GameState : MonoBehaviour
    {
        [Range(0, 1)] [SerializeField] private float _dayTime = 0f;
        [SerializeField] private float _dayLength = 20f;
        [SerializeField] private bool _pauseDayTime;
        [SerializeField] private bool _generateSeedOnEveryRun;
        [SerializeField] private GameObject _player;
        [SerializeField] private GameObject _loadingOverlay;
        [SerializeField] private GameObject _helpOverlay;

        public bool IsWorldInitialized { get; private set; }
        public ReactiveProperty<float> Balance { get; private set; } = new ReactiveProperty<float>(0f);
        public ReactiveProperty<float> PlayerHealth { get; private set; } = new ReactiveProperty<float>(100f);
        public float DayTime => _dayTime;
        public int RandomSeed { get; private set; }
        public Vector3 PlayerStartPos { get; private set; }
        public Vector2 PlayerChunkCoords { get; private set; }
        public ItemContainerSaveData InventoryContents { get; private set; }
        public Dictionary<Vector2, ChunkData> ChunkData { get; private set; } = new Dictionary<Vector2, ChunkData>();
        public float DayLength => _dayLength;
        public Quaternion CameraRot { get; private set; }

        private int _ammoCount;
        private float _health = 100f;
        private bool _isFreshStart;
        private Quaternion _playerRot;

        private void Awake()
        {
            _loadingOverlay.SetActive(true);
        }

        private void Start()
        {
            RegisterServices();
            Tile.MapSettings = ServiceLocator.Resolve<Settings>().MapSettings;
            Formatter.RegisterDictionary<DefaultResolver, Vector2, ChunkData>();

            SetRandomSeed();

            BalanceChangeEvent.AddListener(OnBalanceChanged);
            PlayerHealthChangeEvent.AddListener(OnPlayerHealthChanged);

            TryLoadingSavegame();

            _player.transform.position = new Vector3(PlayerStartPos.x, 200f, PlayerStartPos.z);

            ServiceLocator.Resolve<ChunkManager>().OnAllStartChunksInitialized.Subscribe(_ => { FinalizeWorldInit(); }).AddTo(this);
        }

        private void FinalizeWorldInit()
        {
            GroundRaycaster.GetGroundPoint(new Vector3(PlayerStartPos.x, 200f, PlayerStartPos.z), out var groundPoint);
            _player.transform.position = groundPoint;
            _player.transform.rotation = _playerRot;
            _player.SetActive(true);
            
            var playerController = ServiceLocator.Resolve<PlayerController>();
            playerController.GetAmmo(_ammoCount);
            
            var health = playerController.GetComponent<Health>();
            health.Add(_health - health.Current);

            _loadingOverlay.SetActive(false);
            _helpOverlay.SetActive(_isFreshStart);

            IsWorldInitialized = true;
        }

        private void OnDisable()
        {
            BalanceChangeEvent.RemoveListener(OnBalanceChanged);
            PlayerHealthChangeEvent.RemoveListener(OnPlayerHealthChanged);
            TimeScaleManager.SetPauseState(false);
        }

        public void ReloadWorld()
        {
            AsyncOperation load = SceneManager.LoadSceneAsync("LoadingScreen");
            load.completed += operation => { SceneManager.SetActiveScene(SceneManager.GetSceneByName("LoadingScreen")); };
        }

        public Dictionary<Vector2, ChunkData> GetLatestChunkData()
        {
            foreach (var chunk in ServiceLocator.Resolve<ChunkManager>().ActiveChunks)
            {
                StoreChunkData(chunk.ChunkPosition, chunk.GetChunkData());
            }

            return ChunkData;
        }

        public void StoreChunkData(Vector2 chunkPos, ChunkData data)
        {
            ChunkData[chunkPos] = data;
        }

        public ChunkData GetChunkData(Vector2 chunkPos)
        {
            ChunkData.TryGetValue(chunkPos, out var data);
            return data;
        }

        public void Quit() => Application.Quit();
        
        private void SetRandomSeed()
        {
            if (_generateSeedOnEveryRun)
            {
                RandomSeed = Random.Range(0, int.MaxValue);
            }
            else
            {
                RandomSeed = GetStoredWorldSeed();
            }
        }

        private void TryLoadingSavegame()
        {
            try
            {
                var save = ServiceLocator.Resolve<GameSaveManager>().Load("Save001");

                Balance.Value = save.Balance;
                _dayTime = save.DayTime;
                RandomSeed = save.Seed;
                PlayerStartPos = save.PlayerPos;
                InventoryContents = save.InventoryItems;
                ChunkData = save.ChunkData;
                PlayerChunkCoords = save.PlayerChunkCoords;
                _ammoCount = save.AmmoCount;
                _health = save.Health;
                _playerRot = new Quaternion(
                    save.PlayerRotation.X,
                    save.PlayerRotation.Y,
                    save.PlayerRotation.Z,
                    save.PlayerRotation.W);
                CameraRot = new Quaternion(
                    save.CameraRotation.X,
                    save.CameraRotation.Y,
                    save.CameraRotation.Z,
                    save.CameraRotation.W);
            }
            catch (FileNotFoundException)
            {
                Debug.LogWarning("Could not find save file");
                _isFreshStart = true;
            }
        }

        private int GetStoredWorldSeed() => PlayerPrefs.GetInt("WorldSeed", 1463598377);

        private void RegisterServices()
        {
            ServiceLocator.Reset();

            ServiceLocator.Register<GameSaveManager>(FindObjectOfType<GameSaveManager>());
            ServiceLocator.Register<GameState>(FindObjectOfType<GameState>());
            ServiceLocator.Register<ChunkManager>(FindObjectOfType<ChunkManager>());
            ServiceLocator.Register<ItemSaveManager>(FindObjectOfType<ItemSaveManager>());
            ServiceLocator.Register<ImplementationProvider>(FindObjectOfType<ImplementationProvider>());
            ServiceLocator.Register<Settings>(FindObjectOfType<Settings>());
            ServiceLocator.Register<MapGenerator>(new MapGenerator());
            ServiceLocator.Register<InventoryController>(_player.GetComponent<InventoryController>());
            ServiceLocator.Register<InventoryInput>(FindObjectOfType<InventoryInput>());
            ServiceLocator.Register<MiniMap>(FindObjectOfType<MiniMap>());
            ServiceLocator.Register<PlayerController>(_player.GetComponent<PlayerController>());
            ServiceLocator.Register<Gui>(FindObjectOfType<Gui>());
        }

        private void ProgressDayTime()
        {
            if (!_pauseDayTime)
            {
                _dayTime += Time.deltaTime / _dayLength;
            }

            if (_dayTime >= 1)
            {
                _dayTime = 0;
            }
        }

        private void OnPlayerHealthChanged(PlayerHealthChangeEvent eventData) =>
            PlayerHealth.Value = eventData.CurrentHealth;

        private void OnBalanceChanged(BalanceChangeEvent eventData) =>
            Balance.Value = Mathf.Clamp(Balance.Value + eventData.Amount, -100f, 100f);
    }
}
