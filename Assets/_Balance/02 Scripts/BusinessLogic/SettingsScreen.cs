﻿using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace _Balance._02_Scripts.BusinessLogic
{
    public class SettingsScreen : MonoBehaviour
    {
        [SerializeField] private Slider _mouseSensitivitySlider;
        [SerializeField] private Toggle _invertMouseYAxisToggle;

        private GameSettings _gameSettings;

        private void Start()
        {
            _mouseSensitivitySlider.OnValueChangedAsObservable().Subscribe(sensitivity =>
            {
                _gameSettings.CameraSensitivity = sensitivity;
            }).AddTo(this);

            _invertMouseYAxisToggle.OnValueChangedAsObservable().Subscribe(isYInverted =>
            {
                _gameSettings.IsMouseYInverted = isYInverted;
            }).AddTo(this);
        }

        private void OnEnable()
        {
            if (_gameSettings == null)
            {
                _gameSettings = ServiceLocator.Resolve<Settings>().GameSettings;
            }

            _mouseSensitivitySlider.minValue = GameSettings.MinCameraSensitivity;
            _mouseSensitivitySlider.maxValue = GameSettings.MaxCameraSensitivity;
            _mouseSensitivitySlider.SetValueWithoutNotify(_gameSettings.CameraSensitivity);
            _invertMouseYAxisToggle.isOn = _gameSettings.IsMouseYInverted;
        }
    }
}
