﻿namespace _Balance._02_Scripts.BusinessLogic.Events
{
	public class BalanceChangeEvent : Event<BalanceChangeEvent>
	{
		public float Amount;

		public BalanceChangeEvent(float amount)
		{
			name = "BalanceChangeEvent";
			Amount = amount;
		}
	}

	public class PlayerHealthChangeEvent : Event<PlayerHealthChangeEvent>
	{
		public float CurrentHealth;

		public PlayerHealthChangeEvent(float currentHealth)
		{
			name = "PlayerHealthChangeEvent";
			CurrentHealth = currentHealth;
		}
	}

	public class PlayerDiedEvent : Event<PlayerDiedEvent>
	{
		public PlayerDiedEvent() => name = "PlayerDiedEvent";
	}
}
