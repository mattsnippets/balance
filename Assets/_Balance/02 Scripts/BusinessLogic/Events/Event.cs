﻿namespace _Balance._02_Scripts.BusinessLogic.Events
{
	public abstract class Event<T> where T : Event<T>
	{
		public string Description { get; set; }
		protected string name;

		public delegate void EventListener(T eventData);
		private static event EventListener listeners;

		public static void AddListener(EventListener listener)
		{
			listeners += listener;
		}

		public static void RemoveListener(EventListener listener)
		{
			listeners -= listener;
		}

		public void Fire()
		{
			//Debug.Log( $"Event fired: {name}, {Description}");
			listeners?.Invoke(this as T);
		}
	}
}