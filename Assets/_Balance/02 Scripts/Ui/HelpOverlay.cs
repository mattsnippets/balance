﻿using UnityEngine;

namespace _Balance._02_Scripts.Ui
{
    public class HelpOverlay : MonoBehaviour
    {
        private void Update()
        {
            if (
                Input.GetKeyDown(KeyCode.Return)
                || Input.GetKeyDown(KeyCode.KeypadEnter)
                || Input.GetMouseButtonUp(0))
            {
                gameObject.SetActive(false);
            }
        }
    }
}