﻿using UnityEngine;

namespace _Balance._02_Scripts.Ui
{
    public class Hourglass : MonoBehaviour
    {
        private void Update()
        {
            transform.Rotate(0f, 0f, -180f * Time.deltaTime);
        }
    }
}