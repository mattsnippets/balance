﻿using System.Collections.Generic;
using _Balance._02_Scripts.BusinessLogic;
using _Balance._02_Scripts.BusinessLogic.Events;
using _Balance._02_Scripts.Inventory.CharacterPanel;
using _Balance._02_Scripts.ProceduralWorld.MiniMap;
using DG.Tweening;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace _Balance._02_Scripts.Ui
{
    public class Gui : MonoBehaviour
    {
        [Header("Status")] [SerializeField] private TextMeshProUGUI _balanceAmount;
        [SerializeField] private TextMeshProUGUI _healthAmount;
        [SerializeField] private Image _healthBar;
        [SerializeField] private GameObject _deathOverlay;
        [SerializeField] private GameObject _savingOverlay;
        [SerializeField] private TextMeshProUGUI _gameSavedNotification;
        [SerializeField] private TextMeshProUGUI _versionLabel;

        [Header("Minimap")] [SerializeField] private GameObject _miniMapDisplay;
        [SerializeField] private MiniMap _miniMap;
        [SerializeField] private Slider _zoomSlider;

        [Header("Main Menu")] [SerializeField] private GameObject _menu;
        [SerializeField] private TMP_InputField _seedInputField;
        [SerializeField] private Button _saveButton;
        [SerializeField] private GameObject _settingsMenu;

        private GameState _gameState;
        private InventoryInput _inventoryInput;
        private readonly Stack<EGuiPanel> _guiPanels = new Stack<EGuiPanel>();

        private void Start()
        {
            _gameState = ServiceLocator.Resolve<GameState>();
            _inventoryInput = ServiceLocator.Resolve<InventoryInput>();
            _gameState.Balance.Subscribe(balance => _balanceAmount.text = balance.ToString());
            _gameState.PlayerHealth.Subscribe(health =>
            {
                _healthAmount.text = health.ToString();
                _healthBar.fillAmount = health / 100f;
            });
            _seedInputField.text = _gameState.RandomSeed.ToString();

            PlayerDiedEvent.AddListener(OnPlayerDied);

            _versionLabel.text = Application.version;
        }

        private void Update()
        {
            if (!_gameState.IsWorldInitialized)
            {
                return;
            }

            var topPanel = _guiPanels.Count > 0 ? _guiPanels.Peek() : EGuiPanel.None;

            if (Input.GetKeyDown(KeyCode.Tab) || Input.GetKeyDown(KeyCode.M))
            {
                ToggleMinimap(topPanel);
            }

            if (Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown(KeyCode.I))
            {
                ToggleInventory(topPanel);
            }

            if (Input.GetKeyDown(KeyCode.F10))
            {
                ToggleMainMenu(topPanel);
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (_guiPanels.Count > 0)
                {
                    switch (topPanel)
                    {
                        case EGuiPanel.MainMenu:
                            ToggleMainMenu(topPanel);
                            break;
                        case EGuiPanel.Minimap:
                            ToggleMinimap(topPanel);
                            break;
                        case EGuiPanel.Inventory:
                            ToggleInventory(topPanel);
                            break;
                        case EGuiPanel.Settings:
                            ToggleSettings();
                            break;
                    }
                }
                else
                {
                    ToggleMainMenu(topPanel);
                }
            }

            if (topPanel == EGuiPanel.Minimap)
            {
                HandleMinimapZoom();
            }
        }

        private void HandleMinimapZoom()
        {
            if (Input.GetAxis("Mouse ScrollWheel") > 0f)
            {
                _zoomSlider.value += 0.1f;
            }
            else if (Input.GetAxis("Mouse ScrollWheel") < 0f)
            {
                _zoomSlider.value -= 0.1f;
            }
        }

        private void ToggleMainMenu(EGuiPanel topPanel)
        {
            if (topPanel == EGuiPanel.MainMenu)
            {
                _menu.SetActive(false);
                _guiPanels.Pop();

                if (_guiPanels.Count == 0)
                {
                    TimeScaleManager.SetPauseState(false);
                }
            }
            else if (!_menu.activeInHierarchy)
            {
                _menu.SetActive(true);
                _guiPanels.Push(EGuiPanel.MainMenu);
                TimeScaleManager.SetPauseState(true);
            }
        }

        public void ToggleSettings()
        {
            var topPanel = _guiPanels.Count > 0 ? _guiPanels.Peek() : EGuiPanel.None;
            
            if (topPanel == EGuiPanel.Settings)
            {
                _settingsMenu.SetActive(false);
                _guiPanels.Pop();
            }
            else if (topPanel == EGuiPanel.MainMenu && !_settingsMenu.activeInHierarchy)
            {
                _settingsMenu.SetActive(true);
                _guiPanels.Push(EGuiPanel.Settings);
            }
        }

        private void ToggleInventory(EGuiPanel topPanel)
        {
            if (topPanel == EGuiPanel.MainMenu)
            {
                return;
            }

            if (topPanel == EGuiPanel.Inventory)
            {
                _inventoryInput.SetInventoryPanelOpen(false);
                _guiPanels.Pop();

                if (_guiPanels.Count == 0)
                {
                    TimeScaleManager.SetPauseState(false);
                }
            }
            else if (topPanel == EGuiPanel.None && !_inventoryInput.IsPanelOpen)
            {
                _inventoryInput.SetInventoryPanelOpen(true);
                _guiPanels.Push(EGuiPanel.Inventory);
                TimeScaleManager.SetPauseState(true);
            }
        }

        private void ToggleMinimap(EGuiPanel topPanel)
        {
            if (topPanel == EGuiPanel.MainMenu)
            {
                return;
            }

            if (topPanel == EGuiPanel.Minimap)
            {
                _miniMapDisplay.SetActive(false);
                _guiPanels.Pop();

                if (_guiPanels.Count == 0)
                {
                    TimeScaleManager.SetPauseState(false);
                }
            }
            else if (topPanel == EGuiPanel.None && !_miniMapDisplay.activeInHierarchy)
            {
                _miniMapDisplay.SetActive(true);
                _guiPanels.Push(EGuiPanel.Minimap);
                _miniMap.UpdateActiveTiles();
                TimeScaleManager.SetPauseState(true);
            }
        }

        private void OnDestroy() => PlayerDiedEvent.RemoveListener(OnPlayerDied);

        private void OnPlayerDied(PlayerDiedEvent eventdata)
        {
            _saveButton.interactable = false;
            _deathOverlay.SetActive(true);
        }

        public void StoreWorldSeed() => PlayerPrefs.SetInt("WorldSeed", int.Parse(_seedInputField.text));
        public void ResumeGame() => ToggleMainMenu(_guiPanels.Count > 0 ? _guiPanels.Peek() : EGuiPanel.None);
        public void ToggleSavingOverlay(bool isActive) => _savingOverlay.SetActive(isActive);

        public void ShowGameSaved()
        {
            _gameSavedNotification.alpha = 1f;
            _gameSavedNotification.gameObject.SetActive(true);
            _gameSavedNotification.DOFade(0f, 2f).OnComplete(() =>
                _gameSavedNotification.gameObject.SetActive(false));
        }
    }
    public enum EGuiPanel
    {
        None = 0,
        MainMenu = 1,
        Minimap = 2,
        Inventory = 3,
        Settings = 4
    }
}
