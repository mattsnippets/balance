﻿using UnityEngine;

namespace _Balance._02_Scripts.Ui
{

	/// <summary>
	/// Show a simple text with the current FPS and resolution values.
	/// </summary>

	public class FpsDisplay : MonoBehaviour
	{
		float deltaTime;

		private void Awake()
		{
#if UNITY_EDITOR
			Application.targetFrameRate = 60;
#endif
		}

		void Update()
		{
			deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
		}

		void OnGUI()
		{
			int w = Screen.width, h = Screen.height;

			GUIStyle style = new GUIStyle();

			Rect rect = new Rect(0, 0, w, h * 2 / 100);
			style.alignment = TextAnchor.UpperLeft;
			style.fontSize = h * 2 / 100;
			style.normal.textColor = Color.magenta;
			var msec = deltaTime * 1000.0f;
			var fps = 1.0f / deltaTime;
			var text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
			GUI.Label(rect, text, style);

			style.alignment = TextAnchor.UpperRight;
			GUI.Label(rect, Screen.currentResolution.ToString(), style);
		}
	}
}