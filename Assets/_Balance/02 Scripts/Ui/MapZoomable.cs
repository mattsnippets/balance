﻿using UnityEngine;

namespace _Balance._02_Scripts.Ui
{
	public class MapZoomable : MonoBehaviour
	{
		private RectTransform _rectTransform;

		private void Awake()
		{
			_rectTransform = GetComponent<RectTransform>();
		}

		public void SetZoom(float zoom)
		{
			_rectTransform.localScale = new Vector3(zoom, zoom, 1f);
		}
	}
}