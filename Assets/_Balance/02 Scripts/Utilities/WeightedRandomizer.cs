﻿namespace _Balance._02_Scripts.Utilities
{
	public class WeightedRandomizer
	{
		private float[] _list;
		private float _totalChance;
		private System.Random _generator;

		public WeightedRandomizer(int seed)
		{
			_generator = new System.Random(seed);
		}

		public void InitWithNormalize(float[] list)
		{
			_totalChance = 0.0f;

			for (int i = 0; i < list.Length; i++)
			{
				_totalChance += list[i];
			}

			for (int i = 0; i < list.Length; i++)
			{
				list[i] /= _totalChance;
			}

			_list = list;
		}

		public void Init(float[] list) => _list = list;

		public int GetWeightedIndex()
		{
			double p = _generator.NextDouble();
			double cp = 0.0f;

			for (int i = 0; i < _list.Length; i++)
			{
				cp += _list[i];
				if (p <= cp)
				{
					return i;
				}
			}

			return -1;
		}
	}
}