﻿using System;
using _Balance._02_Scripts.Inventory.Items;
using _Balance._02_Scripts.ProceduralWorld.Props;

namespace _Balance._02_Scripts.Utilities.SerializableDictionaries
{
    [Serializable]
    public class EPropPropImplementationDictionary : SerializableDictionary<EProp, PropImplementation> { }
}