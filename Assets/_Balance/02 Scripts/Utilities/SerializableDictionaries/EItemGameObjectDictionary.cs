﻿using System;
using _Balance._02_Scripts.Inventory.Items;
using UnityEngine;

namespace _Balance._02_Scripts.Utilities.SerializableDictionaries
{
	[Serializable]
	public class EItemGameObjectDictionary : SerializableDictionary<EItem, GameObject> { }
}