﻿using System;
using _Balance._02_Scripts.Inventory.Items;

namespace _Balance._02_Scripts.Utilities.SerializableDictionaries
{
	[Serializable]
	public class EItemItemImplementationDictionary : SerializableDictionary<EItem, ItemImplementation> { }
}