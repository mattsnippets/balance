﻿using System;
using _Balance._02_Scripts.ProceduralWorld.Chunks;

namespace _Balance._02_Scripts.Utilities.SerializableDictionaries
{
    [Serializable]
    public class FloatElevationDictionary : SerializableDictionary<float, Elevation> { }
}
