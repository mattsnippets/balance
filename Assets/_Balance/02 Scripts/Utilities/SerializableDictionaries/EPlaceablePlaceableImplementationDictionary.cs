﻿using System;
using _Balance._02_Scripts.Inventory.Items;
using _Balance._02_Scripts.Inventory.Placeables;

namespace _Balance._02_Scripts.Utilities.SerializableDictionaries
{
	[Serializable]
	public class EPlaceablePlaceableImplementationDictionary : SerializableDictionary<EPlaceable, PlaceableImplementation> { }
}