﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Balance._02_Scripts.Utilities
{
    public class LoadingScreen : MonoBehaviour
    {
        private void Start()
        {
            //SceneManager.sceneUnloaded += scene => Debug.Log($"{scene.name}");

            AsyncOperation cleanup = Resources.UnloadUnusedAssets();

            cleanup.completed += cleanupOperation =>
            {
                AsyncOperation load = SceneManager.LoadSceneAsync("Main");
                load.completed += operation => { SceneManager.SetActiveScene(SceneManager.GetSceneByName("Main")); };
            };
        }
    }
}