﻿using System;
using Lean.Pool;
using UniRx;
using UnityEngine;

namespace _Balance._02_Scripts.Utilities
{
    public class ParticleAutoDespawn : MonoBehaviour
    {
        private ParticleSystem _particleSystem;

        private void Awake()
        {
            _particleSystem = GetComponent<ParticleSystem>();
        }

        private void OnEnable()
        {
            _particleSystem.Play();

            Observable.Timer(TimeSpan.FromSeconds(_particleSystem.main.duration)).Subscribe(_ =>
                LeanPool.Despawn(gameObject)).AddTo(this);
        }
    }
}
