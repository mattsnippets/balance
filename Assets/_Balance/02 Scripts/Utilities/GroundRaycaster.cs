﻿using UnityEngine;

namespace _Balance._02_Scripts.Utilities
{
    public static class GroundRaycaster
    {
        private static readonly LayerMask _groundMask;
        private static readonly RaycastHit[] _raycastHit = new RaycastHit[1];

        static GroundRaycaster() => _groundMask = LayerMask.GetMask("Ground");

        public static bool GetGroundPoint(Vector3 position, out Vector3 groundPoint)
        {
            groundPoint = Vector3.zero;
        
            if (Physics.RaycastNonAlloc(
                position + Vector3.up * 200f,
                Vector3.down,
                _raycastHit,
                400f,
                _groundMask) > 0)
            {
                groundPoint = _raycastHit[0].point;
                return true;
            }

            return false;
        }
    }
}