﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace _Balance._02_Scripts.Utilities
{
    [RequireComponent(typeof(Text))]

    public class DebugShowUnderCursor : MonoBehaviour
    {
        Text text;
        EventSystem eventSystem;
        List<RaycastResult> list;
        void Start()
        {
            eventSystem = EventSystem.current;
            text = GetComponent<Text>();
            text.raycastTarget = false;
        }

        public List<RaycastResult> RaycastMouse()
        {
            PointerEventData pointerData = new PointerEventData(EventSystem.current) { pointerId = -1, };
            pointerData.position = Input.mousePosition;
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pointerData, results);
            return results;
        }
        void Update()
        {
            list = RaycastMouse();
            var objects = "";
            foreach (RaycastResult result in list)
            {
                objects += result.gameObject.name + "\n";
            }

            text.text = objects;
        }
    }
}