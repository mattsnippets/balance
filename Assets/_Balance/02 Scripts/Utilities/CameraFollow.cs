﻿using System;
using _Balance._02_Scripts.BusinessLogic;
using UnityEngine;

namespace _Balance._02_Scripts.Utilities
{
    public class CameraFollow : MonoBehaviour
    {
        [SerializeField] private Transform _focus;
        [SerializeField] [Range(1f, 20f)] private float _distance = 5f;
        [SerializeField] [Min(0f)] private float _focusRadius = 1f;
        [SerializeField] [Range(0f, 1f)] private float _focusCentering = 0.5f;
        [SerializeField] [Range(-89f, 89f)] private float _minVerticalAngle = -30f;
        [SerializeField] [Range(-89f, 89f)] private float _maxVerticalAngle = 60f;
        [SerializeField] [Min(0f)] private float _alignDelay = 5f;
        [SerializeField] [Range(0f, 90f)] private float _alignSmoothRange = 45f;

        private Vector3 _focusPoint;
        private Vector3 _prevFocusPoint;
        private Vector2 _orbitAngles = new Vector2(25f, 0f);
        private float _lastManualRot;
        private RaycastHit[] _cameraRaycastHits = new RaycastHit[1];
        private GameSettings _gameSettings;

        private void Awake()
        {
            _focusPoint = _focus.position;
        }

        private void Start()
        {
            _orbitAngles = ServiceLocator.Resolve<GameState>().CameraRot.eulerAngles;
            _gameSettings = ServiceLocator.Resolve<Settings>().GameSettings;
            transform.localRotation = Quaternion.Euler(_orbitAngles);
        }


        private void OnValidate()
        {
            if (_maxVerticalAngle < _minVerticalAngle)
            {
                _maxVerticalAngle = _minVerticalAngle;
            }
        }

        private void LateUpdate()
        {
            UpdateFocusPoint();
            Quaternion lookRot;

            if (ManualRotation() || AutomaticRotation())
            {
                ConstrainAngles();
                lookRot = Quaternion.Euler(_orbitAngles);
            }
            else
            {
                lookRot = transform.localRotation;
            }

            Vector3 lookDir = lookRot * Vector3.forward;
            Vector3 lookPos = _focusPoint - lookDir * _distance;

            if (Physics.RaycastNonAlloc(transform.position, Vector3.down, _cameraRaycastHits, 100f, LayerMask.GetMask("Ground")) > 0)
            {
                lookPos = new Vector3(lookPos.x, Mathf.Max(lookPos.y, _cameraRaycastHits[0].point.y + 1f), lookPos.z);
            }

            transform.SetPositionAndRotation(lookPos, lookRot);
        }

        private void UpdateFocusPoint()
        {
            _prevFocusPoint = _focusPoint;
            Vector3 targetPoint = _focus.position;

            if (_focusRadius > 0f)
            {
                float currentDistance = Vector3.Distance(targetPoint, _focusPoint);

                float t = 1f;

                if (currentDistance > 0.01f && _focusCentering > 0f)
                {
                    t = Mathf.Pow(1f - _focusCentering, Time.deltaTime);
                }

                if (currentDistance > _focusRadius)
                {
                    t = Mathf.Min(t, _focusRadius / currentDistance);
                }

                _focusPoint = Vector3.Lerp(targetPoint, _focusPoint, t);
            }
            else
            {
                _focusPoint = targetPoint;
            }
        }

        private bool ManualRotation()
        {
            Vector2 input = new Vector2(
                Input.GetAxis("Mouse Y") * (_gameSettings.IsMouseYInverted ? 1f : -1f),
                Input.GetAxis("Mouse X")
            );

            const float e = 0.001f;

            if (input.x < -e || input.x > e || input.y < -e || input.y > e)
            {
                _orbitAngles += _gameSettings.CameraSensitivity * Time.deltaTime * input;
                _lastManualRot = Time.time;
                return true;
            }

            return false;
        }

        private void ConstrainAngles()
        {
            _orbitAngles.x =
                Mathf.Clamp(_orbitAngles.x, _minVerticalAngle, _maxVerticalAngle);

            if (_orbitAngles.y < 0f)
            {
                _orbitAngles.y += 360f;
            }
            else if (_orbitAngles.y >= 360f)
            {
                _orbitAngles.y -= 360f;
            }
        }

        private bool AutomaticRotation()
        {
            if (Time.time - _lastManualRot < _alignDelay)
            {
                return false;
            }

            Vector2 movement = new Vector2(
                _focusPoint.x - _prevFocusPoint.x,
                _focusPoint.z - _prevFocusPoint.z
            );

            float movementDeltaSqr = movement.sqrMagnitude;

            if (movementDeltaSqr < 0.000001f)
            {
                return false;
            }

            float headingAngle = GetAngle(movement / Mathf.Sqrt(movementDeltaSqr));
            float deltaAbs = Mathf.Abs(Mathf.DeltaAngle(_orbitAngles.y, headingAngle));
            float rotationChange = _gameSettings.CameraSensitivity * Mathf.Min(Time.deltaTime, movementDeltaSqr);

            if (deltaAbs < _alignSmoothRange)
            {
                rotationChange *= deltaAbs / _alignSmoothRange;
            }
            else if (180f - deltaAbs < _alignSmoothRange)
            {
                rotationChange *= (180f - deltaAbs) / _alignSmoothRange;
            }

            _orbitAngles.y =
                Mathf.MoveTowardsAngle(_orbitAngles.y, headingAngle, rotationChange);

            return true;
        }

        private static float GetAngle(Vector2 direction)
        {
            float angle = Mathf.Acos(direction.y) * Mathf.Rad2Deg;
            return direction.x < 0f ? 360f - angle : angle;
        }
    }
}
