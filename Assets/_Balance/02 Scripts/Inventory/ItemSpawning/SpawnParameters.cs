﻿using System;
using _Balance._02_Scripts.Inventory.Items;
using _Balance._02_Scripts.ProceduralWorld.Chunks;
using UnityEngine;

namespace _Balance._02_Scripts.Inventory.ItemSpawning
{
	[Serializable]
	public struct SpawnData
	{
		public EItem Type;
		[Range(0f,1f)]
		public float Chance;
		public GameObject ItemPrefab;
		public TileState TileState;
	}

	[CreateAssetMenu(fileName = "SpawnParameters", menuName = "ScriptableObjects/Spawn Parameters")]
	public class SpawnParameters : ScriptableObject
	{
		public SpawnData[] _spawnData;
	}
}