﻿using System;
using ZeroFormatter;

namespace _Balance._02_Scripts.Inventory.FileIO
{
	[ZeroFormattable]
	[Serializable]
	public class ItemSlotSaveData
	{
		[Index(0)]
		public virtual string ItemID { get; set; }

		[Index(1)]
		public virtual int Amount { get; set; }
	}

	[ZeroFormattable]
	[Serializable]
	public class ItemContainerSaveData
	{
		[Index(0)]
		public virtual ItemSlotSaveData[] SavedSlots { get; set; }
	}
}
