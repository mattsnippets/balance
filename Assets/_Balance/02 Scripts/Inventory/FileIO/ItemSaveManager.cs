﻿using System.Collections.Generic;
using _Balance._02_Scripts.BusinessLogic;
using _Balance._02_Scripts.Inventory.CharacterPanel;
using _Balance._02_Scripts.Inventory.Items;
using UnityEngine;

namespace _Balance._02_Scripts.Inventory.FileIO
{
	public class ItemSaveManager : MonoBehaviour
	{
		[SerializeField] ItemDatabase itemDatabase;

		private const string InventoryFileName = "Inventory";

		public void LoadItems(ItemContainerSaveData source, IItemContainer itemContainer)
		{
			ItemContainerSaveData saveData = source;

			if (saveData == null || saveData.SavedSlots == null)
			{
				return;
			}

			itemContainer.Clear();

			for (var i = 0; i < saveData.SavedSlots.Length; i++)
			{
				ItemSlot itemSlot = itemContainer.GetItemSlots()[i];
				ItemSlotSaveData savedSlot = saveData.SavedSlots[i];

				if (savedSlot == null)
				{
					itemSlot.Item = null;
					itemSlot.Amount = 0;
				}
				else
				{
					itemSlot.Item = itemDatabase.GetItemCopy(savedSlot.ItemID);
					itemSlot.Amount = savedSlot.Amount;
				}
			}
		}

		public void LoadInventoryItems(IItemContainer inventory) => 
			LoadItems(ServiceLocator.Resolve<GameState>().InventoryContents, inventory);

		public ItemContainerSaveData SaveInventory(InventoryController controller) => 
			SaveItems(controller.Inventory.ItemSlots);

		public ItemContainerSaveData SaveItems(IList<ItemSlot> itemSlots)
		{
			var saveData = new ItemContainerSaveData()
			{
				SavedSlots = new ItemSlotSaveData[itemSlots.Count]
			};

			saveData.SavedSlots = new ItemSlotSaveData[itemSlots.Count];

			for (var i = 0; i < saveData.SavedSlots.Length; i++)
			{
				ItemSlot itemSlot = itemSlots[i];

				if (itemSlot.Item == null)
				{
					saveData.SavedSlots[i] = null;
				}
				else
				{
					saveData.SavedSlots[i] = new ItemSlotSaveData()
					{
						Amount = itemSlot.Amount,
						ItemID = itemSlot.Item.ID
					};
				}
			}

			return saveData;
		}
	}
}
