﻿using System;
using System.Collections.Generic;
using _Balance._02_Scripts.Inventory.CharacterPanel;
using _Balance._02_Scripts.Inventory.Items;
using UnityEngine;

namespace _Balance._02_Scripts.Inventory.CraftingSystem
{
	public class CraftingRecipeUI : MonoBehaviour
	{
		[Header("References")]
		[SerializeField] RectTransform arrowParent;
		[SerializeField] BaseItemSlot[] itemSlots;

		[Header("Public Variables")]
		public ItemContainer ItemContainer;

		private CraftingRecipe _craftingRecipe;
		public CraftingRecipe CraftingRecipe
		{
			get { return _craftingRecipe; }
			set { SetCraftingRecipe(value); }
		}

		public event Action<BaseItemSlot> OnPointerEnterEvent;
		public event Action<BaseItemSlot> OnPointerExitEvent;

		private void OnValidate()
		{
			itemSlots = GetComponentsInChildren<BaseItemSlot>(includeInactive: true);
		}

		private void Start()
		{
			foreach (BaseItemSlot itemSlot in itemSlots)
			{
				itemSlot.OnPointerEnterEvent += slot => OnPointerEnterEvent(slot);
				itemSlot.OnPointerExitEvent += slot => OnPointerExitEvent(slot);
			}
		}

		public void OnCraftButtonClick()
		{
			if (_craftingRecipe != null && ItemContainer != null)
			{
				_craftingRecipe.Craft(ItemContainer);
			}
		}

		private void SetCraftingRecipe(CraftingRecipe newCraftingRecipe)
		{
			_craftingRecipe = newCraftingRecipe;

			if (_craftingRecipe != null)
			{
				var slotIndex = 0;
				slotIndex = SetSlots(_craftingRecipe.Materials, slotIndex);
				arrowParent.SetSiblingIndex(slotIndex);
				slotIndex = SetSlots(_craftingRecipe.Results, slotIndex);

				for (var i = slotIndex; i < itemSlots.Length; i++)
				{
					itemSlots[i].transform.parent.gameObject.SetActive(false);
				}

				gameObject.SetActive(true);
			}
			else
			{
				gameObject.SetActive(false);
			}
		}

		private int SetSlots(IList<ItemAmount> itemAmountList, int slotIndex)
		{
			for (var i = 0; i < itemAmountList.Count; i++, slotIndex++)
			{
				ItemAmount itemAmount = itemAmountList[i];
				BaseItemSlot itemSlot = itemSlots[slotIndex];

				itemSlot.Item = itemAmount.Item;
				itemSlot.Amount = itemAmount.Amount;
				itemSlot.transform.parent.gameObject.SetActive(true);
			}
			return slotIndex;
		}
	}
}
