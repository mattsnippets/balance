﻿using System;
using _Balance._02_Scripts.Inventory.Hotbar;
using _Balance._02_Scripts.Inventory.Items;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace _Balance._02_Scripts.Inventory.CharacterPanel
{
	public class BaseItemSlot : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
	{
		[SerializeField] protected Image image;
		[SerializeField] protected Text amountText;
		[SerializeField] protected HotbarSlot _hotbarSlot;

		public event Action<BaseItemSlot> OnPointerEnterEvent;
		public event Action<BaseItemSlot> OnPointerExitEvent;
		public event Action<BaseItemSlot> OnRightClickEvent;
		public event Action<BaseItemSlot> OnLeftClickEvent;

		protected bool isPointerOver;
		protected Color normalColor = Color.white;
		protected Color disabledColor = new Color(1, 1, 1, 0);
		protected Item _item;

		public virtual void RemoveEventListeners()
		{
			OnPointerEnterEvent = null;
			OnPointerExitEvent = null;
			OnRightClickEvent = null;
			OnLeftClickEvent = null;
		}

		public Item Item
		{
			get => _item;

			set
			{
				_item = value;
				if (_item == null && Amount != 0)
				{
					Amount = 0;
				}

				if (_item == null)
				{
					image.sprite = null;
					image.color = disabledColor;
				}
				else
				{
					image.sprite = _item.Icon;
					image.color = normalColor;
				}

				if (isPointerOver)
				{
					OnPointerExit(null);
					OnPointerEnter(null);
				}

				if (_hotbarSlot)
				{
					_hotbarSlot.Item = value;
				}
			}
		}

		private int _amount;
		public int Amount
		{
			get => _amount;

			set
			{
				_amount = value;
				if (_amount < 0)
				{
					_amount = 0;
				}

				if (_amount == 0 && Item != null)
				{
					Item = null;
				}

				if (amountText != null)
				{
					amountText.enabled = _item != null && _amount > 1;
					if (amountText.enabled)
					{
						amountText.text = _amount.ToString();
					}
				}

				if (_hotbarSlot)
				{
					_hotbarSlot.Amount = value;
				}
			}
		}

		public virtual bool CanAddStack(Item item, int amount = 1) => Item != null && Item.ID == item.ID;

		public virtual bool CanReceiveItem(Item item) => false;

		protected virtual void OnValidate()
		{
			if (image == null)
			{
				image = GetComponent<Image>();
			}

			if (amountText == null)
			{
				amountText = GetComponentInChildren<Text>();
			}

			Item = _item;
			Amount = _amount;
		}

		protected virtual void OnDisable()
		{
			if (isPointerOver)
			{
				OnPointerExit(null);
			}
		}

		public virtual void OnPointerClick(PointerEventData eventData)
		{
			if (eventData != null)
			{
				if (eventData.button == PointerEventData.InputButton.Right)
				{
					OnRightClickEvent?.Invoke(this);
				}
				else if (eventData.button == PointerEventData.InputButton.Left)
				{
					OnLeftClickEvent?.Invoke(this);
				}
			}
		}

		public void OnPointerEnter(PointerEventData eventData)
		{
			isPointerOver = true;
			OnPointerEnterEvent?.Invoke(this);
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			isPointerOver = false;
			OnPointerExitEvent?.Invoke(this);
		}

		public virtual void SetMovedItemState(bool isMoved)
		{
		}
	}
}
