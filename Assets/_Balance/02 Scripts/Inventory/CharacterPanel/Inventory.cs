﻿using _Balance._02_Scripts.Inventory.Items;
using UnityEngine;

namespace _Balance._02_Scripts.Inventory.CharacterPanel
{
	public class Inventory : ItemContainer
	{
		[SerializeField] protected Item[] startingItems;
		[SerializeField] protected Transform itemsParent;

		protected override void OnValidate()
		{
			if (itemsParent != null)
			{
				itemsParent.GetComponentsInChildren(true, ItemSlots);
			}

			if (!Application.isPlaying)
			{
				SetStartingItems();
			}
		}

		protected override void Awake()
		{
			base.Awake();
			SetStartingItems();
		}

		private void SetStartingItems()
		{
			Clear();
			foreach (var item in startingItems)
			{
				AddItem(item.GetCopy());
			}
		}
	}
}
