﻿using _Balance._02_Scripts.Inventory.Items;
using UnityEngine;
using UnityEngine.UI;

namespace _Balance._02_Scripts.Inventory.CharacterPanel
{
	public class ItemTooltip : MonoBehaviour
	{
		[SerializeField] private Text _itemNameText;
		[SerializeField] private Text _itemTypeText;
		[SerializeField] private Text _itemDescriptionText;

		private void Awake() => gameObject.SetActive(false);

		public void ShowTooltip(Item item)
		{
			_itemNameText.text = item.ItemName;
			_itemTypeText.text = item.GetItemType();
			_itemDescriptionText.text = item.GetDescription();
			gameObject.SetActive(true);
		}

		public void HideTooltip()
		{
			gameObject.SetActive(false);
		}
	}
}
