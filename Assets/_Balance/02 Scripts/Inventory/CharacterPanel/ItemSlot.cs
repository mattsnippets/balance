﻿using System;
using _Balance._02_Scripts.Inventory.Items;
using UnityEngine;
using UnityEngine.EventSystems;

namespace _Balance._02_Scripts.Inventory.CharacterPanel
{
	public class ItemSlot : BaseItemSlot, IBeginDragHandler, IEndDragHandler, IDragHandler, IDropHandler
	{
		public event Action<BaseItemSlot> OnBeginDragEvent;
		public event Action<BaseItemSlot> OnEndDragEvent;
		public event Action<BaseItemSlot> OnDragEvent;
		public event Action<BaseItemSlot> OnDropEvent;

		private bool _isDragging;
		private readonly Color _dragColor = new Color(1, 1, 1, 0.5f);

		public override void RemoveEventListeners()
		{
			base.RemoveEventListeners();
			OnBeginDragEvent = null;
			OnEndDragEvent = null;
			OnDragEvent = null;
			OnDropEvent = null;
		}

		public override bool CanAddStack(Item item, int amount = 1) =>
			base.CanAddStack(item, amount) && Amount + amount <= item.MaximumStacks;

		public override bool CanReceiveItem(Item item) => true;

		protected override void OnDisable()
		{
			base.OnDisable();

			if (_isDragging)
			{
				OnEndDrag(null);
			}
		}

		public void OnBeginDrag(PointerEventData eventData)
		{
			//SetMovedItemState(true);

			OnBeginDragEvent?.Invoke(this);
		}

		public void OnEndDrag(PointerEventData eventData)
		{
			//SetMovedItemState(false);

			OnEndDragEvent?.Invoke(this);
		}

		public void OnDrag(PointerEventData eventData) => OnDragEvent?.Invoke(this);

		public void OnDrop(PointerEventData eventData) => OnDropEvent?.Invoke(this);

		public override void SetMovedItemState(bool isMoved)
		{
			base.SetMovedItemState(isMoved);

			if (isMoved)
			{
				_isDragging = true;

				if (Item != null)
				{
					image.color = _dragColor;
				}
			}
			else
			{
				_isDragging = false;

				if (Item != null)
				{
					image.color = normalColor;
				}
			}
		}
	}
}
