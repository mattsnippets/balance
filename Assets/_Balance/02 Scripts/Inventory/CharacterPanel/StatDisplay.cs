﻿using _Balance._02_Scripts.Inventory.CharacterStats;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace _Balance._02_Scripts.Inventory.CharacterPanel
{
	public class StatDisplay : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
	{
		private CharacterStat _stat;
		public CharacterStat Stat
		{
			get => _stat;
			set
			{
				_stat = value;
				UpdateStatValue();
			}
		}

		private string _name;
		public string Name
		{
			get => _name;
			set
			{
				_name = value;
				nameText.text = _name.ToLower();
			}
		}

		[SerializeField] Text nameText;
		[SerializeField] Text valueText;
		[SerializeField] StatTooltip tooltip;

		private bool _showingTooltip;

		private void OnValidate()
		{
			Text[] texts = GetComponentsInChildren<Text>();
			nameText = texts[0];
			valueText = texts[1];

			if (tooltip == null)
			{
				tooltip = FindObjectOfType<StatTooltip>();
			}
		}

		public void OnPointerEnter(PointerEventData eventData)
		{
			tooltip.ShowTooltip(Stat, Name);
			_showingTooltip = true;
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			tooltip.HideTooltip();
			_showingTooltip = false;
		}

		public void UpdateStatValue()
		{
			valueText.text = _stat.Value.ToString();
			if (_showingTooltip)
			{
				tooltip.ShowTooltip(Stat, Name);
			}
		}
	}
}
