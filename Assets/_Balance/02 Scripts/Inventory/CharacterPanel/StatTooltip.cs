﻿using System.Text;
using _Balance._02_Scripts.Inventory.CharacterStats;
using _Balance._02_Scripts.Inventory.Items;
using UnityEngine;
using UnityEngine.UI;

namespace _Balance._02_Scripts.Inventory.CharacterPanel
{
	public class StatTooltip : MonoBehaviour
	{
		[SerializeField] Text StatNameText;
		[SerializeField] Text StatModifiersLabelText;
		[SerializeField] Text StatModifiersText;

		private readonly StringBuilder _sb = new StringBuilder();

		private void Awake()
		{
			gameObject.SetActive(false);
		}

		public void ShowTooltip(CharacterStat stat, string statName)
		{
			StatNameText.text = GetStatTopText(stat, statName);
			StatModifiersText.text = GetStatModifiersText(stat);
			gameObject.SetActive(true);
		}

		public void HideTooltip()
		{
			gameObject.SetActive(false);
		}

		private string GetStatTopText(CharacterStat stat, string statName)
		{
			_sb.Length = 0;
			_sb.Append(statName);
			_sb.Append(" ");
			_sb.Append(stat.Value);

			if (stat.Value != stat.BaseValue)
			{
				_sb.Append(" (");
				_sb.Append(stat.BaseValue);

				if (stat.Value > stat.BaseValue)
				{
					_sb.Append("+");
				}

				_sb.Append(System.Math.Round(stat.Value - stat.BaseValue, 4));
				_sb.Append(")");
			}

			return _sb.ToString();
		}

		private string GetStatModifiersText(CharacterStat stat)
		{
			_sb.Length = 0;

			foreach (StatModifier mod in stat.StatModifiers)
			{
				if (_sb.Length > 0)
				{
					_sb.AppendLine();
				}

				if (mod.Value > 0)
				{
					_sb.Append("+");
				}

				if (mod.Type == StatModType.Flat)
				{
					_sb.Append(mod.Value);
				}
				else
				{
					_sb.Append(mod.Value * 100);
					_sb.Append("%");
				}

				Item item = mod.Source as Item;

				if (item != null)
				{
					_sb.Append(" ");
					_sb.Append(item.ItemName);
				}
				else
				{
					Debug.LogError("Modifier is not an Item!");
				}
			}

			return _sb.ToString();
		}
	}
}
