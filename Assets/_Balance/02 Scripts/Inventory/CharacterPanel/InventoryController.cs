﻿using System;
using _Balance._02_Scripts.BusinessLogic;
using _Balance._02_Scripts.Inventory.FileIO;
using _Balance._02_Scripts.Inventory.Items;
using _Balance._02_Scripts.Inventory.Ui;
using _Balance._02_Scripts.Player;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace _Balance._02_Scripts.Inventory.CharacterPanel
{
    public class InventoryController : MonoBehaviour
    {
        public Inventory Inventory;

        [SerializeField] private bool _loadInventoryOnStart;
        [SerializeField] private Hotbar.Hotbar _hotbar;
        [SerializeField] private ItemTooltip _itemTooltip;
        [SerializeField] private Image _draggableItem;
        [SerializeField] private DropItemArea _dropItemArea;
        [SerializeField] private QuestionDialog _reallyDropItemDialog;
        [SerializeField] private InventoryInput _inventoryInput;
        [SerializeField] private float _pickupMagnetRadius = 3f;
        [SerializeField] private Transform _itemContainerPanel;

        private static ItemSaveManager _itemSaveManager;

        private BaseItemSlot _dragItemSlot;
        private readonly Collider[] _surroundingPickups = new Collider[50];
        private PlayerController _playerController;

        private bool IsInventoryOpen { get; set; }
        public Transform ItemContainerPanel => _itemContainerPanel;

        private void Awake()
        {
            // Left Click
            Inventory.OnLeftClickEvent += InventoryLeftClick;
            _dropItemArea.OnLeftClickEvent += DropItemOutsideUI;
            // Right Click
            Inventory.OnRightClickEvent += InventoryRightClick;
            // Pointer Enter
            Inventory.OnPointerEnterEvent += ShowTooltip;
            // Pointer Exit
            Inventory.OnPointerExitEvent += HideTooltip;
            // Begin Drag
            //Inventory.OnBeginDragEvent += BeginDrag;
            // End Drag
            //Inventory.OnEndDragEvent += EndDrag;
            // Drag
            //Inventory.OnDragEvent += Drag;
            // Drop
            //Inventory.OnDropEvent += Drop;
            //dropItemArea.OnDropEvent += DropItemOutsideUI;
        }


        private void Start()
        {
            _itemSaveManager = ServiceLocator.Resolve<ItemSaveManager>();
            _playerController = ServiceLocator.Resolve<PlayerController>();

            if (_loadInventoryOnStart)
            {
                _itemSaveManager.LoadInventoryItems(Inventory);
            }

            _hotbar.OnSelectedItemActivated += OnHotbarSelectedItemActivated;
            _inventoryInput.OnInventoryPanelOpenChanged += OnInventoryPanelOpenChanged;

            EnableItemMagnet();
        }

        private void EnableItemMagnet()
        {
            Observable.Interval(TimeSpan.FromSeconds(0.3)).Subscribe(_ =>
            {
                var pickups = Physics.OverlapSphereNonAlloc(
                    transform.position,
                    _pickupMagnetRadius,
                    _surroundingPickups,
                    LayerMask.GetMask("Pickups"),
                    QueryTriggerInteraction.Collide);

                for (var i = 0; i < pickups; i++)
                {
                    var pickup = _surroundingPickups[i].GetComponent<ItemPickup>();

                    if (pickup is AmmoPickup)
                    {
                        if (!_playerController.IsAmmoFull)
                        {
                            pickup.ApplyAttraction(transform, 20f);
                        }
                    }
                    else
                    {
                        if (Inventory.CanAddItem(pickup.Item) && pickup.CanBePickedUp)
                        {
                            pickup.ApplyAttraction(transform, 20f);
                        }
                    }
                }
            }).AddTo(this);
        }

        private void OnInventoryPanelOpenChanged(bool isOpen) => IsInventoryOpen = isOpen;

        private void OnHotbarSelectedItemActivated(int slotIndex)
        {
            if (!IsInventoryOpen)
            {
                ActivateItem(Inventory.ItemSlots[slotIndex]);
            }
        }

        private void Update()
        {
            _draggableItem.transform.position = Input.mousePosition;
        }

        private void OnDestroy()
        {
            _hotbar.OnSelectedItemActivated -= OnHotbarSelectedItemActivated;
            _inventoryInput.OnInventoryPanelOpenChanged += OnInventoryPanelOpenChanged;
        }

        private void ActivateItem(BaseItemSlot itemSlot)
        {
            if (itemSlot.Item is EquippableItem)
            {
                //Equip((EquippableItem)itemSlot.Item);
            }
            else if (itemSlot.Item is UsableItem item)
            {
                if (item.Use(this) && item.IsConsumable)
                {
                    itemSlot.Amount--;
                    item.Destroy();
                }
            }
        }

        private void InventoryRightClick(BaseItemSlot itemSlot)
        {
            ActivateItem(itemSlot);
        }

        private void InventoryLeftClick(BaseItemSlot itemSlot)
        {
            if (_dragItemSlot == null)
            {
                if (itemSlot.Item != null)
                {
                    _dragItemSlot = itemSlot;
                    _draggableItem.sprite = itemSlot.Item.Icon;
                    _draggableItem.transform.position = Input.mousePosition;
                    _draggableItem.gameObject.SetActive(true);
                    itemSlot.SetMovedItemState(true);
                }
            }
            else
            {
                if (itemSlot.CanAddStack(_dragItemSlot.Item))
                {
                    AddStacks(itemSlot);
                    FinishItemMove();
                }
                else if (itemSlot.CanReceiveItem(_dragItemSlot.Item) && _dragItemSlot.CanReceiveItem(itemSlot.Item))
                {
                    SwapItems(itemSlot);
                    FinishItemMove();
                }
            }
        }

        private void FinishItemMove()
        {
            _dragItemSlot.SetMovedItemState(false);
            _dragItemSlot = null;
            _draggableItem.gameObject.SetActive(false);
        }

        private void ShowTooltip(BaseItemSlot itemSlot)
        {
            if (itemSlot.Item != null)
            {
                _itemTooltip.ShowTooltip(itemSlot.Item);
            }
        }

        private void HideTooltip(BaseItemSlot itemSlot)
        {
            if (_itemTooltip.gameObject.activeSelf)
            {
                _itemTooltip.HideTooltip();
            }
        }

        private void AddStacks(BaseItemSlot dropItemSlot)
        {
            var numAddableStacks = dropItemSlot.Item.MaximumStacks - dropItemSlot.Amount;
            var stacksToAdd = Mathf.Min(numAddableStacks, _dragItemSlot.Amount);

            dropItemSlot.Amount += stacksToAdd;
            _dragItemSlot.Amount -= stacksToAdd;
        }

        private void SwapItems(BaseItemSlot dropItemSlot)
        {
            EquippableItem dragEquipItem = _dragItemSlot.Item as EquippableItem;
            EquippableItem dropEquipItem = dropItemSlot.Item as EquippableItem;

            Item draggedItem = _dragItemSlot.Item;
            var draggedItemAmount = _dragItemSlot.Amount;

            _dragItemSlot.Item = dropItemSlot.Item;
            _dragItemSlot.Amount = dropItemSlot.Amount;

            dropItemSlot.Item = draggedItem;
            dropItemSlot.Amount = draggedItemAmount;
        }

        private void DropItemOutsideUI()
        {
            if (_dragItemSlot == null)
            {
                return;
            }

            _reallyDropItemDialog.Show();
            BaseItemSlot slot = _dragItemSlot;

            _reallyDropItemDialog.OnYesEvent += () =>
            {
                DropItem(slot);
                DestroyItemInSlot(slot);
                FinishItemMove();
            };

            _reallyDropItemDialog.OnNoEvent += () => { FinishItemMove(); };
        }

        private void DropItem(BaseItemSlot itemSlot)
        {
            for (var i = 0; i < itemSlot.Amount; i++)
            {
                var randomTranslation = UnityEngine.Random.insideUnitCircle;
                Instantiate(itemSlot.Item.PickupPrefab, transform.position + new Vector3(randomTranslation.x, 1.5f, randomTranslation.y) * 2f, Quaternion.identity);
            }
        }

        private void DestroyItemInSlot(BaseItemSlot itemSlot)
        {
            itemSlot.Item.Destroy();
            itemSlot.Item = null;
        }

        private ItemContainer openItemContainer;

        private void TransferToItemContainer(BaseItemSlot itemSlot)
        {
            Item item = itemSlot.Item;
            if (item != null && openItemContainer.CanAddItem(item))
            {
                Inventory.RemoveItem(item);
                openItemContainer.AddItem(item);
            }
        }

        private void TransferToInventory(BaseItemSlot itemSlot)
        {
            Item item = itemSlot.Item;
            if (item != null && Inventory.CanAddItem(item))
            {
                openItemContainer.RemoveItem(item);
                Inventory.AddItem(item);
            }
        }

        public void OpenItemContainer(ItemContainer itemContainer)
        {
            _inventoryInput.SetInventoryPanelOpen(true);

            openItemContainer = itemContainer;

            Inventory.OnRightClickEvent -= InventoryRightClick;
            Inventory.OnRightClickEvent += TransferToItemContainer;

            itemContainer.OnRightClickEvent += TransferToInventory;
            itemContainer.OnPointerEnterEvent += ShowTooltip;
            itemContainer.OnPointerExitEvent += HideTooltip;
            itemContainer.UpdateItemSlotEvents();

            //itemContainer.OnBeginDragEvent += BeginDrag;
            //itemContainer.OnEndDragEvent += EndDrag;
            //itemContainer.OnDragEvent += Drag;
            //itemContainer.OnDropEvent += Drop;
        }

        public void CloseItemContainer(ItemContainer itemContainer)
        {
            _inventoryInput.SetInventoryPanelOpen(false);

            openItemContainer = null;

            Inventory.OnRightClickEvent += InventoryRightClick;
            Inventory.OnRightClickEvent -= TransferToItemContainer;

            itemContainer.OnRightClickEvent -= TransferToInventory;
            itemContainer.OnPointerEnterEvent -= ShowTooltip;
            itemContainer.OnPointerExitEvent -= HideTooltip;
            itemContainer.UnsubscribeItemSlotEvents();

            //itemContainer.OnBeginDragEvent -= BeginDrag;
            //itemContainer.OnEndDragEvent -= EndDrag;
            //itemContainer.OnDragEvent -= Drag;
            //itemContainer.OnDropEvent -= Drop;
        }
    }
}