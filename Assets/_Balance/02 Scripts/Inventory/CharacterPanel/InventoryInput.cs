﻿using System;
using UnityEngine;

namespace _Balance._02_Scripts.Inventory.CharacterPanel
{
    public class InventoryInput : MonoBehaviour
    {
        [SerializeField] private GameObject _characterPanelGameObject;
        [SerializeField] private GameObject _dropItemArea;
        [SerializeField] private bool _showAndHideMouse = true;
        [SerializeField] private Canvas _inventoryCanvas;

        public event Action<bool> OnInventoryPanelOpenChanged;

        public bool IsPanelOpen { get; private set; }

        public void SetInventoryPanelOpen(bool isEnabled)
        {
            if (isEnabled)
            {
                ShowMouseCursor();
            }
            else
            {
                HideMouseCursor();
            }

            _characterPanelGameObject.SetActive(isEnabled);
            _dropItemArea.SetActive(isEnabled);
            OnInventoryPanelOpenChanged?.Invoke(isEnabled);
            IsPanelOpen = isEnabled;
        }

        private void ShowMouseCursor()
        {
            if (_showAndHideMouse)
            {
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
        }

        private void HideMouseCursor()
        {
            if (_showAndHideMouse)
            {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
    }
}
