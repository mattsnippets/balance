﻿using _Balance._02_Scripts.Inventory.CharacterStats;
using UnityEngine;

namespace _Balance._02_Scripts.Inventory.CharacterPanel
{
	public class StatPanel : MonoBehaviour
	{
		[SerializeField] StatDisplay[] statDisplays;
		[SerializeField] string[] statNames;

		private CharacterStat[] _stats;

		private void OnValidate()
		{
			statDisplays = GetComponentsInChildren<StatDisplay>();
			UpdateStatNames();
		}

		public void SetStats(params CharacterStat[] charStats)
		{
			_stats = charStats;

			if (_stats.Length > statDisplays.Length)
			{
				Debug.LogError("Not Enough Stat Displays!");
				return;
			}

			for (var i = 0; i < statDisplays.Length; i++)
			{
				statDisplays[i].gameObject.SetActive(i < _stats.Length);

				if (i < _stats.Length)
				{
					statDisplays[i].Stat = _stats[i];
				}
			}
		}

		public void UpdateStatValues()
		{
			for (var i = 0; i < _stats.Length; i++)
			{
				statDisplays[i].UpdateStatValue();
			}
		}

		public void UpdateStatNames()
		{
			for (var i = 0; i < statNames.Length; i++)
			{
				statDisplays[i].Name = statNames[i];
			}
		}
	}
}
