﻿using UnityEngine;

namespace _Balance._02_Scripts.Inventory.CharacterPanel
{
	public class InfiniteInventory : Inventory
	{
		[SerializeField] private GameObject itemSlotPrefab;
		[SerializeField] private int maxSlots;
		
		public int MaxSlots
		{
			get => maxSlots;
			set => SetMaxSlots(value);
		}

		protected override void Awake()
		{
			SetMaxSlots(maxSlots);
			base.Awake();
		}

		private void SetMaxSlots(int value)
		{
			if (value <= 0)
			{
				maxSlots = 1;
			}
			else
			{
				maxSlots = value;
			}

			if (maxSlots < ItemSlots.Count)
			{
				for (var i = maxSlots; i < ItemSlots.Count; i++)
				{
					Destroy(ItemSlots[i].transform.parent.gameObject);
				}
				int diff = ItemSlots.Count - maxSlots;
				ItemSlots.RemoveRange(maxSlots, diff);
			}
			else if (maxSlots > ItemSlots.Count)
			{
				int diff = maxSlots - ItemSlots.Count;

				for (var i = 0; i < diff; i++)
				{
					GameObject slot = Instantiate(itemSlotPrefab, itemsParent, false);
					ItemSlots.Add(slot.GetComponentInChildren<ItemSlot>());
				}
			}
		}
	}
}