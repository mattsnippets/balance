﻿using _Balance._02_Scripts.Inventory.Items;
using UnityEngine;

namespace _Balance._02_Scripts.Inventory.Placeables
{
	[CreateAssetMenu(menuName = "Inventory/Item Implementations/Placeable Implementation")]
	public class PlaceableImplementation : ScriptableObject
	{
		public EPlaceable PlaceableType;
		public GameObject PlaceablePrefab;
	}
}