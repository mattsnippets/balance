﻿using _Balance._02_Scripts.Utilities.SerializableDictionaries;
using UnityEngine;

namespace _Balance._02_Scripts.Inventory.Placeables
{
    [CreateAssetMenu(menuName = "Inventory/Items/Placeable Implementation Dictionary")]
    public class PlaceableImplementationDictionary : ScriptableObject
    {
        public EPlaceablePlaceableImplementationDictionary Items;
    }
}