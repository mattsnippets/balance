﻿using _Balance._02_Scripts.Inventory.CharacterPanel;
using UnityEngine;

namespace _Balance._02_Scripts.Inventory.Items.UsableItemEffects
{
    [CreateAssetMenu(menuName = "Inventory/Item Effects/Placeable")]
    public class PlaceableEffect : UsableItemEffect
    {
        public EPlaceable Placeable;
        public ESoil Soil;

        public override bool ExecuteEffect(UsableItem parentItem, InventoryController character) => throw new System.NotImplementedException();
        public override string GetDescription() => throw new System.NotImplementedException();
    }
}