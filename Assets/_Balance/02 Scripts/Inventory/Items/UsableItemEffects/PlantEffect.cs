﻿using _Balance._02_Scripts.BusinessLogic;
using _Balance._02_Scripts.Farming;
using _Balance._02_Scripts.Inventory.CharacterPanel;
using _Balance._02_Scripts.ProceduralWorld.Chunks;
using UnityEngine;

namespace _Balance._02_Scripts.Inventory.Items.UsableItemEffects
{
	[CreateAssetMenu(menuName = "Inventory/Item Effects/Plant")]
	public class PlantEffect : PlaceableEffect
	{
		public EPlantCategory PlantCategory;
		public EPlantRequirement Requirements;
		public float GrowthDays;
		public float BalanceChange;

		public override bool ExecuteEffect(UsableItem parentItem, InventoryController character)
		{
			var chunkManager = ServiceLocator.Resolve<ChunkManager>();

			MapChunk currentChunkMap = chunkManager.CurrentMapChunk;

			if (currentChunkMap)
			{
				var tile = currentChunkMap.GetTile(
					currentChunkMap.WorldXToMap(chunkManager.PlayerPos.x),
					currentChunkMap.WorldZToMap(chunkManager.PlayerPos.z));

				if ((tile.State & (TileState.Tree | TileState.Planted | TileState.Corrupted)) == 0)
				{
					tile.Affect(TileState.Planted);
					//currentChunkMap.Redraw();
					
					var plantGo = Instantiate(
						ServiceLocator.Resolve<ImplementationProvider>().GetPlaceableImplementation(Placeable).PlaceablePrefab,
						new Vector3(currentChunkMap.MapXToWorld(tile.X), 0f, currentChunkMap.MapZToWorld(tile.Y)),
						Quaternion.AngleAxis(UnityEngine.Random.Range(0f, 359f), Vector3.up));

					plantGo.transform.parent = currentChunkMap.transform;

					var plant = plantGo.GetComponent<Plant>();
					plant.FullGrowthTime = GrowthDays * ServiceLocator.Resolve<GameState>().DayLength;
					plant.BalanceChange = BalanceChange ;
					plant.Type = Placeable;

					return true;
				}
			}

			return false;
		}

		public override string GetDescription() => "";
	}
}