﻿using UnityEngine;

namespace _Balance._02_Scripts.Inventory.Items.UsableItemEffects
{
    [CreateAssetMenu(menuName = "Inventory/Item Effects/Special")]
    public class SpecialUsableEffect : PlantEffect
    {
        public int Special;
    }
}
