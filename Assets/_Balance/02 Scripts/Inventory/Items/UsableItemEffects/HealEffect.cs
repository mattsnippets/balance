﻿using _Balance._02_Scripts.Entities;
using _Balance._02_Scripts.Inventory.CharacterPanel;
using UnityEngine;

namespace _Balance._02_Scripts.Inventory.Items.UsableItemEffects
{
	[CreateAssetMenu(menuName = "Inventory/Item Effects/Heal")]
	public class HealEffect : UsableItemEffect
	{
		public int HealAmount;

		public override bool ExecuteEffect(UsableItem parentItem, InventoryController character)
		{
			character.GetComponent<Health>().Add(HealAmount);
			return true;
		}

		public override string GetDescription()
		{
			return $"Heals {HealAmount}";
		}
	}
}