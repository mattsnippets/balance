﻿using System.Collections;
using _Balance._02_Scripts.Inventory.CharacterPanel;
using _Balance._02_Scripts.Inventory.CharacterStats;
using UnityEngine;

namespace _Balance._02_Scripts.Inventory.Items.UsableItemEffects
{
	[CreateAssetMenu(menuName = "Inventory/Item Effects/Buff")]
	public class BuffEffect : UsableItemEffect
	{
		public int AgilityBuff;
		public float Duration;

		public override bool ExecuteEffect(UsableItem parentItem, InventoryController character)
		{
			StatModifier statModifier = new StatModifier(AgilityBuff, StatModType.Flat, parentItem);
			//character.Agility.AddModifier(statModifier);
			//character.UpdateStatValues();
			character.StartCoroutine(RemoveBuff(character, statModifier, Duration));
			return true;
		}

		public override string GetDescription()
		{
			return "Grants " + AgilityBuff + " Agility for " + Duration + " seconds.";
		}

		private static IEnumerator RemoveBuff(InventoryController character, StatModifier statModifier, float duration)
		{
			yield return new WaitForSeconds(duration);
			//character.Agility.RemoveModifier(statModifier);
			//character.UpdateStatValues();
		}
	}
}