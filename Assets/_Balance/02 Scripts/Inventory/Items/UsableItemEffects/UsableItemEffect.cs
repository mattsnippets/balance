﻿using _Balance._02_Scripts.Inventory.CharacterPanel;
using UnityEngine;

namespace _Balance._02_Scripts.Inventory.Items.UsableItemEffects
{
	public abstract class UsableItemEffect : ScriptableObject
	{
		public string Name;
#if UNITY_EDITOR
		private void OnValidate()
		{
			Name = name;
		}
#endif

		public abstract bool ExecuteEffect(UsableItem parentItem, InventoryController character);
		public abstract string GetDescription();


	}
}
