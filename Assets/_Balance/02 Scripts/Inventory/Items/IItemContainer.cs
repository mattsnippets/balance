﻿using System.Collections.Generic;
using _Balance._02_Scripts.Inventory.CharacterPanel;

namespace _Balance._02_Scripts.Inventory.Items
{
	public interface IItemContainer
	{
		List<ItemSlot> GetItemSlots();

		bool CanAddItem(Item item, int amount = 1);
		bool AddItem(Item item);

		Item RemoveItem(string itemID);
		bool RemoveItem(Item item);

		void Clear();

		int ItemCount(string itemID);
	}
}
