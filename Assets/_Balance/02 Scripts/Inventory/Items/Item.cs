﻿using System.Text;
using _Balance._02_Scripts.BusinessLogic;
using UnityEditor;
using UnityEngine;
#if UNITY_EDITOR

#endif

namespace _Balance._02_Scripts.Inventory.Items
{

	[CreateAssetMenu(menuName = "Inventory/Items/Item")]
	public class Item : ScriptableObject
	{

		[SerializeField] string id;
		public string ID { get { return id; } }
		public EItem Type;
		public string ItemName;
		public Sprite Icon;

		[TextArea(3, 3)]
		public string Description;

		[Range(1, 999)]
		public int MaximumStacks = 1;

		protected static readonly StringBuilder sb = new StringBuilder();
		public GameObject PickupPrefab => ServiceLocator.Resolve<ImplementationProvider>().GetItemImplementation(Type).PickupPrefab;

#if UNITY_EDITOR
		protected virtual void OnValidate()
		{
			var path = AssetDatabase.GetAssetPath(this);
			id = AssetDatabase.AssetPathToGUID(path);
			ItemName = name;
			//UnityEngine.Assertions.Assert.IsNotNull(PickupPrefab, $"Pickup prefab should be assigned for item {name}");
		}
#endif

		public virtual Item GetCopy() => this;

		public virtual void Destroy() { }

		public virtual string GetItemType() => "";

		public virtual string GetDescription() => Description;
	}
}
