﻿using _Balance._02_Scripts.Player;
using Lean.Pool;
using UnityEngine;

namespace _Balance._02_Scripts.Inventory.Items
{
    public class AmmoPickup : ItemPickup
    {
        protected override void OnPickedUp(Collider other)
        {
            var controller = other.gameObject.GetComponent<PlayerController>();
            controller.GetAmmo(2);
            CanBePickedUp = false;
            LeanPool.Despawn(gameObject);
        }
    }
}