﻿using _Balance._02_Scripts.BusinessLogic;
using _Balance._02_Scripts.Inventory.CharacterPanel;
using UnityEngine;

namespace _Balance._02_Scripts.Inventory.Items
{
	[CreateAssetMenu(menuName = "Inventory/Items/Usable Item")]
	public class UsableItem : Item
	{
		public bool IsConsumable;

		public virtual bool Use(InventoryController controller)
		{
			if (ServiceLocator.Resolve<ImplementationProvider>().GetItemImplementation(Type) is UsableItemImplementation impl)
			{
				return impl.Use(this, controller);
			}
			else
			{
				Debug.LogError("Tried to call Use() on a non-usable item implementation");
				return false;
			}
		}

		public override string GetItemType()
		{
			return IsConsumable ? "Consumable" : "Usable";
		}

		public override string GetDescription()
		{
			//sb.Length = 0;
			//foreach (UsableItemEffect effect in Effects)
			//{
			//	sb.AppendLine(effect.GetDescription());
			//}
			//return sb.ToString();

			return base.GetDescription();
		}
	}
}
