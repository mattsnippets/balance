﻿using System;
using _Balance._02_Scripts.Inventory.CharacterPanel;
using Lean.Pool;
using UniRx;
using UnityEngine;

namespace _Balance._02_Scripts.Inventory.Items
{
    public class ItemPickup : MonoBehaviour
    {
        [SerializeField] private Item _item;

        public bool CanBePickedUp { get; protected set; }
        public Item Item => _item;

        private bool _isAttracted;
        private float _attractionSpeed;
        private Transform _attractor;
        private IDisposable _cooldownTimer;

        private void Update()
        {
            if (_isAttracted)
            {
                if ((_attractor.position - transform.position).sqrMagnitude > 100f)
                {
                    _attractor = null;
                    _isAttracted = false;
                }
                else
                {
                    transform.Translate((_attractor.position - transform.position).normalized * _attractionSpeed * Time.deltaTime);
                }
            }
            else
            {
                transform.Rotate(Vector3.up, 70f * Time.deltaTime);
                transform.localPosition += new Vector3(0f, Mathf.Sin(Time.time * 2.5f) / 40f * Time.timeScale, 0f);
            }
        }

        private void OnEnable()
        {
            SetNonPickableCooldown();
        }

        private void SetNonPickableCooldown()
        {
            CanBePickedUp = false;
            _cooldownTimer?.Dispose();
            _cooldownTimer = Observable.Timer(TimeSpan.FromSeconds(3.0)).Subscribe(_ => CanBePickedUp = true).AddTo(this);
        }

        public void ApplyAttraction(Transform attractor, float speed)
        {
            _attractor = attractor;
            _attractionSpeed = speed;
            _isAttracted = true;
            transform.rotation = Quaternion.Euler(Vector3.zero);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                _isAttracted = false;

                if (CanBePickedUp)
                {
                    OnPickedUp(other);
                }
            }
        }

        protected virtual void OnPickedUp(Collider other)
        {
            var controller = other.gameObject.GetComponent<InventoryController>();

            if (controller.Inventory.AddItem(_item.GetCopy()))
            {
                CanBePickedUp = false;
                LeanPool.Despawn(gameObject);
            }
        }
    }
}