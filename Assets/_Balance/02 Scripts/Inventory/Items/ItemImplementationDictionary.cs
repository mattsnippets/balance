﻿using _Balance._02_Scripts.Utilities.SerializableDictionaries;
using UnityEngine;

namespace _Balance._02_Scripts.Inventory.Items
{
    [CreateAssetMenu(menuName = "Inventory/Items/Item Implementation Dictionary")]
    public class ItemImplementationDictionary : ScriptableObject
    {
        public EItemItemImplementationDictionary Items;
    }
}