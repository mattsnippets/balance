﻿using System;

namespace _Balance._02_Scripts.Inventory.Items
{
    public enum EItem
    {
        None = 0,
        BlackSeed = 1,
        WhiteSeed = 2,
        GreenPotion = 3,
        Ammo = 4,
        HealthCrystal = 5
    }

    public enum EPlaceable
    {
        None = 0,
        BalanceBerry = 1,
        CleansingPlant = 2,
        Chest = 3
    }

    public enum EProp
    {
        None = 0,
        Rock1  = 1,
        Rock3  = 2,
        Rock4 = 3,
        Mushroom = 4,
        TreeStump = 5,
        Rock6 = 6,
        LeafyBush = 7,
        PointyBushDetailed = 8,
        PointyBushSmall = 9,
        FireflyParticle = 10
    }

    [Flags]
    public enum EPlantCategory
    {
        None = 0,
        Berry = 1 << 0
    }

    [Flags]
    public enum EPlantRequirement
    {
        None = 0,
        Water = 1 << 0,
        Sun = 1 << 1
    }

    [Flags]
    public enum ESoil
    {
        None = 0,
        Grass = 1 << 0,
        Swamp = 1 << 1,
        Sand = 1 << 2
    }
}