﻿using UnityEngine;

namespace _Balance._02_Scripts.Inventory.Items
{
    [CreateAssetMenu(menuName = "Inventory/Item Implementations/Item Implementation")]
    public class ItemImplementation : ScriptableObject
    {
        public EItem ItemType;
        public GameObject PickupPrefab;
    }
}