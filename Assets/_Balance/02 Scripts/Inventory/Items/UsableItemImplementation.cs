﻿using _Balance._02_Scripts.Inventory.CharacterPanel;
using _Balance._02_Scripts.Inventory.Items.UsableItemEffects;
using UnityEngine;

namespace _Balance._02_Scripts.Inventory.Items
{
	[CreateAssetMenu(menuName = "Inventory/Item Implementations/Usable Item Implementation")]
	public class UsableItemImplementation : ItemImplementation
	{
		public UsableItemEffect[] Effects;

		public bool Use(UsableItem item, InventoryController controller)
		{
			foreach (var effect in Effects)
			{
				if (!effect.ExecuteEffect(item, controller))
				{
					return false;
				}
			}

			return true;
		}
	}
}