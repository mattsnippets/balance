﻿using _Balance._02_Scripts.BusinessLogic;
using _Balance._02_Scripts.Inventory.CharacterPanel;
using _Balance._02_Scripts.Inventory.FileIO;
using _Balance._02_Scripts.Inventory.Items;
using UnityEngine;

namespace _Balance._02_Scripts.Inventory.Examples
{
	public class ItemStash : ItemContainer
	{
		[SerializeField] Transform itemsParent;
		[SerializeField] KeyCode openKeyCode = KeyCode.E;

		private bool isOpen;
		private bool isInRange;

		public ItemContainerSaveData Contents { get; set; }
		private InventoryController inventoryController;

		protected override void Awake()
		{
			base.Awake();
		}

		private void Update()
		{
			if (isInRange && Input.GetKeyDown(openKeyCode))
			{
				isOpen = !isOpen;
				itemsParent.gameObject.SetActive(isOpen);

				if (isOpen)
				{
					itemsParent.GetComponentsInChildren(includeInactive: true, result: ItemSlots);
					inventoryController.OpenItemContainer(this);
					ServiceLocator.Resolve<ItemSaveManager>().LoadItems(Contents, this);
				}
				else
				{
					Contents = ServiceLocator.Resolve<ItemSaveManager>().SaveItems(ItemSlots);

					foreach (var itemSlot in ItemSlots)
					{
						itemSlot.Amount = 0;
					}

					inventoryController.CloseItemContainer(this);
					ItemSlots.Clear();
				}
			}
		}

		private void OnTriggerEnter(Collider other)
		{
			CheckCollision(other.gameObject, true);
		}

		private void OnTriggerExit(Collider other)
		{
			CheckCollision(other.gameObject, false);
		}

		private void OnTriggerEnter2D(Collider2D collision)
		{
			CheckCollision(collision.gameObject, true);
		}

		private void OnTriggerExit2D(Collider2D collision)
		{
			CheckCollision(collision.gameObject, false);
		}

		private void CheckCollision(GameObject other, bool state)
		{
			if (other.CompareTag("Player"))
			{
				isInRange = state;

				if (!isInRange && isOpen)
				{
					isOpen = false;
					itemsParent.gameObject.SetActive(false);
					inventoryController.CloseItemContainer(this);
				}

				if (isInRange)
				{
					inventoryController = other.GetComponent<InventoryController>();
					itemsParent = inventoryController.ItemContainerPanel;
				}
				else
				{
					inventoryController = null;
					itemsParent = null;
				}
			}
		}
	}
}