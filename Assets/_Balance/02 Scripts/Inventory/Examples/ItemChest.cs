﻿using _Balance._02_Scripts.Inventory.Items;
using UnityEngine;

namespace _Balance._02_Scripts.Inventory.Examples
{
	public class ItemChest : MonoBehaviour
	{
		[SerializeField] Item item;
		[SerializeField] int amount = 1;
		[SerializeField] CharacterPanel.Inventory inventory;
		[SerializeField] SpriteRenderer spriteRenderer;
		[SerializeField] Color emptyColor;
		[SerializeField] KeyCode itemPickupKeyCode = KeyCode.E;

		private bool _isInRange;
		private bool _isEmpty;

		private void OnValidate()
		{
			if (inventory == null)
			{
				inventory = FindObjectOfType<CharacterPanel.Inventory>();
			}

			if (spriteRenderer == null)
			{
				spriteRenderer = GetComponentInChildren<SpriteRenderer>();
			}

			spriteRenderer.sprite = item.Icon;
			spriteRenderer.enabled = false;
		}

		private void Update()
		{
			if (_isInRange && !_isEmpty && Input.GetKeyDown(itemPickupKeyCode))
			{
				Item itemCopy = item.GetCopy();
				if (inventory.AddItem(itemCopy))
				{
					amount--;
					if (amount == 0)
					{
						_isEmpty = true;
						spriteRenderer.color = emptyColor;
					}
				}
				else
				{
					itemCopy.Destroy();
				}
			}
		}

		private void OnTriggerEnter(Collider other)
		{
			CheckCollision(other.gameObject, true);
		}

		private void OnTriggerExit(Collider other)
		{
			CheckCollision(other.gameObject, false);
		}

		private void OnTriggerEnter2D(Collider2D collision)
		{
			CheckCollision(collision.gameObject, true);
		}

		private void OnTriggerExit2D(Collider2D collision)
		{
			CheckCollision(collision.gameObject, false);
		}

		private void CheckCollision(GameObject gameObject, bool state)
		{
			if (gameObject.CompareTag("Player"))
			{
				_isInRange = state;
				spriteRenderer.enabled = state;
			}
		}
	}
}
