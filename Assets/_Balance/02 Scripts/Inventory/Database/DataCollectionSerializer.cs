﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEditor;
using UnityEngine;

namespace _Balance._02_Scripts.Inventory.Database
{
	public class DataCollectionSerializer : MonoBehaviour
	{
#if UNITY_EDITOR
		public static void ExportAllDataCollections()
		{
			foreach (var collection in GetDataCollections())
			{
				ExportDataCollectionToJson(collection, collection.name);
			}
		}

		public static void ImportAllDataCollections()
		{
			foreach (var collection in GetDataCollections())
			{
				ImportDataCollectionFromJson(collection.name, collection);
			}
		}

		private static List<DataCollection> GetDataCollections()
		{
			var guids = AssetDatabase.FindAssets("t:DataCollection");
			List<DataCollection> dataCollections = new List<DataCollection>();

			foreach (var guid in guids)
			{
				dataCollections.Add(AssetDatabase.LoadAssetAtPath<DataCollection>(AssetDatabase.GUIDToAssetPath(guid)));
			}

			return dataCollections;
		}

		private static void ImportDataCollectionFromJson(string filename, DataCollection collection)
		{
			string input;

			using (var stream = File.Open($"Assets/_Balance/DataExport/{filename}.json", FileMode.Open))
			{
				using (var r = new StreamReader(stream))
				{
					input = r.ReadToEnd();
				}
			}

			var items = JArray.Parse(input).Select(x => x.ToString(Formatting.None)).ToArray();

			for (int i = 0; i < collection.Items.Length; i++)
			{
				JsonUtility.FromJsonOverwrite(items[i], collection.Items[i]);
			}

			Debug.Log($"DataCollection {collection.name} imported from Assets/_Balance/DataExport/{filename}.json");
		}

		private static void ExportDataCollectionToJson(DataCollection collection, string filename)
		{
			var json = DataCollectionToJson(collection);

			using (var stream = File.Open($"Assets/_Balance/DataExport/{filename}.json", FileMode.Create))
			{
				using (var w = new StreamWriter(stream))
				{
					w.Write(json);
					Debug.Log($"DataCollection {collection.name} exported to Assets/_Balance/DataExport/{filename}.json");
				}
			}
		}

		private static string DataCollectionToJson(DataCollection collection)
		{
			StringBuilder s = new StringBuilder();

			bool firstEntry = true;

			s.Append("[");

			foreach (var item in collection.Items)
			{
				if (firstEntry)
				{
					firstEntry = false;
				}
				else
				{
					s.Append(",");
				}

				s.Append(JsonUtility.ToJson(item));
			}

			s.Append("]");
			return s.ToString();
		}
#endif
	}
}