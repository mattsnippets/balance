﻿using UnityEngine;

namespace _Balance._02_Scripts.Inventory.Database
{
    [CreateAssetMenu(menuName = "Inventory/Data Collection")]
    public class DataCollection : ScriptableObject
    {
        public ScriptableObject[] Items;
    }
}