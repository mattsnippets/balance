﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace _Balance._02_Scripts.Inventory.Ui
{
	public class DropItemArea : MonoBehaviour, IDropHandler, IPointerClickHandler
	{
		public event Action OnDropEvent;
		public event Action OnLeftClickEvent;

		public void OnDrop(PointerEventData eventData)
		{
			OnDropEvent?.Invoke();
		}

		public void OnPointerClick(PointerEventData eventData)
		{
			if (eventData != null)
			{
				if (eventData.button == PointerEventData.InputButton.Left)
				{
					OnLeftClickEvent?.Invoke();
				}
			}
		}
	}
}