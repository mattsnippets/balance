﻿using System;
using UnityEngine;

namespace _Balance._02_Scripts.Inventory.Ui
{
	public class QuestionDialog : MonoBehaviour
	{
		public event Action OnYesEvent;
		public event Action OnNoEvent;

		public void Show()
		{
			gameObject.SetActive(true);
			OnYesEvent = null;
			OnNoEvent = null;
		}

		private void Hide()
		{
			gameObject.SetActive(false);
		}

		public void OnYesButtonClick()
		{
			OnYesEvent?.Invoke();
			Hide();
		}

		public void OnNoButtonClick()
		{
			OnNoEvent?.Invoke();
			Hide();
		}
	}
}