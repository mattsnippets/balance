﻿using System;
using System.Linq;
using _Balance._02_Scripts.BusinessLogic;
using UnityEngine;

namespace _Balance._02_Scripts.Inventory.Hotbar
{
    public class Hotbar : MonoBehaviour
    {
        [SerializeField] private HotbarSlot[] _slots;

        public event Action<int> OnSelectedItemActivated;

        private int _selectedSlot;

        private void Start()
        {
            _slots[_selectedSlot].SetSelectedState(true);
        }

        private void Update()
        {
            if (TimeScaleManager.IsPaused)
            {
                return;
            }

            for (int i = 0; i < 9; i++)
            {
                if (Input.GetKeyDown((i + 1).ToString()))
                {
                    SelectSlot(i);
                }
            }

            if (Input.GetAxis("Mouse ScrollWheel") > 0f)
            {
                SelectNextFilledSlot(true);
            }
            else if (Input.GetAxis("Mouse ScrollWheel") < 0f)
            {
                SelectNextFilledSlot(false);
            }

            if (Input.GetMouseButtonDown(1))
            {
                OnSelectedItemActivated?.Invoke(_selectedSlot);
            }
        }

        private void SelectNextFilledSlot(bool upperNeighbor)
        {
            var index = -1;

            if (upperNeighbor)
            {
                index = Array.IndexOf(_slots, _slots.Skip(_selectedSlot + 1).FirstOrDefault(e => e.Item != null));
            }
            else
            {
                index = Array.IndexOf(_slots, _slots.Take(_selectedSlot).LastOrDefault(e => e.Item != null));
            }

            if (index != -1)
            {
                SelectSlot(index);
            }
        }

        private void SelectSlot(int index)
        {
            _slots[_selectedSlot].SetSelectedState(false);
            _slots[index].SetSelectedState(true);
            _selectedSlot = index;
        }
    }
}