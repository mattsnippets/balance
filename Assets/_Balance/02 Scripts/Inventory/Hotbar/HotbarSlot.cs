﻿using _Balance._02_Scripts.Inventory.CharacterPanel;
using UnityEngine;
using UnityEngine.UI;

namespace _Balance._02_Scripts.Inventory.Hotbar
{
    public class HotbarSlot : BaseItemSlot
    {
        [SerializeField] protected Image border;

        private Color selectedBorderColor = new Color32(0, 170, 255, 255);
        private Color normalBorderColor = new Color32(255, 255, 255, 92);

        public void SetSelectedState(bool isSelected)
        {
            border.color = isSelected ? selectedBorderColor : normalBorderColor;
        }
    }
}