﻿using UniRx;
using UnityEngine;

namespace _Balance._02_Scripts.Entities
{
	public class Health : MonoBehaviour
	{
		[SerializeField]
		private float _max = 100f;

		[SerializeField]
		private float _starting = 1f;

		[SerializeField]
		private float _current = 1f;

		public float Current => _current;

		public Subject<float> OnChanged { get; } = new Subject<float>();
		public Subject<Unit> OnDied { get; } = new Subject<Unit>();

		private bool _isDead;

		public void Add(float amount)
		{
			_current = Mathf.Clamp(Current + amount, 0f, _max);
			OnChanged.OnNext(Current);

			if (Current == 0f && !_isDead)
			{
				_isDead = true;
				OnDied.OnNext(Unit.Default);
			}
		}

		public void Reset()
		{
			_current = _starting;
			_isDead = false;
		}
	}
}