﻿using UnityEngine;

namespace _Balance._02_Scripts.Entities
{
	public class Damager : MonoBehaviour
	{
		[SerializeField]
		private float _damage;

		public float Damage { get => _damage; set => _damage = value; }

		private void OnCollisionEnter(Collision collision)
		{
			if (collision.gameObject.TryGetComponent<Damageable>(out var damageable))
			{
				damageable.Hit(_damage);
			}
		}

		private void OnTriggerEnter(Collider other)
		{
			if (other.gameObject.TryGetComponent<Damageable>(out var damageable))
			{
				damageable.Hit(_damage);
			}
		}
	}
}