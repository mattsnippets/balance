﻿using UniRx;
using UnityEngine;

namespace _Balance._02_Scripts.Entities
{
    public class ProjectileTarget : MonoBehaviour
    {
        public Subject<Vector3> OnHit = new Subject<Vector3>();
        public void Hit(Vector3 dir) => OnHit.OnNext(dir);
    }
}
