﻿using Lean.Pool;
using UnityEngine;

namespace _Balance._02_Scripts.Entities
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField] private float _speed;
        [SerializeField] private float _damage;
        [SerializeField] private float _lifeSpan;
        [SerializeField] private float _maxDistance;
        [SerializeField] private LayerMask _layerMask;
        [SerializeField] private GameObject _hitFxPrefab;

        private Vector3 _dir;
        private Vector3 _prevPos;
        private float _lifeTime;
        private float _traversedDistance;
        private bool _isLaunched;
        private Collider _collider;

        private void Awake()
        {
            _collider = GetComponent<Collider>();
        }

        private void OnEnable()
        {
            _lifeTime = 0f;
            _traversedDistance = 0f;
            _isLaunched = false;
            _collider.enabled = false;
        }

        private void Update()
        {
            if (!_isLaunched)
            {
                return;
            }

            if (_lifeTime >= _lifeSpan || _traversedDistance >= _maxDistance)
            {
                LeanPool.Despawn(gameObject);
            }

            _lifeTime += Time.deltaTime;

            transform.Translate(_dir * (Time.deltaTime * _speed), Space.World);
        }

        private void FixedUpdate()
        {
            if (!_isLaunched)
            {
                return;
            }
            
            CheckCollision();
            _prevPos = transform.position;
        }

        private void OnTriggerEnter(Collider other)
        {
            HandleCollision(other);
        }

        public void Launch(Vector3 dir) => Launch(dir, _speed, _maxDistance);

        public void Launch(Vector3 dir, float speed, float maxDistance)
        {
            _dir = dir;
            _maxDistance = maxDistance;
            _collider.enabled = true;
            _isLaunched = true;
            _prevPos = transform.position;
        }

        private void CheckCollision()
        {
            float dist = Vector3.Distance(transform.position, _prevPos);

            _traversedDistance += dist;

            if (Physics.Raycast(
                _prevPos,
                _dir,
                out var hit,
                dist,
                _layerMask))
            {
                HandleCollision(hit.collider);
            }
        }

        private void HandleCollision(Collider other)
        {
            if (other.TryGetComponent<Health>(out var health))
            {
                health.Add(-_damage);
            }

            if (other.TryGetComponent<ProjectileTarget>(out var target))
            {
                target.Hit(_dir);
            }

            LeanPool.Spawn(_hitFxPrefab, transform.position, Quaternion.LookRotation(_dir));
            LeanPool.Despawn(gameObject);
        }
    }
}