﻿using System;
using UniRx;
using UnityEngine;

namespace _Balance._02_Scripts.Entities
{
	[RequireComponent(typeof(Health))]
	public class Damageable : MonoBehaviour
	{
		[SerializeField]
		private double _hitCooldown;

		[SerializeField]
		private GameObject _hitParticles;

		private Health _health;
		private bool _isOnCooldown;

		private void Awake()
		{
			_health = GetComponent<Health>();
		}

		public void Hit(float damage)
		{
			if (!_isOnCooldown && damage > 0f)
			{
				if (_hitParticles && _health.Current > 0f)
				{
					_hitParticles.SetActive(true);
				}
					
				Observable.Timer(TimeSpan.FromSeconds(0.2)).Subscribe(_ => _health.Add(-damage)).AddTo(this);
				_isOnCooldown = true;
				Observable.Timer(TimeSpan.FromSeconds(_hitCooldown)).Subscribe(_ => _isOnCooldown = false).AddTo(this);
			}
		}
	}
}