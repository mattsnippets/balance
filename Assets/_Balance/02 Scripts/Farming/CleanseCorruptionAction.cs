﻿using _Balance._02_Scripts.BusinessLogic;
using _Balance._02_Scripts.ProceduralWorld.Chunks;
using UnityEngine;

namespace _Balance._02_Scripts.Farming
{
	[CreateAssetMenu(menuName = "Farming/Plants/Cleanse Corruption Action")]
	public class CleanseCorruptionAction : GrowthFinishedAction
	{
		public int Radius;

		public override bool Execute(Plant plant)
		{
			ServiceLocator.Resolve<ChunkManager>().ChangeTileStateInCircle(
				plant.transform.position.x,
				plant.transform.position.z,
				Radius, TileState.Protected);
			
			return true;
		}
	}
}