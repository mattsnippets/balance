﻿using UnityEngine;

namespace _Balance._02_Scripts.Farming
{
    public abstract class GrowthFinishedAction : ScriptableObject
    {
        public abstract bool Execute(Plant plant);
    }
}