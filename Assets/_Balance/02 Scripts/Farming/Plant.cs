﻿using _Balance._02_Scripts.BusinessLogic.Events;
using _Balance._02_Scripts.Inventory.Items;
using UnityEngine;

namespace _Balance._02_Scripts.Farming
{
	public class Plant : MonoBehaviour
	{
		[SerializeField]
		private Transform _model;

		[SerializeField]
		private GrowthFinishedAction[] _growthFinishedActions;

		private bool _isGrowing;

		public float FullGrowthTime { get; set; } = 1f;
		public float BalanceChange { get; set; }
		public float LifeTime { get; set; }
		public EPlaceable Type { get; set; }

		private void Awake()
		{
			_isGrowing = true;
			_model.localScale = Vector3.Lerp(
				new Vector3(0.01f, 0.01f, 0.01f),
				Vector3.one,
				LifeTime / FullGrowthTime);
		}

		private void Update()
		{
			if (_isGrowing)
			{
				LifeTime += Time.deltaTime;

				if (LifeTime < FullGrowthTime)
				{
					_model.localScale = Vector3.Lerp(
						new Vector3(0.01f, 0.01f, 0.01f),
						Vector3.one,
						LifeTime / FullGrowthTime);
				}
				else
				{
					_model.localScale = Vector3.one;
					_isGrowing = false;
					new BalanceChangeEvent(BalanceChange).Fire();

					foreach (var action in _growthFinishedActions)
					{
						action.Execute(this);
					}
				}
			}
		}
	}
}