﻿using System;
using _Balance._02_Scripts.BusinessLogic;
using UnityEngine;

namespace _Balance._02_Scripts.DayNightCycle
{
    public class Sun : MonoBehaviour
    {
        [SerializeField] private Light _sun;

        [SerializeField] private Light _moon;

        private float _defaultSunIntensity;
        private float _defaultMoonIntensity;

        [Header("Colors")] [SerializeField] private AmbientColors _midnightColors;

        [SerializeField] private AmbientColors _sunriseColors;

        [SerializeField] private AmbientColors _dayColors;

        [SerializeField] private AmbientColors _sunsetColors;

        private void Start()
        {
            _defaultSunIntensity = _sun.intensity;
            _defaultMoonIntensity = _moon.intensity;
        }

        private void Update()
        {
            var dayTime = ServiceLocator.Resolve<GameState>().DayTime;

            transform.localRotation = Quaternion.Euler((dayTime * 360f) - 90f, 0f, 0f);

            float sunIntensityFactor = 1f;
            float moonIntensityFactor = 0f;

            if (dayTime <= 0.23f || dayTime >= 0.75f)
            {
                sunIntensityFactor = 0f;
                moonIntensityFactor = 1f;
            }
            else if (dayTime <= 0.25f)
            {
                sunIntensityFactor = Mathf.Clamp01((dayTime - 0.23f) * (1f / 0.02f));
            }
            else if (dayTime >= 0.73f)
            {
                sunIntensityFactor = Mathf.Clamp01(1 - ((dayTime - 0.73f) * (1f / 0.02f)));
            }

            if (dayTime <= 0.25f)
            {
                moonIntensityFactor = Mathf.Clamp01(1 - ((dayTime - 0.23f) * (1f / 0.02f)));
            }
            else if (dayTime >= 0.73f)
            {
                moonIntensityFactor = Mathf.Clamp01((dayTime - 0.73f) * (1f / 0.02f));
            }

            _sun.intensity = _defaultSunIntensity * sunIntensityFactor;
            _moon.intensity = _defaultMoonIntensity * moonIntensityFactor;

            if (dayTime < 0.25f)
            {
                RenderSettings.ambientSkyColor = _midnightColors.SkyColor;
                RenderSettings.ambientEquatorColor = _midnightColors.EquatorColor;
                RenderSettings.ambientGroundColor = _midnightColors.GroundColor;
            }
            else if (dayTime < 0.4f)
            {
                RenderSettings.ambientSkyColor = _sunriseColors.SkyColor;
                RenderSettings.ambientEquatorColor = _sunriseColors.EquatorColor;
                RenderSettings.ambientGroundColor = _sunriseColors.GroundColor;
            }
            else if (dayTime < 0.7f)
            {
                RenderSettings.ambientSkyColor = _dayColors.SkyColor;
                RenderSettings.ambientEquatorColor = _dayColors.EquatorColor;
                RenderSettings.ambientGroundColor = _dayColors.GroundColor;
            }
            else if (dayTime < 0.8f)
            {
                RenderSettings.ambientSkyColor = _sunsetColors.SkyColor;
                RenderSettings.ambientEquatorColor = _sunsetColors.EquatorColor;
                RenderSettings.ambientGroundColor = _sunsetColors.GroundColor;
            }
            else
            {
                RenderSettings.ambientSkyColor = _midnightColors.SkyColor;
                RenderSettings.ambientEquatorColor = _midnightColors.EquatorColor;
                RenderSettings.ambientGroundColor = _midnightColors.GroundColor;
            }
        }

        private static float CalculateAngle(Vector3 from, Vector3 to) =>
            Quaternion.FromToRotation(Vector3.up, to - from).eulerAngles.x;
    }

    [Serializable]
    public struct AmbientColors
    {
        [ColorUsage(true, true)] public Color SkyColor;
        [ColorUsage(true, true)] public Color EquatorColor;
        [ColorUsage(true, true)] public Color GroundColor;
    }
}