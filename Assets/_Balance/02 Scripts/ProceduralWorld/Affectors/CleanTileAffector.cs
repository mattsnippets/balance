﻿using System;
using _Balance._02_Scripts.ProceduralWorld.Chunks;
using UnityEngine;

namespace _Balance._02_Scripts.ProceduralWorld.Affectors
{
	public class CleanTileAffector : TileAffector
	{
		public override TileState Affect(
			TileState affectingState,
			Action<Tile, TileState> onTileStateChanged,
			Tile tile)
		{
			switch (affectingState)
			{
				case TileState.Clean:
					return TileState.Clean;
				case TileState.Planted:
					onTileStateChanged(tile, TileState.Planted);
					return TileState.Planted;
				case TileState.Protected:
					onTileStateChanged(tile, TileState.Protected);
					return TileState.Protected;
				case TileState.Corrupted:
					onTileStateChanged(tile, TileState.Corrupted);
					return TileState.Corrupted;
				case TileState.Tree:
					onTileStateChanged(tile, TileState.Tree);
					return TileState.Tree;
				default:
					Debug.LogWarning($"This tile type doesn't handle being affected by {affectingState}");
					return TileState.Clean;
			}
		}
	}
}