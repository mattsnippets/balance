﻿using System;
using _Balance._02_Scripts.ProceduralWorld.Chunks;
using UnityEngine;

namespace _Balance._02_Scripts.ProceduralWorld.Affectors
{
	public class CorruptedTileAffector : TileAffector
	{
		public override TileState Affect(
			TileState affectingState,
			Action<Tile, TileState> onTileStateChanged,
			Tile tile)
		{
			switch (affectingState)
			{
				case TileState.Clean:
					onTileStateChanged(tile, TileState.Clean);
					return TileState.Clean;
				case TileState.Planted:
					return TileState.Corrupted;
				case TileState.Protected:
					onTileStateChanged(tile, TileState.Protected);
					return TileState.Protected;
				case TileState.Corrupted:
					return TileState.Corrupted;
				case TileState.Tree:
					return TileState.Corrupted;
				default:
					Debug.LogWarning($"This tile type doesn't handle being affected by {affectingState}");
					return TileState.Corrupted;
			}
		}
	}
}