﻿using System;
using _Balance._02_Scripts.ProceduralWorld.Chunks;
using UnityEngine;

namespace _Balance._02_Scripts.ProceduralWorld.Affectors
{
	public class PlantedTileAffector : TileAffector
	{
		public override TileState Affect(
			TileState affectingState,
			Action<Tile, TileState> onTileStateChanged,
			Tile tile)
		{
			switch (affectingState)
			{
				case TileState.Clean:
					return TileState.Planted;
				case TileState.Planted:
					return TileState.Planted;
				case TileState.Protected:
					return TileState.Planted;
				case TileState.Corrupted:
					return TileState.Planted;
				case TileState.Tree:
					onTileStateChanged(tile, TileState.Tree);
					return TileState.Tree;
				default:
					Debug.LogWarning($"This tile type doesn't handle being affected by {affectingState}");
					return TileState.Planted;
			}
		}
	}
}