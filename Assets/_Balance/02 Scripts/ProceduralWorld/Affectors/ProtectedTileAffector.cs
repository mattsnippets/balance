﻿using System;
using _Balance._02_Scripts.ProceduralWorld.Chunks;

namespace _Balance._02_Scripts.ProceduralWorld.Affectors
{
	public class ProtectedTileAffector : TileAffector
	{
		public override TileState Affect(
			TileState affectingState,
			Action<Tile, TileState> onTileStateChanged,
			Tile tile)
		{
			switch (affectingState)
			{
				case TileState.Planted:
					onTileStateChanged(tile, TileState.Planted);
					return TileState.Planted;
				default:
					return TileState.Protected;
			}
		}
	}
}