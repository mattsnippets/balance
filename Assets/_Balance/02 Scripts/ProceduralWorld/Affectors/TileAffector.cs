﻿using System;
using _Balance._02_Scripts.ProceduralWorld.Chunks;

namespace _Balance._02_Scripts.ProceduralWorld.Affectors
{
	public abstract class TileAffector
	{
		public abstract TileState Affect(
			TileState affectingState,
			Action<Tile, TileState> onTileStateChanged,
			Tile tile);
	}
}