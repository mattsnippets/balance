﻿namespace _Balance._02_Scripts.ProceduralWorld.Affectors
{
	public static class AffectorLocator
	{
		public static readonly CleanTileAffector CleanTileAffector = new CleanTileAffector();
		public static readonly CorruptedTileAffector CorruptedTileAffector = new CorruptedTileAffector();
		public static readonly PlantedTileAffector PlantedTileAffector = new PlantedTileAffector();
		public static readonly ProtectedTileAffector ProtectedTileAffector = new ProtectedTileAffector();
		public static readonly TreeTileAffector TreeTileAffector = new TreeTileAffector();
	}
}