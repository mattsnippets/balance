﻿using UnityEngine;

namespace _Balance._02_Scripts.ProceduralWorld.MapGenerator
{
	public class NoiseGenerator
	{
		public enum NormalizeMode
		{
			Local,
			Global
		}

		public float[,] GenerateNoiseMap(MapGenerationData data)
		{
			var noiseMap = new float[data.Width, data.Height];

			System.Random prng = new System.Random(data.Seed);

			Vector2[] octaveOffsets = new Vector2[data.Octaves];

			float maxPossibleHeight = 0;
			float amplitude = 1;
			float frequency = 1;

			for (var i = 0; i < data.Octaves; i++)
			{
				var offsetX = prng.Next(-100000, 100000) + data.Center.y;
				var offsetY = prng.Next(-100000, 100000) - data.Center.x;
				octaveOffsets[i] = new Vector2(offsetX, offsetY);

				maxPossibleHeight += amplitude;
				amplitude *= data.Persistance;
			}

			if (data.Scale <= 0)
			{
				data.Scale = 0.0001f;
			}

			var maxLocalNoiseHeight = float.MinValue;
			var minLocalNoiseHeight = float.MaxValue;

			var halfWidth = data.Width / 2f;
			var halfHeight = data.Height / 2f;

			for (var row = 0; row < data.Width; row++)
			{
				for (var column = 0; column < data.Height; column++)
				{
					amplitude = 1;
					frequency = 1;
					float noiseHeight = 0;

					for (var i = 0; i < data.Octaves; i++)
					{
						var sampleX = (column - halfWidth + octaveOffsets[i].x) / data.Scale * frequency;
						var sampleY = (row - halfHeight + octaveOffsets[i].y) / data.Scale * frequency;

						var perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;

						noiseHeight += perlinValue * amplitude;

						amplitude *= data.Persistance;
						frequency *= data.Lacunarity;
					}

					if (noiseHeight > maxLocalNoiseHeight)
					{
						maxLocalNoiseHeight = noiseHeight;
					}
					else if (noiseHeight < minLocalNoiseHeight)
					{
						minLocalNoiseHeight = noiseHeight;
					}
					noiseMap[row, column] = noiseHeight;
				}
			}

			for (var row = 0; row < data.Width; row++)
			{
				for (var column = 0; column < data.Height; column++)
				{
					switch (data.NormalizeMode)
					{
						case NormalizeMode.Local:
							noiseMap[column, row] = Mathf.InverseLerp(minLocalNoiseHeight, maxLocalNoiseHeight, noiseMap[column, row]);
							break;
						case NormalizeMode.Global:
							var normalizedHeight = (noiseMap[column, row] + 1) / maxPossibleHeight;
							noiseMap[column, row] = Mathf.Clamp(normalizedHeight, 0, int.MaxValue);
							break;
					}
				}
			}

			return noiseMap;
		}
	}
}