﻿using UnityEngine;

namespace _Balance._02_Scripts.ProceduralWorld.MapGenerator
{
	public class MapDisplay : MonoBehaviour
	{
		public Renderer TextureRender;

		public void DrawTexture(Texture2D texture)
		{
			TextureRender.sharedMaterial.SetTexture("_BaseMap", texture);
			//textureRender.transform.localScale = new Vector3 (texture.width, 1, texture.height);
		}
	}
}