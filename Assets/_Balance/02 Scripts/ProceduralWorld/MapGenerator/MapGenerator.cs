﻿using System;
using UnityEngine;

namespace _Balance._02_Scripts.ProceduralWorld.MapGenerator
{
	public class MapGenerator
	{
		private NoiseGenerator.NormalizeMode normalizeMode;
		private float scale;
		private int octaves;
		private float persistance;
		private float lacunarity;
		private int seed;
		private static NoiseGenerator noiseGenerator = new NoiseGenerator();

		public int Seed
		{
			get { return seed; }
		}

		public MapGenerator()
		{

		}

		public MapGenerator(NoiseGenerator.NormalizeMode normalizeMode, float scale, int octaves, float persistance,
			float lacunarity, int seed, TestMapTerrainType[] regions)
		{
			this.normalizeMode = normalizeMode;
			this.scale = scale;
			this.octaves = octaves;
			this.persistance = persistance;
			this.lacunarity = lacunarity;
			this.seed = seed;

			if (octaves < 1)
			{
				octaves = 1;
				Debug.LogWarning("Map generator octaves should be at least 1. Value set to 1.");
			}

			Debug.Assert(regions.Length > 0, "Regions array length should be a positive integer.");
			Debug.Assert(lacunarity >= 1, "Lacunarity should be 1 or bigger.");
		}

		public float[,] GenerateMap(MapGenerationData data) => noiseGenerator.GenerateNoiseMap(data);
	}

	[Serializable]
	public struct MapGenerationData
	{
		public int Width;
		public int Height;
		public int Seed;
		public float Scale;
		public int Octaves;
		public float Persistance;
		public float Lacunarity;
		public Vector2 Center;
		public NoiseGenerator.NormalizeMode NormalizeMode;
	}
}