﻿using UnityEngine;

namespace _Balance._02_Scripts.ProceduralWorld.MapGenerator
{
	public static class MapTextureGenerator
	{
		public static Texture2D TextureFromColorMap(Color[] colorMap, int width, int height)
		{
			Texture2D texture = new Texture2D(width, height);
			texture.filterMode = FilterMode.Point;
			texture.wrapMode = TextureWrapMode.Clamp;
			texture.SetPixels(colorMap);
			texture.Apply();

			return texture;
		}

		public static Texture2D TextureFromHeightMap(float[,] heightMap)
		{
			var width = heightMap.GetLength(0);
			var height = heightMap.GetLength(1);

			Color[] colorMap = new Color[width * height];
			for (var y = 0; y < height; y++)
			{
				for (var x = 0; x < width; x++)
				{
					colorMap[y * width + x] = Color.Lerp(Color.black, Color.white, heightMap[x, y]);
				}
			}

			return TextureFromColorMap(colorMap, width, height);
		}
	}
}