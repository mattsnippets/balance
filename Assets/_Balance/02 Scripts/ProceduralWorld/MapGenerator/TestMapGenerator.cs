﻿using UnityEngine;

namespace _Balance._02_Scripts.ProceduralWorld.MapGenerator
{
	public class TestMapGenerator : MonoBehaviour
	{
		public enum DrawMode { NoiseMap, ColorMap };
		public DrawMode CurrentDrawMode;
		public NoiseGenerator.NormalizeMode NormalizeMode;

		public int MapWidth;
		public int MapHeight;
		public float NoiseScale;
		public int Octaves;
		[Range(0, 1)]
		public float Persistance;
		public float Lacunarity;
		public int Seed;
		public Vector2 Center;

		public bool AutoUpdate;

		public TestMapTerrainType[] Regions;

		private MapGenerator mapGenerator;

		public void DrawHeightmapPreview()
		{
			if (MapWidth < 1)
			{
				MapWidth = 1;
			}
			if (MapHeight < 1)
			{
				MapHeight = 1;
			}
			if (Lacunarity < 1)
			{
				Lacunarity = 1;
			}
			if (Octaves < 1)
			{
				Octaves = 1;
			}

			if (mapGenerator == null)
			{
				mapGenerator = new MapGenerator(NormalizeMode, NoiseScale, Octaves, Persistance, Lacunarity, Seed, Regions);
			}

			var noiseMap = new MapGenerator().GenerateMap(
				new MapGenerationData()
				{
					Width = MapWidth,
					Height = MapHeight,
					Seed = Seed,
					Scale = NoiseScale,
					Octaves = Octaves,
					Persistance = Persistance,
					Lacunarity = Lacunarity,
					Center = Center,
					NormalizeMode = NormalizeMode
				});

			Color[] colorMap = new Color[MapWidth * MapHeight];

			for (var y = 0; y < MapHeight; y++)
			{
				for (var x = 0; x < MapWidth; x++)
				{
					var currentHeight = noiseMap[x, y];

					for (var i = 0; i < Regions.Length; i++)
					{
						if (currentHeight <= Regions[i].height)
						{
							colorMap[x * MapHeight + y] = Regions[i].color;
							break;
						}
					}
				}
			}

			MapDisplay display = gameObject.GetComponent<MapDisplay>();

			if (CurrentDrawMode == DrawMode.NoiseMap)
			{
				display.DrawTexture(MapTextureGenerator.TextureFromHeightMap(noiseMap));
			}
			else if (CurrentDrawMode == DrawMode.ColorMap)
			{
				display.DrawTexture(MapTextureGenerator.TextureFromColorMap(colorMap, MapWidth, MapHeight));
			}
		}
	}

	[System.Serializable]
	public struct TestMapTerrainType
	{
		public string name;
		public float height;
		public Color color;
	}
}