﻿using System.Collections.Generic;
using _Balance._02_Scripts.BusinessLogic;
using _Balance._02_Scripts.ProceduralWorld.Chunks;
using UnityEngine;
using UnityEngine.UI;

namespace _Balance._02_Scripts.ProceduralWorld.MiniMap
{
    public class MiniMap : MonoBehaviour
    {
        [SerializeField] private GameObject _tilePrefab;
        [SerializeField] private Transform _tilesParent;
        [SerializeField] private Transform _homeMarker;
        [SerializeField] private Transform _playerMarker;

        private Color32 _grass;
        private Color32 _water;
        private Color32 _tree;
        
        private Dictionary<Vector2, RawImage> _mapTiles = new Dictionary<Vector2, RawImage>();
        private MapSettings _settings;
        private ChunkManager _chunkManager;
        private int _width;
        private int _height;

        private void Start()
        {
            _grass = new Color32(0x39, 0xB2, 0x56, 0xFF);
            _water = new Color32(0x25, 0x96, 0xBE, 0xFF);
            _tree = new Color32(97, 67, 24, 255);
            
            _settings = ServiceLocator.Resolve<Settings>().MapSettings;
            _chunkManager = ServiceLocator.Resolve<ChunkManager>();
            _width = _settings.Width - 1;
            _height = _settings.Height - 1;
        }

        public void TrackPlayerPosition(float x, float y, float yRotation)
        {
            _tilesParent.localPosition = new Vector3(-x, -y, 0f);
            _playerMarker.localRotation = Quaternion.Euler(0f, 0f, -yRotation);
        }

        public void TryCreateMiniMapTile(Vector2 tilePos, Tile[][] tileData)
        {
            if (_mapTiles.ContainsKey(tilePos))
            {
                return;
            }

            var tile = Instantiate(_tilePrefab, _tilesParent);
            tile.transform.localPosition = new Vector3(
                tilePos.x * _settings.ActualChunkSize,
                tilePos.y * _settings.ActualChunkSize, 0f);
            tile.name = $"MiniMap {tilePos.x} {tilePos.y}";
            
            tile.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, _settings.ActualChunkSize);
            tile.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, _settings.ActualChunkSize);

            var tileImage = tile.GetComponent<RawImage>();

            Texture2D texture = new Texture2D(_width, _height, TextureFormat.RGB24, true)
            {
                filterMode = FilterMode.Point
            };

            tileImage.texture = texture;

            UpdateTileTexture(tileData, tileImage);
            
            _mapTiles[tilePos] = tileImage;

            _homeMarker.SetSiblingIndex(_mapTiles.Count);
        }

        public void UpdateActiveTiles()
        {
            foreach (var chunk in _chunkManager.ActiveChunks)
            {
                if (!_mapTiles.ContainsKey(chunk.ChunkPosition))
                {
                    continue;
                }

                UpdateTileTexture(chunk.Tiles, _mapTiles[chunk.ChunkPosition]);
            }
        }

        private void UpdateTileTexture(Tile[][] tileData, RawImage tileImage)
        {
            Texture2D texture = (Texture2D) tileImage.texture;

            for (var i = 0; i < _height; i++)
            {
                for (var j = 0; j < _width; j++)
                {
                    Color color = _grass;

                    var tile = tileData[i][j];

                    if (tile.Elevation == Elevation.Water)
                    {
                        color = _water;
                    }

                    switch (tile.State)
                    {
                        // case TileState.Clean:
                        //     color = new Color32(0x39, 0xB2, 0x56, 0xFF);
                        //     break;
                        case TileState.Planted:
                            color = Color.yellow;
                            break;
                        case TileState.Protected:
                            color = Color.cyan;
                            break;
                        case TileState.Corrupted:
                            color = Color.grey;
                            break;
                        case TileState.Tree:
                            color = _tree;
                            break;
                    }

                    texture.SetPixel(i, j, color);
                }
            }

            texture.Apply();

            tileImage.texture = texture;
        }
    }
}