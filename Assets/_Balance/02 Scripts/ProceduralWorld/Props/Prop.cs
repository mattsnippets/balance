﻿using _Balance._02_Scripts.Inventory.Items;
using UnityEngine;

namespace _Balance._02_Scripts.ProceduralWorld.Props
{
	public class Prop : MonoBehaviour
	{
		public EProp Type { get; set; }
	}
}