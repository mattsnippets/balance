﻿using UnityEngine;

namespace _Balance._02_Scripts.ProceduralWorld.Props
{
	[CreateAssetMenu(menuName = "Map/Map Props")]
	public class Trees : ScriptableObject
	{
		public GameObject[] TreePrefabs;
	}
}