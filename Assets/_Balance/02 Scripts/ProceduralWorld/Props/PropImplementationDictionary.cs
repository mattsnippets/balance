﻿using _Balance._02_Scripts.Utilities.SerializableDictionaries;
using UnityEngine;

namespace _Balance._02_Scripts.ProceduralWorld.Props
{
    [CreateAssetMenu(menuName = "Map/Prop Implementation Dictionary")]
    public class PropImplementationDictionary : ScriptableObject
    {
        public EPropPropImplementationDictionary Items;
    }
}