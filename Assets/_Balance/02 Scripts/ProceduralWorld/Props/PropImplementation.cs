﻿using _Balance._02_Scripts.Inventory.Items;
using UnityEngine;

namespace _Balance._02_Scripts.ProceduralWorld.Props
{
    [CreateAssetMenu(menuName = "Map/Prop Implementation")]
    public class PropImplementation : ScriptableObject
    {
        public EProp Type;
        public GameObject PropPrefab;
    }
}