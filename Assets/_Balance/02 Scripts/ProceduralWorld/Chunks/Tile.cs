﻿using System;
using _Balance._02_Scripts.BusinessLogic;
using _Balance._02_Scripts.ProceduralWorld.Affectors;
using UnityEngine;

namespace _Balance._02_Scripts.ProceduralWorld.Chunks
{
    [Flags]
    public enum TileState
    {
        None = 0,
        Clean = 1 << 0,
        Planted = 1 << 1,
        Protected = 1 << 2,
        Corrupted = 1 << 3,
        Tree = 1 << 4
    }

    public enum Elevation
    {
        Water = 0,
        Shore = 1,
        Grass = 2,
        Highland = 3,
        Hill = 4,
        Mountain = 5,
    }

    public enum TileRatioChange
    {
        Cleaned,
        Polluted
    }

    public class Tile
    {
        public int X { get; private set; }
        public int Y { get; private set; }

        public TileState State
        {
            get => _state;

            set { _state = value; }
        }

        public float Height
        {
            get => _height;
            set
            {
                _height = Mathf.Max(0.3f, value);
                var clampedInput = Mathf.Min(value, 1f);
                var remapped = MapSettings.HeightCurve.Evaluate(clampedInput);
                Elevation = (Elevation) Mathf.FloorToInt(remapped);
            }
        }

        public float ScaledHeight => Height * MapSettings.HeightMultiplier;

        public Elevation Elevation { get; private set; }
        public Vector3 WorldPos { get; }
        public float DistanceRatio { get; }
        public Vector3 SurfacePositionInChunk { get; set; }
        public Vector3 SurfaceNormal { get; set; }

        public static MapSettings MapSettings;

        private TileState _state;
        private TileAffector _affector;
        private Action<Tile, TileState> _onTileStateChanged;
        private float _height;


        public Tile(
            int x,
            int y,
            TileState state,
            Action<Tile, TileState> onTileStateChanged,
            Vector3 worldPos)
        {
            X = x;
            Y = y;
            State = state;
            SetAffector(state);
            _onTileStateChanged = onTileStateChanged;
            WorldPos = worldPos;
            DistanceRatio = (Vector3.zero - WorldPos).sqrMagnitude / MapSettings.TileDistanceRatioDivisor;
        }

        private void SetAffector(TileState state)
        {
            switch (state)
            {
                case TileState.Clean:
                    _affector = AffectorLocator.CleanTileAffector;
                    break;
                case TileState.Planted:
                    _affector = AffectorLocator.PlantedTileAffector;
                    break;
                case TileState.Protected:
                    _affector = AffectorLocator.ProtectedTileAffector;
                    break;
                case TileState.Corrupted:
                    _affector = AffectorLocator.CorruptedTileAffector;
                    break;
                case TileState.Tree:
                    _affector = AffectorLocator.TreeTileAffector;
                    break;
                default:
                    Debug.LogWarning($"No suitable affector found for tile state: {state}");
                    break;
            }
        }

        public void Affect(TileState affectingState)
        {
            TileState newState = _affector.Affect(
                affectingState,
                _onTileStateChanged,
                this);

            if (State != newState)
            {
                State = newState;
                SetAffector(newState);
            }
        }
    }
}
