﻿using System;
using _Balance._02_Scripts.BusinessLogic;
using UnityEngine;

namespace _Balance._02_Scripts.ProceduralWorld.Chunks
{
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public class ChunkMesh : MonoBehaviour
    {
        private float _tileSize;
        private int _borderedSize;
        private int _meshSize;
        private int _tileResolution;
        private bool _isInitialized;
        private MapSettings _mapSettings;

        public Mesh Mesh => _mesh;

        private Mesh _mesh;
        private MeshData _meshData;
        private float _normalizedTextureTileSize;
        private Vector2[][] _textureCoords;
        private int _simplificationIncrement;

        private MapSettings MapSettings
        {
            get
            {
                if (_mapSettings == null)
                {
                    _mapSettings = ServiceLocator.Resolve<Settings>().MapSettings;
                }

                return _mapSettings;
            }
        }

        private void Init()
        {
            _isInitialized = true;
            _tileSize = MapSettings.TileSize;
            _normalizedTextureTileSize = 1f / _mapSettings.AtlasSize;

            var texCount = MapSettings.AtlasSize * MapSettings.AtlasSize;
            _textureCoords = new Vector2[texCount][];

            for (int i = 0; i < texCount; i++)
            {
                _textureCoords[i] = CreateTileTexture(i);
            }
        }

        public Vector2[] GenerateTextureAssignNormals(
            Tile[][] tileData,
            int uvsLength,
            Vector3[] meshNormals,
            int[] meshTriangles,
            Vector3[] meshVertices)
        {
            Vector2[] uvs = new Vector2[uvsLength];
            Vector3[] normals = new Vector3[6];
            var length = Mathf.Pow(_meshSize - 1, 2);

            for (int i = 0; i < length; i++)
            {
                var index = i * 6;
                Tile tile = tileData[i / (_meshSize - 1)][i % (_meshSize - 1)];
                Vector2[] coords = _textureCoords[(int)tile.Elevation];

                for (int j = 0; j < 6; j++)
                {
                    uvs[index + j] = coords[j];
                    normals[j] = meshNormals[meshTriangles[index + j]];
                }

                var tri1Normal = (normals[0] + normals[1] + normals[2]) / 3f;
                var tri2Normal = (normals[3] + normals[4] + normals[5]) / 3f;

                Vector3 a = meshVertices[meshTriangles[index]];
                Vector3 b = meshVertices[meshTriangles[index + 3]];

                tile.SurfacePositionInChunk = new Vector3((a.x + b.x) / 2f, (a.y + b.y) / 2f, (a.z + b.z) / 2f);
                tile.SurfaceNormal = (tri1Normal + tri2Normal) / 2f;
            }

            return uvs;
        }

        public void AssignUvs(Vector2[] uvs) => _mesh.uv = uvs;

        private Vector2[] CreateTileTexture(int textureId)
        {
            var coords = new Vector2[6];

            float y = textureId / _mapSettings.AtlasSize;
            float x = textureId - (y * _mapSettings.AtlasSize);

            x *= _normalizedTextureTileSize;
            y *= _normalizedTextureTileSize;

            y = 1f - y - _normalizedTextureTileSize;

            var padding = 0.01f;

            coords[0] = new Vector2(x + padding, y + _normalizedTextureTileSize - padding);
            coords[1] = new Vector2(x + _normalizedTextureTileSize - padding, y + padding);
            coords[2] = new Vector2(x + padding, y + padding);
            coords[3] = new Vector2(x + _normalizedTextureTileSize - padding, y + padding);
            coords[4] = new Vector2(x + padding, y + _normalizedTextureTileSize - padding);
            coords[5] = new Vector2(x + _normalizedTextureTileSize - padding, y + _normalizedTextureTileSize - padding);

            return coords;
        }

        public void GenerateMesh(Tile[][] tileData)
        {
            if (!_isInitialized)
            {
                Init();
            }

            //int meshSimplificationIncrement = (levelOfDetail == 0)?1:levelOfDetail * 2;
            int simplificationIncrement = 1;

            _borderedSize = tileData.Length;
            _simplificationIncrement = simplificationIncrement;
            _meshSize = _borderedSize - 2 * _simplificationIncrement;

            int verticesPerLine = (_meshSize - 1) / _simplificationIncrement + 1;

            var mapSettings = MapSettings;

            _meshData = new MeshData(verticesPerLine);

            int[][] vertexIndicesMap = new int[_borderedSize][];

            for (int index = 0; index < _borderedSize; index++)
            {
                vertexIndicesMap[index] = new int[_borderedSize];
            }

            SetVertexIndices(vertexIndicesMap);
            SetVerticesAndTriangles(tileData, vertexIndicesMap, mapSettings);

            _meshData.GenerateNormals();
        }

        private void SetVerticesAndTriangles(Tile[][] tileData, int[][] vertexIndicesMap, MapSettings mapSettings)
        {
            int meshSizeUnsimplified = _borderedSize - 2;

            for (int y = 0; y < _borderedSize; y += _simplificationIncrement)
            {
                for (int x = 0; x < _borderedSize; x += _simplificationIncrement)
                {
                    int vertexIndex = vertexIndicesMap[x][y];

                    Vector2 percent = new Vector2(
                        (x - _simplificationIncrement) / (float)_meshSize,
                        (y - _simplificationIncrement) / (float)_meshSize);

                    float height = tileData[y][x].Height * mapSettings.HeightMultiplier;

                    Vector3 vertexPosition = new Vector3(
                        percent.x * meshSizeUnsimplified * _tileSize,
                        height,
                        -percent.y * meshSizeUnsimplified * _tileSize);

                    _meshData.AddVertex(vertexPosition, percent, vertexIndex);

                    if (x < _borderedSize - 1 && y < _borderedSize - 1)
                    {
                        int a = vertexIndicesMap[x][y];
                        int b = vertexIndicesMap[x + _simplificationIncrement][y];
                        int c = vertexIndicesMap[x][y + _simplificationIncrement];
                        int d = vertexIndicesMap[x + _simplificationIncrement][y + _simplificationIncrement];
                        _meshData.AddTriangle(a, d, c);
                        _meshData.AddTriangle(d, a, b);
                    }
                }
            }
        }

        private void SetVertexIndices(int[][] vertexIndicesMap)
        {
            int borderVertexIndex = -1;
            int meshVertexIndex = 0;

            for (int y = 0; y < _borderedSize; y += _simplificationIncrement)
            {
                for (int x = 0; x < _borderedSize; x += _simplificationIncrement)
                {
                    bool isBorderVertex = y == 0 || y == _borderedSize - 1 || x == 0 || x == _borderedSize - 1;

                    if (isBorderVertex)
                    {
                        vertexIndicesMap[x][y] = borderVertexIndex;
                        borderVertexIndex--;
                    }
                    else
                    {
                        vertexIndicesMap[x][y] = meshVertexIndex;
                        meshVertexIndex++;
                    }
                }
            }
        }

        public void AssignMesh()
        {
            _mesh = _meshData.CreateMesh();

            GetComponent<MeshFilter>().mesh = _mesh;
            GetComponent<MeshCollider>().sharedMesh = _mesh;

            transform.localPosition = new Vector3(
                -_borderedSize / 2f * _tileSize,
                0f,
                _borderedSize / 2f * _tileSize);
        }

        public class MeshData
        {
            private Vector3[] _vertices;
            private readonly int[] _triangles;
            private Vector2[] _uvs;

            private readonly Vector3[] _borderVertices;
            private readonly int[] _borderTriangles;

            private int _triangleIndex;
            private int _borderTriangleIndex;

            public MeshData(int verticesPerLine)
            {
                _vertices = new Vector3[verticesPerLine * verticesPerLine];
                _uvs = new Vector2[verticesPerLine * verticesPerLine];
                _triangles = new int[(verticesPerLine - 1) * (verticesPerLine - 1) * 6];

                _borderVertices = new Vector3[verticesPerLine * 4 + 4];
                _borderTriangles = new int[24 * verticesPerLine];
            }

            public void AddVertex(Vector3 vertexPos, Vector2 uv, int vertexIndex)
            {
                if (vertexIndex < 0)
                {
                    _borderVertices[-vertexIndex - 1] = vertexPos;
                }
                else
                {
                    _vertices[vertexIndex] = vertexPos;
                    _uvs[vertexIndex] = uv;
                }
            }

            public void GenerateNormals()
            {
                FlatShading();
            }

            public void AddTriangle(int a, int b, int c)
            {
                if (a < 0 || b < 0 || c < 0)
                {
                    _borderTriangles[_borderTriangleIndex] = a;
                    _borderTriangles[_borderTriangleIndex + 1] = b;
                    _borderTriangles[_borderTriangleIndex + 2] = c;
                    _borderTriangleIndex += 3;
                }
                else
                {
                    _triangles[_triangleIndex] = a;
                    _triangles[_triangleIndex + 1] = b;
                    _triangles[_triangleIndex + 2] = c;
                    _triangleIndex += 3;
                }
            }

            private void FlatShading()
            {
                Vector3[] flatShadedVertices = new Vector3[_triangles.Length];
                Vector2[] flatShadedUVs = new Vector2[_triangles.Length];

                for (int i = 0; i < _triangles.Length; i++)
                {
                    flatShadedVertices[i] = _vertices[_triangles[i]];
                    flatShadedUVs[i] = _uvs[_triangles[i]];
                    _triangles[i] = i;
                }

                _vertices = flatShadedVertices;
                _uvs = flatShadedUVs;
            }

            public Mesh CreateMesh()
            {
                Mesh mesh = new Mesh
                {
                    vertices = _vertices,
                    triangles = _triangles,
                    uv = _uvs
                };
                mesh.RecalculateNormals();
                return mesh;
            }
        }
    }
}
