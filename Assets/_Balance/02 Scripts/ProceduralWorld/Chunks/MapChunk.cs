﻿using System;
using System.Linq;
using System.Threading.Tasks;
using _Balance._02_Scripts.BusinessLogic;
using _Balance._02_Scripts.Inventory.Items;
using _Balance._02_Scripts.Inventory.ItemSpawning;
using _Balance._02_Scripts.Utilities;
using UniRx;
using UnityEngine;

namespace _Balance._02_Scripts.ProceduralWorld.Chunks
{
    public class MapChunk : MonoBehaviour
    {
        [SerializeField] private ChunkMesh _chunkMesh;
        [SerializeField] private AnimationCurve _edgeHeightFactor;
        [SerializeField] private GameObject _canopyShadowPrefab;

        private static readonly float[] PropSizeVariants = { 1f, 0.5f, 1.2f, 1.5f };

        private int _width;
        private int _height;
        private float _tileSize;
        private Tile[][] _tiles;
        private int _chunkCoordX;
        private int _chunkCoordZ;
        private Vector2 _chunkPosition;

        private WeightedRandomizer _spawnRandomizer;
        private int _chunkSeed;
        private System.Random _random;
        private float[,] _terrainData;
        private float[,] _treeAndPropsData;

        private bool _shouldEnqueue;
        private bool _isEdgeChunk;

        private LayerMask _groundLayerMask;
        private RaycastHit[] _spawnPlacementHits = new RaycastHit[1];

        private ChunkElementSpawner _elementSpawner;

        private Settings _settings;
        private MapSettings _mapSettings;
        private GameState _gameState;
        private ImplementationProvider _implementationProvider;

        public int Width => _width;
        public int Height => _height;
        public float TileSize => _tileSize;

        public Tile[][] Tiles => _tiles;

        public int ChunkCoordX => _chunkCoordX;
        public int ChunkCoordZ => _chunkCoordZ;
        public Vector2 ChunkPosition => _chunkPosition;

        public System.Random Random => _random;

        public bool IsEdgeChunk => _isEdgeChunk;

        public Tile GetTile(int x, int z)
        {
            if (IsWithinBounds(x, z))
            {
                return _tiles[z][x];
            }

            return null;
        }

        public int WorldXToMap(float coord) => Mathf.FloorToInt((coord - _chunkCoordX * _mapSettings.ActualChunkSize) / _tileSize + _width / 2f);
        public int WorldZToMap(float coord) => Mathf.FloorToInt(-(coord - _chunkCoordZ * _mapSettings.ActualChunkSize) / _tileSize + _height / 2f);
        public Vector3 WorldPosToMap(Vector3 pos) => new Vector3(WorldXToMap(pos.x), pos.y, WorldZToMap(pos.z));
        public float MapXToWorld(int coord) => ((coord - _width / 2f) * _tileSize + _tileSize / 2f) + _chunkCoordX * _mapSettings.ActualChunkSize;
        public float MapZToWorld(int coord) => ((-coord + _height / 2f) * _tileSize - _tileSize / 2f) + _chunkCoordZ * _mapSettings.ActualChunkSize;
        public Vector3 MapPosToWorld(int x, int z) => new Vector3(MapXToWorld(x), 0f, MapZToWorld(z));
        private bool IsWithinBounds(int x, int z) => x >= 0 && x < _width && z >= 0 && z < _height;

        public Subject<Unit> OnChunkSetupFinished = new Subject<Unit>();


        private void Update()
        {
            if (_shouldEnqueue)
            {
                _shouldEnqueue = false;
                Observable.TimerFrame(1, FrameCountType.FixedUpdate).Subscribe(_ => { EnqueueSpawnRequests(); }).AddTo(this);
            }
        }

        private void OnDisable()
        {
            _gameState.StoreChunkData(ChunkPosition, GetChunkData());
        }

        public void Init(int chunkCoordX, int chunkCoordZ, int chunkSeed)
        {
            InitializeFirstTime(chunkCoordX, chunkCoordZ, chunkSeed);
        }

        public void Reactivate()
        {
            var chunkData = _gameState.GetChunkData(ChunkPosition);
            RestoreChunkData(chunkData);
        }

        public void SetTileStateInCircle(int posX, int posY, float radius, TileState to)
        {
            //Using r + 0.5 creates nicer circles
            radius += 0.5f;

            int top = Mathf.RoundToInt(posY - radius);
            int bottom = Mathf.RoundToInt(posY + radius);
            int left = Mathf.RoundToInt(posX - radius);
            int right = Mathf.RoundToInt(posX + radius);

            for (int y = top; y <= bottom; y++)
            {
                for (int x = left; x <= right; x++)
                {
                    if (IsInsideCircle(posX, posY, x, y, radius))
                    {
                        ChangeTileState(x, y, to);
                    }
                }
            }
        }

        public void ClearCircle(Vector3 position, float radius)
        {
            var mapPos = ChunkPosToMap(transform.InverseTransformPoint(position));
            SetTileStateInCircle((int)mapPos.x, (int)mapPos.z, radius, TileState.Clean);
        }

        public ChunkData GetChunkData() => _elementSpawner.ConstructChunkData();

        private void InitializeFirstTime(int chunkCoordX, int chunkCoordZ, int chunkSeed)
        {
            _gameState = ServiceLocator.Resolve<GameState>();
            _settings = ServiceLocator.Resolve<Settings>();
            _mapSettings = _settings.MapSettings;
            _implementationProvider = ServiceLocator.Resolve<ImplementationProvider>();

            _chunkSeed = chunkSeed;
            _spawnRandomizer = new WeightedRandomizer(chunkSeed);
            _random = new System.Random(_chunkSeed);

            float[] chances = (from spawnData
                    in _settings.SpawnParameters._spawnData
                select spawnData.Chance).ToArray();

            _spawnRandomizer.Init(chances);

            _groundLayerMask = LayerMask.GetMask("Ground");

            SetupPositionAndSize(chunkCoordX, chunkCoordZ);

            _elementSpawner = GetComponent<ChunkElementSpawner>();
            _elementSpawner.Init(this, _implementationProvider, _settings);

            if (_settings.MapSettings.GenerateCanopy)
            {
                var canopyShadow = Instantiate(_canopyShadowPrefab, transform);
                canopyShadow.transform.localPosition = new Vector3(-4.5f, 140f, 4.5f);
            }

            CreateTileArrayAsync();
        }

        private async void GenerateTreeAndTerrainDataAsync(MapGenerator.MapGenerator generator)
        {
            await Task.Run(() => GenerateTreeAndTerrainData(generator));
            OnMapDataGenerationComplete();
        }

        private async void CreateTileArrayAsync()
        {
            await Task.Run(CreateTileArray);
            var generator = ServiceLocator.Resolve<MapGenerator.MapGenerator>();
            OnChunkSetupFinished.Subscribe(_ => { _gameState.StoreChunkData(ChunkPosition, GetChunkData()); }).AddTo(this);
            GenerateTreeAndTerrainDataAsync(generator);
        }

        private void SetupPositionAndSize(int chunkCoordX, int chunkCoordZ)
        {
            _chunkCoordX = chunkCoordX;
            _chunkCoordZ = chunkCoordZ;
            _chunkPosition = new Vector2(_chunkCoordX, _chunkCoordZ);

            _width = _settings.MapSettings.Width + 2;
            _height = _settings.MapSettings.Height + 2;
            _tileSize = _settings.MapSettings.TileSize;

            _isEdgeChunk = Mathf.Abs(_chunkCoordX) == _settings.MapSettings.ChunksPerSide / 2
                || Mathf.Abs(_chunkCoordZ) == _settings.MapSettings.ChunksPerSide / 2;
        }

        private void EnqueueSpawnRequests()
        {
            var propValues = Enum.GetValues(typeof(EProp));

            for (int i = 0; i < _height - 3; i++)
            {
                for (int j = 0; j < _width - 3; j++)
                {
                    var tile = GetTile(j, i);
                    var worldPos = tile.WorldPos;

                    if (_elementSpawner.IsInsideNestRadius(worldPos))
                    {
                        continue;
                    }

                    var hitPos = tile.SurfacePositionInChunk;

                    if (hitPos == Vector3.zero)
                    {
                        continue;
                    }

                    if (_treeAndPropsData[j, i] <= 0.4f && !IsNearTree(j, i))
                    {
                        tile.Affect(TileState.Tree);

                        _elementSpawner.EnqueueSpawnRequest(new ChunkElementSpawner.SpawnRequest()
                        {
                            SpawnType = SpawnType.Tree,
                            Position = hitPos + Vector3.down,
                            Rotation = Quaternion.AngleAxis(_random.Next(0, 360), Vector3.up),
                            Scale = Vector3.one,
                            SpawnVariantIndex = _random.Next(0, _implementationProvider.Trees.TreePrefabs.Length)
                        });
                    }
                    else if (_treeAndPropsData[j, i] >= 0.73f && tile.Elevation != Elevation.Water)
                    {
                        _elementSpawner.EnqueueSpawnRequest(new ChunkElementSpawner.SpawnRequest()
                        {
                            SpawnType = SpawnType.Prop,
                            Position = hitPos,
                            Rotation = Quaternion.LookRotation(
                                    Vector3.forward,
                                    tile.SurfaceNormal != Vector3.zero ? tile.SurfaceNormal : Vector3.up)
                                * Quaternion.AngleAxis(_random.Next(0, 360), Vector3.up),
                            Scale = Vector3.one * PropSizeVariants[_random.Next(0, PropSizeVariants.Length)],
                            SpawnVariantIndex = _random.Next(1, propValues.Length)
                        });
                    }
                }
            }

            Observable.TimerFrame(1, FrameCountType.FixedUpdate).Subscribe(_ => EnqueueItemSpawns()).AddTo(this);
        }

        private bool IsNearTree(int x, int z)
        {
            for (int i = z - 5; i <= z + 5; i++)
            {
                for (int j = x - 5; j <= x + 5; j++)
                {
                    var tile = GetTile(j, i);

                    if (tile != null && tile.State == TileState.Tree)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private void GenerateTreeAndTerrainData(MapGenerator.MapGenerator mapGenerator)
        {
            float width = _mapSettings.Width;
            var mapCenter = new Vector2(-(width - 1) * _chunkCoordX, -(width - 1) * _chunkCoordZ);

            var terrainMapData = _mapSettings.TerrainMapData;
            var treeMapData = _mapSettings.TreeMapData;
            terrainMapData.Center = mapCenter;
            treeMapData.Center = mapCenter;

            terrainMapData.Width += 2;
            terrainMapData.Height += 2;
            treeMapData.Width += 2;
            treeMapData.Height += 2;

            _terrainData = mapGenerator.GenerateMap(terrainMapData);
            _treeAndPropsData = mapGenerator.GenerateMap(treeMapData);
        }

        private void OnMapDataGenerationComplete()
        {
            for (var i = 0; i < _height; i++)
            {
                for (var j = 0; j < _width; j++)
                {
                    var tile = GetTile(j, i);
                    tile.Height = GetTerrainHeight(tile);
                    _elementSpawner.GenerateSpiderNestPosition(tile);
                }
            }

            GenerateMeshAndTextureAsync();
        }

        private async void GenerateMeshAndTextureAsync()
        {
            await Task.Run(() => { _chunkMesh.GenerateMesh(Tiles); });

            _chunkMesh.AssignMesh();

            Vector2[] uvs = null;
            var uvLength = _chunkMesh.Mesh.uv.Length;
            var norms = _chunkMesh.Mesh.normals;
            var tris = _chunkMesh.Mesh.triangles;
            var verts = _chunkMesh.Mesh.vertices;

            await Task.Run(() => { uvs = _chunkMesh.GenerateTextureAssignNormals(Tiles, uvLength, norms, tris, verts); });

            _chunkMesh.AssignUvs(uvs);

            Observable.Timer(TimeSpan.FromSeconds(1.0)).Subscribe(_ =>
            {
                SetTileSurfacePoints();

                if (_isEdgeChunk)
                {
                    OnChunkSetupFinished.OnNext(Unit.Default);
                }
                else
                {
                    var chunkData = _gameState.GetChunkData(ChunkPosition);

                    if (chunkData != null)
                    {
                        Reactivate();
                    }
                    else
                    {
                        _shouldEnqueue = true;
                    }
                }
            }).AddTo(this);
        }

        private void SetTileSurfacePoints()
        {
            var toChunkMatrix = transform.worldToLocalMatrix * _chunkMesh.transform.localToWorldMatrix;

            for (var i = 0; i < _height; i++)
            {
                for (var j = 0; j < _width; j++)
                {
                    var tile = GetTile(j, i);
                    tile.SurfacePositionInChunk = toChunkMatrix.MultiplyPoint3x4(tile.SurfacePositionInChunk);
                }
            }
        }

        private float GetTerrainHeight(Tile tile) =>
            _terrainData[tile.X, tile.Y] * _edgeHeightFactor.Evaluate(tile.DistanceRatio);

        private void CreateTileArray()
        {
            _tiles = new Tile[_height][];

            for (int i = 0; i < _tiles.Length; i++)
            {
                _tiles[i] = new Tile[_width];
            }

            for (var i = 0; i < _height; i++)
            {
                for (var j = 0; j < _width; j++)
                {
                    _tiles[i][j] = new Tile(j, i, TileState.Clean, OnTileStateChanged, MapPosToWorld(j, i));
                }
            }
        }

        private void OnTileStateChanged(Tile arg1, TileState arg2) { }

        private void RestoreChunkData(ChunkData data)
        {
            TileState[][] loadedTileStates = new TileState[_height][];

            for (int index = 0; index < _height; index++)
            {
                loadedTileStates[index] = new TileState[_width];
            }

            for (int x = 0; x < _height; x++)
            {
                for (int y = 0; y < _width; y++)
                {
                    loadedTileStates[x][y] = data.TileStates[x * _width + y];
                }
            }

            for (var i = 0; i < _height; i++)
            {
                for (var j = 0; j < _width; j++)
                {
                    _tiles[i][j].Affect(loadedTileStates[i][j]);
                }
            }

            foreach (var treeData in data.Trees)
            {
                _elementSpawner.EnqueueSpawnRequest(new ChunkElementSpawner.SpawnRequest()
                {
                    SpawnType = SpawnType.Tree,
                    Position = treeData.Position,
                    Rotation = Quaternion.identity,
                    SpawnVariantIndex = treeData.VariantIndex
                });
            }

            foreach (var propData in data.Props)
            {
                _elementSpawner.EnqueueSpawnRequest(new ChunkElementSpawner.SpawnRequest()
                {
                    SpawnType = SpawnType.Prop,
                    Position = propData.Position,
                    Rotation = new Quaternion(
                        propData.Rotation.X,
                        propData.Rotation.Y,
                        propData.Rotation.Z,
                        propData.Rotation.W),
                    Scale = propData.Scale,
                    SpawnVariantIndex = (int)propData.Type
                });
            }

            foreach (var itemPickupData in data.Items)
            {
                _elementSpawner.EnqueueSpawnRequest(new ChunkElementSpawner.SpawnRequest()
                {
                    SpawnType = SpawnType.Item,
                    Position = itemPickupData.Position,
                    Rotation = Quaternion.identity,
                    SpawnVariantIndex = itemPickupData.VariantIndex
                });
            }

            // foreach (var plantData in data.Plants)
            // {
            //     var plantGo = Instantiate(implementationProvider.GetPlaceableImplementation(plantData.Type).PlaceablePrefab, transform);
            //
            //     plantGo.transform.rotation = new Quaternion(
            //         plantData.Rotation.X,
            //         plantData.Rotation.Y,
            //         plantData.Rotation.Z,
            //         plantData.Rotation.W);
            //
            //     plantGo.transform.localPosition = plantData.Position;
            //
            //
            //     var plant = plantGo.GetComponent<Plant>();
            //     plant.FullGrowthTime = plantData.FullGrowthTime;
            //     plant.Type = plantData.Type;
            //     plant.BalanceChange = plantData.BalanceChange;
            //     plant.LifeTime = plantData.LifeTime;
            // }
            //
            //
            // foreach (var itemStashData in data.ItemStashes)
            // {
            //     var stashGo = Instantiate(implementationProvider.GetPlaceableImplementation(EPlaceable.Chest).PlaceablePrefab, transform);
            //     stashGo.transform.localPosition = itemStashData.Position;
            //     stashGo.GetComponent<ItemStash>().Contents = itemStashData.Contents;
            // }
        }


        private int ChunkXToMap(float coord) => Mathf.FloorToInt(coord / _tileSize + _width / 2f);
        private int ChunkZToMap(float coord) => Mathf.FloorToInt(-coord / _tileSize + _height / 2f);
        private Vector3 ChunkPosToMap(Vector3 pos) => new Vector3(ChunkXToMap(pos.x), pos.y, ChunkZToMap(pos.z));
        private float MapXToChunk(int coord) => ((coord - _width / 2f) * _tileSize + _tileSize / 2f);
        private float MapZToChunk(int coord) => ((-coord + _height / 2f) * _tileSize - _tileSize / 2f);
        private Vector3 MapPosToChunk(int x, int z) => new Vector3(MapXToChunk(x), 0f, MapZToChunk(z));

        private bool IsInsideCircle(float centerX, float centerY, float tileX, float tileY, float radius)
        {
            float dx = centerX - tileX;
            float dy = centerY - tileY;
            float distanceSquared = dx * dx + dy * dy;
            return distanceSquared < radius * radius;
        }

        private void GrowCorruptionOnTile(int x, int z)
        {
            var tile = GetTile(x, z);
            tile?.Affect(TileState.Corrupted);
        }

        private void EnqueueItemSpawns()
        {
            for (int i = 0; i < _height - 3; i++)
            {
                for (int j = 0; j < _width - 3; j++)
                {
                    var index = _spawnRandomizer.GetWeightedIndex();

                    if (index <= -1)
                    {
                        continue;
                    }

                    var tile = GetTile(j, i);

                    var hitPos = tile.SurfacePositionInChunk;

                    if (hitPos == Vector3.zero || tile.DistanceRatio > 0.7f)
                    {
                        continue;
                    }

                    SpawnData spawnData = _settings.SpawnParameters._spawnData[index];

                    if ((tile.State | spawnData.TileState) == spawnData.TileState)
                    {
                        _elementSpawner.EnqueueSpawnRequest(new ChunkElementSpawner.SpawnRequest()
                        {
                            SpawnType = SpawnType.Item,
                            Position = hitPos + Vector3.up * 1.8f,
                            Rotation = Quaternion.identity,
                            Scale = Vector3.one,
                            SpawnVariantIndex = index
                        });
                    }
                }
            }
        }

        private void ChangeTileState(int x, int z, TileState to)
        {
            var tile = GetTile(x, z);
            tile?.Affect(to);
        }
    }
}
