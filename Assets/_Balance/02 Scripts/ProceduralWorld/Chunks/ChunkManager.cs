﻿using System;
using System.Collections.Generic;
using System.Linq;
using _Balance._02_Scripts.BusinessLogic;
using _Balance._02_Scripts.Player;
using _Balance._02_Scripts.Utilities;
using Lean.Pool;
using UniRx;
using UnityEngine;

namespace _Balance._02_Scripts.ProceduralWorld.Chunks
{
    public class ChunkManager : MonoBehaviour
    {
        [SerializeField] private bool _dynamicChunkGeneration;
        [SerializeField] private bool _finiteMapSize;
        [SerializeField] private bool _allowPoolRecycle;

        [SerializeField] private PlayerController _player;
        [SerializeField] private Transform _chunksParent;
        [SerializeField] private GameObject _chunkPrefab;
        [SerializeField] private AnimationCurve _corruptionGrowthCurve;

        public readonly ReactiveCollection<Vector2> ChunksBeingCreated = new ReactiveCollection<Vector2>();
        
        private readonly Dictionary<Vector2, GameObject> _chunks = new Dictionary<Vector2, GameObject>();
        private readonly HashSet<Vector2> _activeChunkPositions = new HashSet<Vector2>();
        private readonly List<MapChunk> _activeChunks = new List<MapChunk>();
       
        private int _playerXChunkCoord;
        private int _playerZChunkCoord;
        private Vector3 _playerPos;
        
        private float _actualChunkSize;
        private int _mapExtent;
        
        private IDisposable _corruptionTimer;
        private int _startChunksInitialized;
        private int _seed;
        
        private MiniMap.MiniMap _miniMap;
        private MapSettings _mapSettings;
        private GameState _gameState;

        public Subject<Unit> OnAllStartChunksInitialized = new Subject<Unit>();
        public MapChunk CurrentMapChunk { get; private set; }
        public int PlayerXChunkCoord => _playerXChunkCoord;
        public int PlayerZChunkCoord => _playerZChunkCoord;
        public Vector3 PlayerPos => _playerPos;

        public List<MapChunk> ActiveChunks
        {
            get
            {
                _activeChunks.Clear();

                foreach (var chunkPos in _activeChunkPositions)
                {
                    _activeChunks.Add(_chunks[chunkPos].GetComponent<MapChunk>());
                }

                return _activeChunks;
            }
        }

        private void Start()
        {
            _gameState = ServiceLocator.Resolve<GameState>();
            _miniMap = ServiceLocator.Resolve<MiniMap.MiniMap>();
            _mapSettings = ServiceLocator.Resolve<Settings>().MapSettings;

            InitializeGenerators();

            _mapExtent = _mapSettings.ChunksPerSide / 2;
            _actualChunkSize = _mapSettings.ActualChunkSize;

            foreach (var pool in GetComponents<LeanGameObjectPool>())
            {
                pool.Recycle = _allowPoolRecycle;
            }

            if (_dynamicChunkGeneration)
            {
                GenerateStartChunks(_gameState.PlayerChunkCoords);
            }
            else
            {
                GenerateAllChunks();
            }

            _gameState.Balance.Subscribe(ChangeCorruptionGrowthInterval);
            UpdateChunks((int)_gameState.PlayerChunkCoords.x, (int)_gameState.PlayerChunkCoords.y);
        }

        private void Update()
        {
            var playerPos = _player.transform.position;
            var playerXChunkCoord = Mathf.RoundToInt(playerPos.x / _actualChunkSize);
            var playerZChunkCoord = Mathf.RoundToInt(playerPos.z / _actualChunkSize);
            _playerPos = new Vector3(playerPos.x, 0f, playerPos.z);

            if (_playerXChunkCoord != playerXChunkCoord || _playerZChunkCoord != playerZChunkCoord)
            {
                UpdateChunks(playerXChunkCoord, playerZChunkCoord);
            }
        }

        public void ChangeTileStateInCircle(float posX, float posZ, float radius, TileState to)
        {
            foreach (var chunk in ActiveChunks)
            {
                chunk.SetTileStateInCircle(chunk.WorldXToMap(posX), chunk.WorldZToMap(posZ), radius, to);
            }
        }

        private void InitializeGenerators()
        {
            _seed = _gameState.RandomSeed;
            _mapSettings.TerrainMapData.Seed = _seed;
            _mapSettings.TreeMapData.Seed = _seed;
        }

        private void SetCurrentChunkMap(float x, float y)
        {
            if (_chunks.TryGetValue(new Vector2(x, y), out var chunk))
            {
                CurrentMapChunk = chunk.GetComponent<MapChunk>();
            }
            else
            {
                CurrentMapChunk = null;
            }
        }

        private void GenerateStartChunks(Vector2 playerChunkCoords)
        {
            for (var i = playerChunkCoords.y - 1; i <= playerChunkCoords.y + 1; i++)
            {
                for (var j = playerChunkCoords.x - 1; j <= playerChunkCoords.x + 1; j++)
                {
                    if (i <= _mapExtent && i >= -_mapExtent && j <= _mapExtent && j >= -_mapExtent)
                    {
                        CreateChunk((int) j, (int) i);
                        _activeChunkPositions.Add(new Vector2(j, i));
                    }
                }
            }

            foreach (var startChunk in _chunks.Values)
            {
                SubscribeToStartChunkFinishedEvent(startChunk);
            }
        }

        private void SubscribeToStartChunkFinishedEvent(GameObject startChunk)
        {
            var mapChunk = startChunk.GetComponent<MapChunk>();

            mapChunk.OnChunkSetupFinished.Subscribe(_ =>
            {
                _miniMap.TryCreateMiniMapTile(mapChunk.ChunkPosition, mapChunk.Tiles);

                _startChunksInitialized++;

                if (_startChunksInitialized == _chunks.Count)
                {
                    OnAllStartChunksInitialized.OnNext(Unit.Default);
                }
            }).AddTo(this);
        }

        private void GenerateAllChunks()
        {
            var halfExtent = _mapSettings.ChunksPerSide / 2;

            for (var i = -halfExtent; i <= halfExtent; i++)
            {
                for (var j = -halfExtent; j <= halfExtent; j++)
                {
                    CreateChunk(j, i);
                }
            }

            for (var i = -1; i <= 1; i++)
            {
                for (var j = -1; j <= 1; j++)
                {
                    var pos = new Vector2(j, i);
                    _chunks[pos].SetActive(true);
                    _activeChunkPositions.Add(pos);
                }
            }
        }

        private void CreateChunk(int x, int z)
        {
            var chunk = Instantiate(
                _chunkPrefab,
                new Vector3(x * _actualChunkSize, 0f, z * _actualChunkSize),
                Quaternion.identity);
            
            chunk.transform.parent = _chunksParent;
            chunk.name = $"Chunk {x} {z}";
            _chunks[new Vector2(x, z)] = chunk;

            var chunkMap = chunk.GetComponent<MapChunk>();
            chunkMap.Init(x, z, _seed + GetChunkHash(x, z));

            ChunksBeingCreated.Add(chunkMap.ChunkPosition);

            chunkMap.OnChunkSetupFinished.Subscribe(_ =>
            {
                _miniMap.TryCreateMiniMapTile(chunkMap.ChunkPosition, chunkMap.Tiles);
                ChunksBeingCreated.Remove(chunkMap.ChunkPosition);
            }).AddTo(this);
        }

        private int GetChunkHash(int x, int z) => (z * (_mapExtent - -_mapExtent)) + x;

        private void UpdateChunks(int currentPlayerXCoord, int currentPlayerZCoord)
        {
            var newChunkPositions = GetChunkPositions(currentPlayerXCoord, currentPlayerZCoord);

            foreach (var pos in newChunkPositions)
            {
                if (!_chunks.ContainsKey(pos))
                {
                    CreateChunk((int) pos.x, (int) pos.y);
                }
            }

            _playerXChunkCoord = currentPlayerXCoord;
            _playerZChunkCoord = currentPlayerZCoord;

            var chunksToDeactivate = _activeChunkPositions
                .Except(newChunkPositions)
                .Where(c => Mathf.Max(Mathf.Abs(c.x - _playerXChunkCoord), Mathf.Abs(c.y - _playerZChunkCoord)) >= 3)
                .ToList();

            var chunksToActivate = newChunkPositions.Except(_activeChunkPositions).ToList();

            _activeChunkPositions.UnionWith(newChunkPositions);
            _activeChunkPositions.ExceptWith(chunksToDeactivate);

            foreach (var pos in chunksToDeactivate)
            {
                if (_chunks.ContainsKey(pos))
                {
                    _chunks[pos].SetActive(false);
                }
            }

            foreach (var pos in chunksToActivate)
            {
                if (_chunks.ContainsKey(pos) && !_chunks[pos].gameObject.activeInHierarchy)
                {
                    _chunks[pos].SetActive(true);
                    _chunks[pos].GetComponent<MapChunk>().Reactivate();
                }
            }

            SetCurrentChunkMap(_playerXChunkCoord, _playerZChunkCoord);
        }

        private void ChangeCorruptionGrowthInterval(float balance)
        {
            _corruptionTimer?.Dispose();

            if (balance <= 0f)
            {
                // var normalizedBalance = (balance + 100f) / 100f;
                // var interval = _corruptionGrowthCurve.Evaluate(normalizedBalance);
                //
                // _corruptionTimer = Observable.Interval(TimeSpan.FromSeconds(interval)).Subscribe(_ =>
                // {
                // 	foreach (var pos in _activeChunkPositions)
                // 	{
                // 		_chunks[pos].GetComponent<MapChunk>().SpreadCorruption();
                // 	}
                // }).AddTo(this);
            }
        }

        private HashSet<Vector2> GetChunkPositions(int x, int z)
        {
            HashSet<Vector2> positions = new HashSet<Vector2>();

            for (var i = x - 1; i <= x + 1; i++)
            {
                for (var j = z - 1; j <= z + 1; j++)
                {
                    if (_finiteMapSize)
                    {
                        if (i >= -_mapExtent && i <= _mapExtent && j >= -_mapExtent && j <= _mapExtent)
                        {
                            positions.Add(new Vector2(i, j));
                        }
                    }
                    else
                    {
                        positions.Add(new Vector2(i, j));
                    }
                }
            }

            return positions;
        }
    }
}