﻿using System;
using System.Collections.Generic;
using System.Linq;
using _Balance._02_Scripts.BusinessLogic;
using _Balance._02_Scripts.Farming;
using _Balance._02_Scripts.Inventory.Examples;
using _Balance._02_Scripts.Inventory.Items;
using _Balance._02_Scripts.ProceduralWorld.Props;
using Lean.Pool;
using UniRx;
using UnityEngine;

namespace _Balance._02_Scripts.ProceduralWorld.Chunks
{
    public class ChunkElementSpawner : MonoBehaviour
    {
        [SerializeField] private GameObject _spiderNestPrefab;

        private readonly Queue<SpawnRequest> _spawnRequests = new Queue<SpawnRequest>();

        private MapChunk _chunk;
        private Transform _treesParent;
        private Transform _propsParent;
        private Transform _itemsParent;
        private Transform _nestsParent;
        
        private ImplementationProvider _implementationProvider;
        private Settings _settings;
        
        private float _spiderNestSqRadius;

        private readonly List<Vector3> _nestPositions = new List<Vector3>();

        public void Init(
            MapChunk chunk,
            ImplementationProvider implementationProvider,
            Settings settings)
        {
            _chunk = chunk;
            _implementationProvider = implementationProvider;
            _settings = settings;
            _spiderNestSqRadius = _settings.SpiderSettings.NestClearRadius * _settings.SpiderSettings.NestClearRadius;

            SetupElementParents();
        }

        private void Update()
        {
            if (_spawnRequests.Count > 0)
            {
                HandleRemainingSpawnRequests();
            }
        }

        private void OnDisable()
        {
            DespawnElements();
        }

        public void EnqueueSpawnRequest(SpawnRequest request) => _spawnRequests.Enqueue(request);

        public bool IsInsideNestRadius(Vector3 worldPos)
        {
            for (int i = 0; i < _nestPositions.Count; i++)
            {
                var nestPos = _nestPositions[i];

                if ((worldPos.x - nestPos.x) * (worldPos.x - nestPos.x)
                    + (worldPos.y - nestPos.y) * (worldPos.y - nestPos.y)
                    + (worldPos.z - nestPos.z) * (worldPos.z - nestPos.z) <= _spiderNestSqRadius)
                {
                    return true;
                }
            }

            return false;
        }

        public void GenerateSpiderNestPosition(Tile tile)
        {
            if (_chunk.IsEdgeChunk || tile.DistanceRatio > 0.7f)
            {
                return;
            }

            var x = tile.X;
            var z = tile.Y;

            if (z > 20 && z < _chunk.Height - 20
                       && x > 20 && x < _chunk.Width - 20
                       && _chunk.Random.NextDouble() <= 0.01)
            {
                var worldPos = tile.WorldPos;
                var pos = new Vector3(worldPos.x, _chunk.GetTile(x, z).ScaledHeight, worldPos.z);
                var tooClose = false;

                foreach (var otherPos in _nestPositions)
                {
                    if (Vector3.ProjectOnPlane(otherPos - pos, Vector3.up).sqrMagnitude
                        <= Mathf.Pow(_settings.SpiderSettings.MinDistanceBetweenNests, 2))
                    {
                        tooClose = true;
                        break;
                    }
                }

                if (!tooClose)
                {
                    _nestPositions.Add(pos);
                }
            }
        }

        public ChunkData ConstructChunkData()
        {
            var itemCount = _itemsParent.childCount;
            ItemPickupData[] items = new ItemPickupData[itemCount];

            for (int i = 0; i < itemCount; i++)
            {
                var item = _settings.SpawnParameters._spawnData.First(e =>
                    e.Type == _itemsParent.GetChild(i).GetComponent<ItemPickup>().Item.Type);

                items[i] = new ItemPickupData(
                    _itemsParent.GetChild(i).localPosition,
                    Array.IndexOf(_settings.SpawnParameters._spawnData, item));
            }

            var plantItems = GetComponentsInChildren<Plant>();
            PlantData[] plants = new PlantData[plantItems.Length];

            for (int i = 0; i < plantItems.Length; i++)
            {
                plants[i] = new PlantData(
                    plantItems[i].transform.localPosition,
                    plantItems[i].Type,
                    plantItems[i].LifeTime,
                    new Rotation(
                        plantItems[i].transform.rotation.x,
                        plantItems[i].transform.rotation.y,
                        plantItems[i].transform.rotation.z,
                        plantItems[i].transform.rotation.w),
                    plantItems[i].FullGrowthTime,
                    plantItems[i].BalanceChange);
            }

            var itemStashes = GetComponentsInChildren<ItemStash>();
            ItemStashData[] stashes = new ItemStashData[itemStashes.Length];

            for (int i = 0; i < itemStashes.Length; i++)
            {
                stashes[i] = new ItemStashData(
                    itemStashes[i].transform.localPosition,
                    itemStashes[i].Contents);
            }

            var propCount = _propsParent.childCount;
            PropData[] props = new PropData[propCount];

            for (int i = 0; i < propCount; i++)
            {
                var prop = _propsParent.GetChild(i).GetComponent<Prop>();
                var propTrans = prop.transform;
                var rotation = propTrans.rotation;

                props[i] = new PropData(
                    propTrans.localPosition,
                    new Rotation(
                        rotation.x,
                        rotation.y,
                        rotation.z,
                        rotation.w),
                    propTrans.localScale,
                    prop.Type);
            }

            var treeCount = _treesParent.childCount;
            TreeData[] trees = new TreeData[treeCount];

            for (int i = 0; i < treeCount; i++)
            {
                var tree = _treesParent.GetChild(i);
                var rotation = tree.transform.rotation;

                trees[i] = new TreeData(
                    tree.localPosition,
                    new Rotation(
                        rotation.x,
                        rotation.y,
                        rotation.z,
                        rotation.w),
                    tree.name.Contains("Gray") ? 1 : 0);
            }

            return new ChunkData()
            {
                TileStates = _chunk.Tiles.SelectMany(t => t).Select(e => e.State).ToArray(),
                Items = items,
                Plants = plants,
                ItemStashes = stashes,
                Props = props,
                Trees = trees
            };
        }

        public void DespawnElements()
        {
            int childCount = _itemsParent.childCount;

            for (int i = childCount - 1; i >= 0; i--)
            {
                LeanPool.Despawn(_itemsParent.GetChild(i).gameObject);
            }

            childCount = _propsParent.childCount;

            for (int i = childCount - 1; i >= 0; i--)
            {
                LeanPool.Despawn(_propsParent.GetChild(i).gameObject);
            }

            childCount = _treesParent.childCount;

            for (int i = childCount - 1; i >= 0; i--)
            {
                LeanPool.Despawn(_treesParent.GetChild(i).gameObject);
            }
        }

        private void SetupElementParents()
        {
            _itemsParent = new GameObject("Items").transform;
            _propsParent = new GameObject("Props").transform;
            _treesParent = new GameObject("Trees").transform;
            _nestsParent = new GameObject("SpiderNests").transform;

            var ownTransform = transform;

            _itemsParent.parent = ownTransform;
            _propsParent.parent = ownTransform;
            _treesParent.parent = ownTransform;
            _nestsParent.parent = ownTransform;
            
            _itemsParent.localPosition = Vector3.zero;
            _propsParent.localPosition = Vector3.zero;
            _treesParent.localPosition = Vector3.zero;
            _nestsParent.localPosition = Vector3.zero;
        }

        private void ProcessNextSpawnRequest()
        {
            var request = _spawnRequests.Dequeue();

            switch (request.SpawnType)
            {
                case SpawnType.None:
                    break;
                case SpawnType.Tree:
                    var treeGo = LeanPool.Spawn(_implementationProvider.Trees.TreePrefabs[request.SpawnVariantIndex], _treesParent);
                    treeGo.transform.localPosition = request.Position;
                    treeGo.transform.localRotation = request.Rotation;
                    break;
                case SpawnType.Prop:
                    var propType = (EProp) request.SpawnVariantIndex;
                    var propGo = LeanPool.Spawn(_implementationProvider.GetPropImplementation(propType).PropPrefab, _propsParent);
                    propGo.transform.localPosition = request.Position;
                    propGo.transform.localRotation = request.Rotation;
                    propGo.transform.localScale = request.Scale;
                    propGo.GetComponent<Prop>().Type = propType;
                    break;
                case SpawnType.Item:
                    var spawnData = _settings.SpawnParameters._spawnData[request.SpawnVariantIndex];
                    var itemGo = LeanPool.Spawn(spawnData.ItemPrefab, _itemsParent);
                    itemGo.transform.localPosition = request.Position;
                    itemGo.transform.localRotation = request.Rotation;
                    break;
            }
        }

        private void HandleRemainingSpawnRequests()
        {
            var activations = 0;

            while (activations < _settings.MapSettings.ObjectsActivatedPerFrame && _spawnRequests.Count > 0)
            {
                ProcessNextSpawnRequest();
                activations++;
            }

            if (_spawnRequests.Count == 0)
            {
                if (!(_chunk.ChunkCoordX == 0 && _chunk.ChunkCoordX == 0))
                {
                    CreateNests();
                }

                _chunk.OnChunkSetupFinished.OnNext(Unit.Default);
            }
        }

        private void CreateNests()
        {
            foreach (var pos in _nestPositions)
            {
                Instantiate(_spiderNestPrefab, pos, Quaternion.identity, _nestsParent);
                _chunk.ClearCircle(pos, _settings.SpiderSettings.NestClearRadius);
            }
        }

        public struct SpawnRequest
        {
            public SpawnType SpawnType;
            public Vector3 Position;
            public Quaternion Rotation;
            public Vector3 Scale;
            public int SpawnVariantIndex;
        }
    }


    public enum SpawnType
    {
        None = 0,
        Tree = 1,
        Prop = 2,
        Item = 3
    }
}