﻿using _Balance._02_Scripts.Inventory.FileIO;
using _Balance._02_Scripts.Inventory.Items;
using _Balance._02_Scripts.ProceduralWorld.Chunks;
using UnityEngine;
using ZeroFormatter;

[ZeroFormattable]
public struct Rotation
{
    [Index(0)] public float X;
    [Index(1)] public float Y;
    [Index(2)] public float Z;
    [Index(3)] public float W;

    public Rotation(float x, float y, float z, float w)
    {
        X = x;
        Y = y;
        Z = z;
        W = w;
    }
}

[ZeroFormattable]
public struct ItemPickupData
{
    [Index(0)] public Vector3 Position;
    [Index(1)] public int VariantIndex;

    public ItemPickupData(Vector3 position, int variantIndex)
    {
        Position = position;
        VariantIndex = variantIndex;
    }
}

[ZeroFormattable]
public struct PlantData
{
    [Index(0)] public Vector3 Position;
    [Index(1)] public EPlaceable Type;
    [Index(2)] public float LifeTime;
    [Index(3)] public Rotation Rotation;
    [Index(4)] public float FullGrowthTime;
    [Index(5)] public float BalanceChange;

    public PlantData(
        Vector3 position,
        EPlaceable type,
        float lifeTime,
        Rotation rotation,
        float fullGrowthTime,
        float balanceChange)
    {
        Position = position;
        Type = type;
        LifeTime = lifeTime;
        Rotation = rotation;
        FullGrowthTime = fullGrowthTime;
        BalanceChange = balanceChange;
    }
}

[ZeroFormattable]
public struct PropData
{
    [Index(0)] public Vector3 Position;
    [Index(1)] public Rotation Rotation;
    [Index(2)] public Vector3 Scale;
    [Index(3)] public EProp Type;

    public PropData(Vector3 position, Rotation rotation, Vector3 scale, EProp type)
    {
        Position = position;
        Rotation = rotation;
        Scale = scale;
        Type = type;
    }
}

[ZeroFormattable]
public struct TreeData
{
    [Index(0)] public Vector3 Position;
    [Index(1)] public Rotation Rotation;
    [Index(2)] public int VariantIndex;

    public TreeData(Vector3 position, Rotation rotation, int variantIndex)
    {
        Position = position;
        Rotation = rotation;
        VariantIndex = variantIndex;
    }
}

[ZeroFormattable]
public struct ItemStashData
{
    [Index(0)] public Vector3 Position;
    [Index(1)] public ItemContainerSaveData Contents;

    public ItemStashData(Vector3 position, ItemContainerSaveData contents)
    {
        Position = position;
        Contents = contents;
    }
}

[ZeroFormattable]
public class ChunkData
{
    [Index(0)] public virtual TileState[] TileStates { get; set; }
    [Index(1)] public virtual ItemPickupData[] Items { get; set; }
    [Index(2)] public virtual PlantData[] Plants { get; set; }
    [Index(3)] public virtual ItemStashData[] ItemStashes { get; set; }
    [Index(4)] public virtual PropData[] Props { get; set; }
    [Index(5)] public virtual TreeData[] Trees { get; set; }
}