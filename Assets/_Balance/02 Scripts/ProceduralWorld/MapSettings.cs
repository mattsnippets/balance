﻿using _Balance._02_Scripts.ProceduralWorld.MapGenerator;
using _Balance._02_Scripts.Utilities.SerializableDictionaries;
using UnityEngine;

namespace _Balance._02_Scripts.ProceduralWorld
{
    [CreateAssetMenu(fileName = "MapSettings", menuName = "ScriptableObjects/MapSettings")]
    public class MapSettings : ScriptableObject
    {
        public int Width;
        public int Height;
        public float TileSize;
        public int AtlasSize;
        public int ChunksPerSide;
        public bool GenerateCanopy;
        public float TileDistanceRatioDivisor;

        [Space] public MapGenerationData TerrainMapData;

        [Space] public MapGenerationData TreeMapData;

        [Space] public int ObjectsActivatedPerFrame;
        public float HeightMultiplier;

        public float ActualChunkSize => (Width-1) * TileSize;

        public AnimationCurve HeightCurve;
    }
}