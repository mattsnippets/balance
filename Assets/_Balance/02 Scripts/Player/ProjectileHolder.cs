﻿using System.Collections.Generic;
using _Balance._02_Scripts.Entities;
using Lean.Pool;
using UnityEngine;

namespace _Balance._02_Scripts.Player
{
    public class ProjectileHolder : MonoBehaviour
    {
        [SerializeField] private Transform _followTarget;
        [SerializeField] private Vector3 _followOffset;
        [SerializeField] private GameObject _projectilePrefab;
        [SerializeField] private float _radius;
        [SerializeField] private float _rotationSpeed;

        public int AmmoCount => _projectiles.Count;

        private readonly List<Projectile> _projectiles = new List<Projectile>();

        private void Update()
        {
            transform.Rotate(transform.up, _rotationSpeed * Time.deltaTime, Space.Self);
        }

        private void LateUpdate()
        {
            transform.position = _followTarget.position + _followOffset;
        }
        
        public bool TryGetProjectile(out Projectile projectile)
        {
            if (_projectiles.Count == 0)
            {
                projectile = null;
                return false;
            }

            var chosenProjectile = _projectiles[0];
            _projectiles.RemoveAt(0);
            RepositionProjectiles();
            
            chosenProjectile.transform.parent = null;
            projectile = chosenProjectile;
            return true;
        }

        public void AddProjectile()
        {
            _projectiles.Add(LeanPool.Spawn(
                _projectilePrefab,
                transform.position,
                Quaternion.identity,
                transform).GetComponent<Projectile>());
            
            RepositionProjectiles();
        }

        private void RepositionProjectiles()
        {
            for (int i = 0; i < _projectiles.Count; i++)
            {
                var angle = i * Mathf.PI * 2f / _projectiles.Count;
                Vector3 pos = new Vector3(Mathf.Cos(angle) * _radius, 0f, Mathf.Sin(angle) * _radius);
                _projectiles[i].transform.localPosition = pos;
            }
        }
    }
}