﻿using UnityEngine;

namespace _Balance._02_Scripts.Player
{
    public class ShadowCaster : MonoBehaviour
    {
        [SerializeField] private float _rotationMaxDelta;

        private LayerMask _groundMask;
        private readonly RaycastHit[] _raycastHit = new RaycastHit[1];

        private void Start()
        {
            _groundMask = LayerMask.GetMask("Ground");
        }

        private void Update()
        {
            if (Physics.RaycastNonAlloc(transform.parent.position, Vector3.down, _raycastHit, 100f, _groundMask) > 0)
            {
                transform.position = _raycastHit[0].point + _raycastHit[0].normal * 0.2f;
                transform.rotation = Quaternion.RotateTowards(
                    transform.rotation,
                    Quaternion.LookRotation(_raycastHit[0].normal),
                    Time.deltaTime * _rotationMaxDelta);
            }
            else
            {
                transform.localPosition = new Vector3(0f, 0.2f, 0f);
            }
        }
    }
}