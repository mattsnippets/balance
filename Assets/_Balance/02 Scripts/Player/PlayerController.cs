﻿using System;
using _Balance._02_Scripts.BusinessLogic;
using _Balance._02_Scripts.BusinessLogic.Events;
using _Balance._02_Scripts.Entities;
using _Balance._02_Scripts.ProceduralWorld.Chunks;
using _Balance._02_Scripts.ProceduralWorld.MiniMap;
using UniRx;
using UnityEngine;

namespace _Balance._02_Scripts.Player
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private bool _corruptionDamageEnabled;
        [SerializeField] private float _corruptionDamage;
        [SerializeField] private GameObject _playerPositionMarker;

        [Header("Movement")] [SerializeField] private float _movingTurnSpeed = 360;
        [SerializeField] private float _stationaryTurnSpeed = 180;
        [SerializeField] private float _jumpPower = 12f;
        [Range(1f, 10f)] [SerializeField] private float _gravityMultiplier = 2f;
        [SerializeField] private float _runCycleLegOffset = 0.2f;
        [SerializeField] private float _moveSpeedMultiplier = 1f;
        [SerializeField] private float _animSpeedMultiplier = 1f;
        [SerializeField] private float _groundCheckDistance = 0.1f;

        [Header("Shooting")] [SerializeField] private ProjectileHolder _projectileHolder;
        [SerializeField] private float _autoAimRadius;
        [SerializeField] private int _maxAmmo;

        private Rigidbody _rigidbody;
        private CapsuleCollider _capsule;
        private Vector3 _heading;
        private Animator _animator;
        private Health _health;
        private Damageable _damageable;

        private float _origGroundCheckDistance;
        private Vector3 _groundNormal;
        private float _turnAmount;
        private float _forwardAmount;
        private bool _isGrounded = true;
        private Transform _cameraTransform;
        private float _climbHeightLimit = -1f;

        private PlayerHealthChangeEvent _playerHealthChanged = new PlayerHealthChangeEvent(100f);
        private BalanceChangeEvent _balanceChanged = new BalanceChangeEvent(0f);
        private PlayerDiedEvent _playerDied = new PlayerDiedEvent();

        private ChunkManager _chunkManager;
        private Settings _settings;
        private MiniMap _miniMap;

        private LayerMask _spidersMask;
        private Collider[] _nearbyEnemies = new Collider[10];

        private bool _isDead;

        private static readonly int Forward = Animator.StringToHash("Forward");
        private static readonly int Turn = Animator.StringToHash("Turn");
        private static readonly int Jump = Animator.StringToHash("Jump");

        public bool IsAmmoFull => _projectileHolder.AmmoCount >= _maxAmmo;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _capsule = GetComponent<CapsuleCollider>();
            _health = GetComponent<Health>();
            _damageable = GetComponent<Damageable>();
            _animator = GetComponent<Animator>();

            _rigidbody.constraints =
                RigidbodyConstraints.FreezeRotationX
                | RigidbodyConstraints.FreezeRotationY
                | RigidbodyConstraints.FreezeRotationZ;

            _origGroundCheckDistance = _groundCheckDistance;
        }

        private void Start()
        {
            _chunkManager = ServiceLocator.Resolve<ChunkManager>();
            _settings = ServiceLocator.Resolve<Settings>();
            _miniMap = ServiceLocator.Resolve<MiniMap>();

            _spidersMask = LayerMask.GetMask("Spiders");

            _cameraTransform = Camera.main.transform;

            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;

            SetupDamageHandling();

            _playerPositionMarker.transform.localScale = new Vector3(
                _settings.MapSettings.TileSize,
                0.3f,
                _settings.MapSettings.TileSize);

            _playerHealthChanged.CurrentHealth = _health.Current;
            _playerHealthChanged.Fire();
        }

        private void FixedUpdate()
        {
            Move(_heading);
        }

        private void Update()
        {
            if (_isDead)
            {
                return;
            }

            HandleMovement();

            var position = transform.position;

            if (_chunkManager.CurrentMapChunk)
            {
                var playerPosToMap = _chunkManager.CurrentMapChunk.WorldPosToMap(new Vector3(
                    position.x,
                    0f,
                    position.z));

                _playerPositionMarker.transform.position = new Vector3(
                    _chunkManager.CurrentMapChunk.MapXToWorld((int)playerPosToMap.x),
                    0f,
                    _chunkManager.CurrentMapChunk.MapZToWorld((int)playerPosToMap.z));
            }

            _miniMap.TrackPlayerPosition(position.x, position.z, transform.localEulerAngles.y);

            if (Input.GetMouseButtonDown(0) && !TimeScaleManager.IsPaused)
            {
                Fire();
            }
        }

        public void GetAmmo(int amount)
        {
            for (int i = 0; i < amount; i++)
            {
                _projectileHolder.AddProjectile();
            }
        }

        private void Fire()
        {
            if (!_projectileHolder.TryGetProjectile(out var projectile))
            {
                return;
            }

            Vector3 shootingDir;

            var nearbyEnemyCount = Physics.OverlapSphereNonAlloc(
                transform.position,
                _autoAimRadius,
                _nearbyEnemies,
                _spidersMask,
                QueryTriggerInteraction.Collide);

            shootingDir = nearbyEnemyCount > 0 ? GetAutoAimDirection(nearbyEnemyCount) : transform.forward;

            projectile.Launch(shootingDir);
        }

        private Vector3 GetAutoAimDirection(int nearbyEnemyCount)
        {
            Vector3 shootingDir;
            var minSqrDistance = float.MaxValue;
            var closestIndex = 0;

            for (int i = 0; i < nearbyEnemyCount; i++)
            {
                var sqrDistance = (transform.position - _nearbyEnemies[i].transform.position).sqrMagnitude;

                if (sqrDistance < minSqrDistance)
                {
                    minSqrDistance = sqrDistance;
                    closestIndex = i;
                }
            }

            shootingDir = (_nearbyEnemies[closestIndex].transform.position - transform.position).normalized;
            return shootingDir;
        }

        private void SetupDamageHandling()
        {
            if (_corruptionDamageEnabled)
            {
                Observable.Interval(TimeSpan.FromSeconds(1.0)).Subscribe(_ => CheckGroundDamage()).AddTo(this);
            }

            _health.OnDied.Subscribe(_ =>
            {
                _isDead = true;
                _animator.Play("Dead");
                _projectileHolder.gameObject.SetActive(false);
                _playerDied.Fire();

            }).AddTo(this);

            _health.OnChanged.Subscribe(health =>
            {
                _playerHealthChanged.CurrentHealth = health;
                _playerHealthChanged.Fire();
            }).AddTo(this);
        }

        private void CheckGroundDamage()
        {
            if (_chunkManager.CurrentMapChunk == null)
            {
                return;
            }

            var xMapCoord = _chunkManager.CurrentMapChunk.WorldXToMap(transform.position.x);
            var zMapCoord = _chunkManager.CurrentMapChunk.WorldZToMap(transform.position.z);

            if (_chunkManager.CurrentMapChunk.GetTile(xMapCoord, zMapCoord).State == TileState.Corrupted)
            {
                _damageable.Hit(_corruptionDamage);
            }
        }

        private void Move(Vector3 move)
        {
            move = transform.InverseTransformDirection(move);
            CheckGroundStatus();
            move = Vector3.ProjectOnPlane(move, _groundNormal);
            _turnAmount = Mathf.Atan2(move.x, move.z);
            _forwardAmount = move.z;

            ApplyExtraTurnRotation();

            if (_isGrounded)
            {
                HandleGroundedMovement();
            }
            else
            {
                HandleAirborneMovement();
            }

            UpdateAnimator(move);
        }

        private void CheckGroundStatus()
        {
            if (Physics.Raycast(
                transform.position + (Vector3.up * 0.1f),
                Vector3.down,
                out var hitInfo,
                _groundCheckDistance))
            {
                _groundNormal = hitInfo.normal;
                _isGrounded = true;
            }
            else
            {
                _isGrounded = false;
                _groundNormal = Vector3.up;
            }
        }

        private void ApplyExtraTurnRotation()
        {
            float turnSpeed = Mathf.Lerp(_stationaryTurnSpeed, _movingTurnSpeed, _forwardAmount);
            transform.Rotate(0, _turnAmount * turnSpeed * Time.deltaTime, 0);
        }

        private void HandleAirborneMovement()
        {
            Vector3 extraGravityForce = (Physics.gravity * _gravityMultiplier) - Physics.gravity;
            _rigidbody.AddForce(extraGravityForce);
            _groundCheckDistance = _rigidbody.velocity.y < 0 ? _origGroundCheckDistance : 0.01f;
        }

        private void HandleGroundedMovement()
        {
            // optional jump check here
        }

        public void OnAnimatorMove()
        {
            if (_climbHeightLimit == -1f)
            {
                _climbHeightLimit = _settings.MapSettings.HeightMultiplier;
            }

            if (_isGrounded && Time.deltaTime > 0)
            {
                Vector3 velocity = (_animator.deltaPosition * _moveSpeedMultiplier) / Time.deltaTime;

                if (_settings != null)
                {
                    velocity.y = transform.position.y > _climbHeightLimit
                        ? Mathf.Min(_rigidbody.velocity.y, 0f) : _rigidbody.velocity.y;
                }
                else
                {
                    velocity.y = _rigidbody.velocity.y;
                }

                _rigidbody.velocity = velocity;
            }
        }

        private void UpdateAnimator(Vector3 move)
        {
            if (_isDead)
            {
                _animator.speed = 1f;
                return;
            }

            _animator.SetFloat(Forward, _forwardAmount, 0.1f, Time.deltaTime);

            if (_turnAmount < 0.01f)
            {
                _turnAmount = 0f;
            }

            _animator.SetFloat(Turn, _turnAmount, 0.1f, Time.deltaTime);
            //_animator.SetBool("OnGround",_isGrounded);

            if (!_isGrounded)
            {
                _animator.SetFloat(Jump, _rigidbody.velocity.y);
            }

            float runCycle = Mathf.Repeat(_animator.GetCurrentAnimatorStateInfo(0).normalizedTime + _runCycleLegOffset, 1);
            float jumpLeg = (runCycle < 0.5f ? 1 : -1) * _forwardAmount;

            if (_isGrounded)
            {
                //_animator.SetFloat("JumpLeg", jumpLeg);
            }

            if (_isGrounded && move.magnitude > 0)
            {
                _animator.speed = _animSpeedMultiplier;
            }
            else
            {
                _animator.speed = 0f;
            }
        }

        private void HandleMovement()
        {
            Vector2 input;
            input.x = Input.GetAxis("Horizontal");
            input.y = Input.GetAxis("Vertical");
            input = Vector2.ClampMagnitude(input, 1f);

            Vector3 forward = Vector3.Scale(_cameraTransform.forward, new Vector3(1f, 0f, 1f)).normalized;
            Vector3 right = Vector3.Scale(_cameraTransform.right, new Vector3(1f, 0f, 1f)).normalized;

            _heading = forward * input.y + right * input.x;
        }
    }
}
