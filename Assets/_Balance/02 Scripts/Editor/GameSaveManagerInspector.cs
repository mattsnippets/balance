using _Balance._02_Scripts.BusinessLogic;
using _Balance._02_Scripts.BusinessLogic.GameSaves;
using UnityEditor;
using UnityEngine;

namespace _Balance._02_Scripts.Editor
{
    [CustomEditor(typeof(GameSaveManager))]
    public class GameSaveManagerInspector : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            if (GUILayout.Button("Save Game"))
            {
                ServiceLocator.Resolve<GameSaveManager>().Save();
            }

            GUILayout.Space(5f);

            if (GUILayout.Button("Load Game"))
            {
                ServiceLocator.Resolve<GameSaveManager>().Load();
            }
        }
    }
}