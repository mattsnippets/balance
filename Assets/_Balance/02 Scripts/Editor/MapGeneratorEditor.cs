﻿using _Balance._02_Scripts.ProceduralWorld.MapGenerator;
using UnityEditor;
using UnityEngine;

namespace _Balance._02_Scripts.Editor
{
	[CustomEditor(typeof(TestMapGenerator))]
	public class MapGeneratorEditor : UnityEditor.Editor
	{

		public override void OnInspectorGUI()
		{
			TestMapGenerator mapGen = (TestMapGenerator)target;

			if (DrawDefaultInspector())
			{
				if (mapGen.AutoUpdate)
				{
					mapGen.DrawHeightmapPreview();
				}
			}

			if (GUILayout.Button("Generate"))
			{
				mapGen.DrawHeightmapPreview();
			}
		}
	}
}