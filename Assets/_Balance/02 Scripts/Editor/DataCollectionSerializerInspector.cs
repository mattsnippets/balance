﻿using _Balance._02_Scripts.Inventory.Database;
using UnityEditor;
using UnityEngine;

namespace _Balance._02_Scripts.Editor
{
    [CustomEditor(typeof(DataCollectionSerializer))]
    public class DataCollectionSerializerInspector : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            if (GUILayout.Button("Export All DataCollections"))
            {
                DataCollectionSerializer.ExportAllDataCollections();
            }

            GUILayout.Space(5f);

            if (GUILayout.Button("Import All DataCollections"))
            {
                DataCollectionSerializer.ImportAllDataCollections();
            }
        }
    }
}
